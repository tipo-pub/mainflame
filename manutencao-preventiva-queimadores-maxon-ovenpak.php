<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON OVENPAK ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-maxon-ovenpak">
                <img src="img/manutencao-preventiva-queimadores-maxon-ovenpak.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">28</span>
            <span class="month">maio</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon-ovenpak">MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON OVENPAK</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica e limpeza dos queimadores instalados;</p>
            <p>– Inspeção mecânica e limpeza dos cavaletes de alimentação de gás;</p>
            <p>– Troca dos elementos filtrantes dos filtros de gás (as peças serão fornecidas pela MAINFLAME);</p>
            <p>– Troca da junta grafitada do tubo de chama dos queimadores (as peças serão fornecidas pela MAINFLAME);</p>
            <p>– Inspeção mecânica e limpeza da rampa de entrada de gás da linha;</p>
            <p>– Troca do elemento filtrante do filtro de gás da rampa de entrada de gás (a peça será fornecida pela MAINFLAME);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análises e ajustes da relação ar/gás;</p>
            <p>– Curva de desempenho dos queimadores;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento posto em marcha.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/manutencao-preventiva-queimadores-maxon-ovenpak-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/thumbs/manutencao-preventiva-queimadores-maxon-ovenpak-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/manutencao-preventiva-queimadores-maxon-ovenpak-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/thumbs/manutencao-preventiva-queimadores-maxon-ovenpak-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/manutencao-preventiva-queimadores-maxon-ovenpak-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/thumbs/manutencao-preventiva-queimadores-maxon-ovenpak-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/manutencao-preventiva-queimadores-maxon-ovenpak-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/thumbs/manutencao-preventiva-queimadores-maxon-ovenpak-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/manutencao-preventiva-queimadores-maxon-ovenpak-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/thumbs/manutencao-preventiva-queimadores-maxon-ovenpak-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/manutencao-preventiva-queimadores-maxon-ovenpak-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-ovenpak/thumbs/manutencao-preventiva-queimadores-maxon-ovenpak-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
