<?php
include 'includes/geral.php';
$title = 'Painel De Comando E Controle';
$description = 'Os Painéis Comando e Controle são projetados para gerenciar a sequência de partida e monitorar a operação do Queimador, instalado em Sistemas de Combustão.';
$keywords = 'produtos, , a melhor ';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/painel-comando-controle.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/painel-comando-controle.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Os Painéis Comando e Controle são projetados para gerenciar a sequência de partida e monitorar a operação do Queimador, instalado em Sistemas de Combustão.</p>

            <h3>Geralmente são compostos por:</h3>

            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>Programador de Chama;</li>
                        <li>Display do Programador de Chama;</li>
                        <li>Controlador de parada de emergência;</li>
                        <li>Fonte de tensão 24Vdc;</li>
                        <li>Estabilizador de tensão;</li>
                        <li>Relés para lógica e intertravamentos;</li>
                        <li>Régua de bornes para interligação elétrica de campo;</li>
                        <li>Disjuntores para proteção dos circuitos de comando;</li>
                        <li>Disjuntores Motor e Contatores;/li&gt;</li>
                        <li>Transformador Isolador 380Vac para 220Vac -1kVA;</li>
                    </ul>
                </div>

                <div class="col-md-6">
                    <ul>
                        <li>Iluminação do painel;</li>
                        <li>Chaves Seccionadora Fusível;</li>
                        <li>Chave Seccionadora Geral, com trava para cadeado;</li>
                        <li>Conversores de Frequência c/ IHM instalado no frontal do painel;</li>
                        <li>IHM Comando;</li>
                        <li>PCL;</li>
                        <li>Condicionador de ar;</li>
                        <li>Relês de Acionamento;</li>
                        <li>Tomada de Serviço.</li>
                    </ul>
                </div>
            </div>



        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
