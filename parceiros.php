<?php
include 'includes/geral.php';
$title = 'Parceiros';
$description = 'Para oferecer as melhores soluções tecnológicas, a maiflame Combustion Technology mantem parcerias.';
$keywords = 'parceiros, parcerias';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="masonry-loader masonry-loader-showing">
        <div class="row product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/maxon-1-300x242.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/honeywell-300x235.png">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/iastech-300x235.png">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/baltur-300x235.png">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/delta-300x235.png">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/dungs-300x235.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/ge-300x235.png">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/madas-300x235.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/mhfornos-300x235.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/pietrofiorentini-300x235.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/firstfornos-300x235.png">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/eclipse-1-300x235.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/hauck-300x235.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/siemens-1-300x231.jpg">
                    </span>
                </span>

            </div>
            
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/parceiros/rockwell.png">
                    </span>
                </span>

            </div>

        </div>
        
    </div>

</div>

<?php include 'includes/footer.php' ;?>
