﻿User-agent: *
Disallow: /css/
Disallow: /img/
Disallow: /includes/
Disallow: /js/
Disallow: /php/
Disallow: /vendor/
Disallow: /video/

Sitemap: https://www.mainflame.com.br/sitemap.xml