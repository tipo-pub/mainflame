<?php
include 'includes/geral.php';
$title = 'Projetos desenvolvidos';
$description = 'A MAINFLAME desenvolve projetos elétricos e mecânicos de maneira a atender as necessidades dos mais variados processos industriais.';
$keywords = 'produtos, Projetos desenvolvidos, a melhor Projetos desenvolvidos';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/projetos-desenvolvidos.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos-desenvolvidos.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A MAINFLAME Combustion Technology desenvolve projetos elétricos e mecânicos de maneira a atender as necessidades dos mais variados processos industriais que utilizam sistemas de combustão, sem deixar de seguir as exigências das normas vigentes no país.</p>

        </div>
    </div>

    <?php include 'includes/cases-relacionados.php' ;?>
    
</div>


<?php include 'includes/footer.php' ;?>
