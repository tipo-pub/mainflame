<?php
include 'includes/geral.php';
$title = 'Manômetro';
$description = 'Instrumento de medição de pressão com elemento de cápsula e carcaça de aço.';
$keywords = 'produtos, Manômetro, a melhor Manômetro';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/manometro.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/manometro.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Instrumento de medição de pressão com elemento de cápsula e carcaça de aço.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Uso: para gás e ar</li>
                <li>Diametro do visor: 80 / 100 mm</li>
                <li>Conexão roscada Rp 1/2″</li>
                <li>Faixa de pressões: 0..25 mbar a 0..25 bar</li>
                <li>Temperatura ambiente: – 20 ÷ +60°C</li>
                <li>Grau de proteção: IP32.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
