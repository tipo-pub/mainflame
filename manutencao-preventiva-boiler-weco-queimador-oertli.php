<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA BOLIER WECO';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-boiler-weco-queimador-oertli">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">02</span>
            <span class="month">ago</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-boiler-weco-queimador-oertli">MANUTENÇÃO PREVENTIVA BOLIER WECO – QUEIMADOR OERTLI</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <ul>
                <li>Inspeção mecânica do queimador instalado;</li>
                <li>Inspeção mecânica do cavalete de alimentação de gás;</li>
                <li>Mão de obra para substituição do aparelho de teste de estanqueidade;</li>
                <li>Levantamento fotográfico (com prévia autorização do cliente);</li>
                <li>Teste nas lógicas de segurança;</li>
                <li>Análise e ajuste da relação ar/gás;</li>
                <li>Elaboração de relatório;</li>
                <li>Curva de desempenho do queimador;</li>
                <li>Teste de operação;</li>
                <li>Acompanhamento de posta em marcha.</li>
            </ul>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/boiler-weco-queimador-oertli/boiler-weco-queimador-oertli-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/boiler-weco-queimador-oertli/thumbs/boiler-weco-queimador-oertli-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/boiler-weco-queimador-oertli/boiler-weco-queimador-oertli-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/boiler-weco-queimador-oertli/thumbs/boiler-weco-queimador-oertli-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/boiler-weco-queimador-oertli/boiler-weco-queimador-oertli-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/boiler-weco-queimador-oertli/thumbs/boiler-weco-queimador-oertli-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/boiler-weco-queimador-oertli/boiler-weco-queimador-oertli-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/boiler-weco-queimador-oertli/thumbs/boiler-weco-queimador-oertli-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/boiler-weco-queimador-oertli/boiler-weco-queimador-oertli-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/boiler-weco-queimador-oertli/thumbs/boiler-weco-queimador-oertli-05.jpg" alt="<?=$title;?>">
            </a>
        </div>        
    </div>

</div>


<?php include 'includes/footer.php' ;?>
