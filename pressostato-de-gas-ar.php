<?php
include 'includes/geral.php';
$title = 'Pressostato De Gás E Ar';
$description = 'É um instrumento de medição de pressão utilizado como componente do sistema de proteção de equipamento ou processos industriais. ';
$keywords = 'produtos, Pressostato De Gás E Ar, a melhor Pressostato De Gás E Ar';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/pressostato-gas-ar.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/pressostato-gas-ar.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">É um instrumento de medição de pressão utilizado como componente do sistema de proteção de equipamento ou processos industriais. Sua função básica é de proteger a integridade de equipamentos contra sobrepressão ou subpressão aplicada aos mesmos durante o seu funcionamento.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Uso: para gás e ar</li>
                <li>Faixa de pressões: 0,4 mbar a 6 bar</li>
                <li>Temperatura ambiente: – 15 ÷ +70°C</li>
                <li>Grau de proteção: IP54, IP65.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
