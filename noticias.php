<?php
include 'includes/geral.php';
$title = 'Notícias';
$description = 'A mainflame combustion technology atua no mercado de combustão industrial desde 2010, oferecendo soluções completas e otimizadas para diversos processos da indústria em geral.';
$keywords = 'notícias, principais notícias, notícias mainflame';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>
<div class="container py-4">

    <div class="row">
        
        <?php include 'includes/info-lateral.php' ;?>
        
        <div class="col-lg-9 order-lg-1">
            <div class="blog-posts">

                <article class="post post-large">
                    <div class="post-image">
                        <a href="noticia-berneck">
                            <img src="img/berneck-blog.jpg" class="img-thumbnail d-block" alt="Berneck" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="noticia-berneck">ADEQUAÇÃO DE CAVALETES DE GÁS E PAINEL ELETRICO DO SECADOR # 3.</a></h3>
                        <p> Visando a melhoria operacional redução do consumo de combustível acima de 40% e com a automação o correto diagnostico de falhas sem perda de tempo. O projeto teve como base a adequação a Norma ABNT NBR 12.313 de Setembro de 2000. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="noticia-berneck"> Berneck</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="noticia-berneck"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="noticia-ajinomoto">
                            <img src="img/ajinomoto.jpg" class="img-thumbnail d-block" alt="Ajinomoto" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">26</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="noticia-ajinomoto">FORNECIMENTO DE CAVALETES DE GÁS NATURAL, PAINEL LOCAL E PAINEL DE AUTOMAÇÃO PARA UPGRADE DO SISTEMA DE COMBUSTÃO DO FORNO DE CARVÃO GAC</a></h3>
                        <p> Adequar os sistemas de combustão a norma ABNT NBR 12.313/2000 e as normas internas AJINOMOTO. Garantindo a segurança operacional. Melhoria operacional identificando e registrando as condições de operação, proporcionando menor tempo de parada para manutenção e eliminando situações de paradas inesperadas. Substituição dos sistema de controle de combustão proporcionando economia de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="noticia-ajinomoto"> Ajinomoto</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="noticia-ajinomoto"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>


                <article class="post post-large">
                    <div class="post-image">
                        <a href="noticia-ahlstrom">
                            <img src="img/ahlstrom.jpg" class="img-thumbnail d-block" alt="Mars" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">25</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="noticia-ahlstrom">FORNECIMENTO DO QUEIMADOR, CAVALETE DE GÁS NATURAL, PAINEL DE COMANDO, INSTRUMENTAÇÃO DE CAMPO E SERVIÇOS ESPECIALIZADOS PARA ORIENTAÇÃO DE MONTAGEM, COMISSIONAMENTO E PARTIDA, PARA O SISTEMA DE COMBUSTÃO DA CALDEIRA ATA MP-807.</a></h3>
                        <p>FORNECIMENTO DE CAVALETES DE GÁS NATURAL, PAINEL LOCAL E PAINEL DE AUTOMAÇÃO PARA UPGRADE DO SISTEMA DE COMBUSTÃO DO FORNO DE CARVÃO GAC Adequar os sistemas de combustão a norma ABNT NBR 12.313/2000 e as normas internas AJINOMOTO. Garantindo a segurança operacional. Melhoria operacional identificando e registrando as condições de operação, [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="noticia-ahlstrom"> Ahlstrom</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="noticia-ahlstrom"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="noticia-mars">
                            <img src="img/mars.jpg" class="img-thumbnail d-block" alt="Mars" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">25</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="noticia-mars">CONTROLE AVANÇADO DE PROCESSO / QUEIMADOR DE ALTA PERFORMANCE AALBORG AWN – 20</a></h3>
                        <p>CONTROLE AVANÇADO DE PROCESSO / QUEIMADOR DE ALTA PERFORMANCE AALBORG AWN – 20 Substituir o Painel de Automação instalado e o Queimador na Caldeira Aalborg AWN – 20: Economia de energia elétrica; Economia de combustível (Gás Natural); Segurança da caldeira; Histórico operacional e alarmes; Melhoria operacional do sistema; Melhoria do rendimento da caldeira. Implantação sistema de controle [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="noticia-mars"> Mars</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="controle-avancado-de-processo-queimador-de-alta-performance-aalborg-awn-20"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>


            </div>
        </div>
    </div>

</div>

<?php include 'includes/footer.php' ;?>