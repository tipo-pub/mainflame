<?php
include 'includes/geral.php';
$title = 'Notícias Mars Brasil';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4 noticias">

    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">

                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-image ml-0">
                        <a href="fornecimento-de-cavaletes-de-gas-natural-painel-local-e-painel-de-automacao-para-upgrade-do-sistema-de-combustao-do-forno-de-carvao-gac-2">
                            <img src="img/mars.jpg" class="img-thumbnail d-block" alt="mars" />
                        </a>
                    </div>

                    <div class="post-date ml-0">
                        <span class="day">28</span>
                        <span class="month">Maio</span>
                    </div>

                    <div class="post-content ml-0">

                        <h3 class="text-6 line-height-3 mb-2"><a href="<?=$canonical?>">CONTROLE AVANÇADO DE PROCESSO / QUEIMADOR DE ALTA PERFORMANCE AALBORG AWN - 20</a></h3>

                        <div class="post-meta">
                            <a href="notica-mars"><span><i class="fa fa-folder-open"></i> mars</span></a>
                            <a href="noticias"><span><i class="fas fa-newspaper"></i> Notícias</span></a>
                        </div>

                        <h4>Substituir o Painel de Automação instalado e o Queimador na Caldeira Aalborg AWN - 20:</h4>

                        <ul>
                            <li>Economia de energia elétrica;</li>
                            <li>Economia de combustível (Gás Natural);</li>
                            <li>Segurança da caldeira;</li>
                            <li>Histórico operacional e alarmes;</li>
                            <li>Melhoria operacional do sistema;</li>
                            <li>Melhoria do rendimento da caldeira.</li>
                            <li>Implantação sistema de controle de combustão;</li>
                            <li>Implantação sistema de controle de nível;</li>
                            <li>Instalação de inversores de frequência nas bombas d’agua.</li>
                            <li>Instalação do queimador;</li>
                        </ul>

                    </div>
                </article>

            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/mars/image001-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/mars/thumbs/image001-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/mars/image002-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/mars/thumbs/image002-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/mars/image003-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/mars/thumbs/image003-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/mars/image004-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/mars/thumbs/image004-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>


</div>

<?php include 'includes/footer.php' ;?>
