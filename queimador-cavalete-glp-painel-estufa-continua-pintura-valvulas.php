<?php
include 'includes/geral.php';
$title = ' QUEIMADOR, CAVALETE DE GLP E PAINEL PARA ESTUFA CONTÍNUA DE PINTURA DE VÁLVULAS ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas">
                <img src="img/queimador-cavalete-de-glp-e-painel-para-estufa-continua-de-pintura.jpg" style="width: 100%" class="img-thumbnail d-block" alt="" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">10</span>
            <span class="month">fez</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas">QUEIMADOR, CAVALETE DE GLP E PAINEL PARA ESTUFA CONTÍNUA DE PINTURA DE VÁLVULAS</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p><strong>QUEIMADOR PARA GÁS GLP</strong></p>

            <ul>
                <li>Queimador de Duto montado em Gerador de Ar Quente</li>
                <li>Liberação de calor máxima: 250.000 kcal/h</li>
                <li>Vazão de gás máxima: 10 Nm³/h</li>
                <li>Sistema de controle: modulante (ar fixo)</li>
                <li>Eletrodo de ignição</li>
                <li>Sensor de chama ultravioleta para operação contínua</li>
                <li>Ventilador de ar de combustão com motor elétrico</li>
                <li>Pressostato de baixa pressão de ar</li>
                <li>Pressostato diferencial para ar</li>
                <li>Transformador de ignição</li>
                <li>Fabricação: Maxon/Prastech</li>
            </ul>

            <p><strong>CÁLCULO EFICIÊNCIA ENERGÉTICA:</strong></p>

            <p>– Operação com Resistência:</p>

            <ul>
                <li>Consumo médio de (Kw) mensal informado: 22.951Kw com um sistema de resistência em espiral.</li>
            </ul>
            
            <p>– Operação com GLP:</p>

            <ul>
                <li>Consumo médio de GLP mensal estimado a 35% da capacidade nominal do sistema de combustão: 818,40kg/mês.</li>
                <li>Obtido uma economia mensal de 70%.</li>
                <li>Tempo de retorno do investimento do projeto: 1 ano e 6 meses.</li>
            </ul>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-cavalete-glp-painel-estufa/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-cavalete-glp-painel-estufa/thumbs/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-cavalete-glp-painel-estufa/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-cavalete-glp-painel-estufa/thumbs/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-cavalete-glp-painel-estufa/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-cavalete-glp-painel-estufa/thumbs/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-cavalete-glp-painel-estufa/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-cavalete-glp-painel-estufa/thumbs/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-cavalete-glp-painel-estufa/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-cavalete-glp-painel-estufa/thumbs/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-cavalete-glp-painel-estufa/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-cavalete-glp-painel-estufa/thumbs/queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
