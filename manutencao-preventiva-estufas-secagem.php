<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA NAS ESTUFAS DE SECAGEM';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-estufas-secagem">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">30</span>
            <span class="month">out</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-estufas-secagem">MANUTENÇÃO PREVENTIVA NAS ESTUFAS DE SECAGEM</a></h3>
            
            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-estufas-secagem"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
            </div>
            
            <p>– inspeção mecânica do queimador instalado;</p>
            
            <p>– inspeção mecânica do cavalete de alimentação de gás;</p>
            
            <p>– teste nas lógicas de segurança;</p>
            
            <p>– análise e ajuste da relação ar/gás;</p>
            
            <p>– elaboração de relatório;</p>
            
            <p>– curva de desempenho do queimador;</p>
            
            <p>– teste de operação;</p>
            
            <p>– acompanhamento da posta em marcha.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-estufas-secagem/manutencao-estufas-secagem-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-estufas-secagem/thumbs/manutencao-estufas-secagem-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-estufas-secagem/manutencao-estufas-secagem-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-estufas-secagem/thumbs/manutencao-estufas-secagem-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-estufas-secagem/manutencao-estufas-secagem-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-estufas-secagem/thumbs/manutencao-estufas-secagem-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-3">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-estufas-secagem/manutencao-estufas-secagem-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-estufas-secagem/thumbs/manutencao-estufas-secagem-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
