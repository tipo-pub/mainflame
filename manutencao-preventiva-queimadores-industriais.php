<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA – QUEIMADORES INDUSTRIAISs';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-industriais">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">06</span>
            <span class="month">maio</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-industriais">MANUTENÇÃO PREVENTIVA – QUEIMADORES INDUSTRIAIS</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Verificação das condições operacionais;</p>
            <p>– Verificação da curva de relação ar/gás;</p>
            <p>– Medição e análise dos gases resultantes da combustão;</p>
            <p>– Testes operacionais.</p>

        </div>
    </article>


</div>


<?php include 'includes/footer.php' ;?>
