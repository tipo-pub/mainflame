<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO CORRETIVA JOHN DEERE ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-corretiva-john-deere">
                <img src="img/manutencao-corretiva-john-deere.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">27</span>
            <span class="month">maio</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="notica-berneck">MANUTENÇÃO CORRETIVA JOHN DEERE</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="assistencia-tecnica"> Assistência Técnica</a>, <a href="noticias">Notícias</a> </span>
            </div>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-john-deere/manutencao-corretiva-john-deere-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-john-deere/thumbs/manutencao-corretiva-john-deere-01.jpg" alt="<?=$title;?>">
            </a>
        </div>        
    </div>

</div>


<?php include 'includes/footer.php' ;?>
