<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – QUEIMADOR MAXON OVENPAK EBC 4SP ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimador-maxon-ovenpakebc-4sp">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">22</span>
            <span class="month">jun</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-maxon-ovenpakebc-4sp">MANUTENÇÃO PREVENTIVA – QUEIMADOR MAXON OVENPAK EBC 4SP</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica do queimador instalado;</p>
            <p>– Substituição do cone de mistura;</p>
            <p>– Inspeção mecânica do cavalete de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análise e ajuste da relação ar/gás;</p>
            <p>– Elaboração de relatório;</p>
            <p>– Curva de desempenho do queimador;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento da posta em marcha;</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-maxon-ovenpakebc-4sp/queimador-maxon-ovenpakebc-4sp-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-maxon-ovenpakebc-4sp/thumbs/queimador-maxon-ovenpakebc-4sp-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-maxon-ovenpakebc-4sp/queimador-maxon-ovenpakebc-4sp-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-maxon-ovenpakebc-4sp/thumbs/queimador-maxon-ovenpakebc-4sp-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-maxon-ovenpakebc-4sp/queimador-maxon-ovenpakebc-4sp-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-maxon-ovenpakebc-4sp/thumbs/queimador-maxon-ovenpakebc-4sp-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-maxon-ovenpakebc-4sp/queimador-maxon-ovenpakebc-4sp-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-maxon-ovenpakebc-4sp/thumbs/queimador-maxon-ovenpakebc-4sp-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-maxon-ovenpakebc-4sp/queimador-maxon-ovenpakebc-4sp-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-maxon-ovenpakebc-4sp/thumbs/queimador-maxon-ovenpakebc-4sp-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/queimador-maxon-ovenpakebc-4sp/queimador-maxon-ovenpakebc-4sp-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/queimador-maxon-ovenpakebc-4sp/thumbs/queimador-maxon-ovenpakebc-4sp-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
