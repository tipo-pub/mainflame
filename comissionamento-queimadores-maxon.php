<?php
include 'includes/geral.php';
$title = 'COMISSIONAMENTO QUEIMADORES MAXON';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="comissionamento-queimadores-maxon">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">12</span>
            <span class="month">fev</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="comissionamento-queimadores-maxon">COMISSIONAMENTO QUEIMADORES MAXON</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <h3 class="mt-4">COMISSIONAMENTO E START-UP</h3>

            <p>– Acendimento do queimador;</p>
            <p>– Ajustes de operação do queimador;</p>
            <p>– Teste das lógicas de segurança;</p>
            <p>– Operação assistida.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimadores-maxon/comissionamento-queimadores-maxon-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimadores-maxon/thumbs/comissionamento-queimadores-maxon-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimadores-maxon/comissionamento-queimadores-maxon-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimadores-maxon/thumbs/comissionamento-queimadores-maxon-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimadores-maxon/comissionamento-queimadores-maxon-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimadores-maxon/thumbs/comissionamento-queimadores-maxon-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimadores-maxon/comissionamento-queimadores-maxon-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimadores-maxon/thumbs/comissionamento-queimadores-maxon-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimadores-maxon/comissionamento-queimadores-maxon-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimadores-maxon/thumbs/comissionamento-queimadores-maxon-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimadores-maxon/comissionamento-queimadores-maxon-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimadores-maxon/thumbs/comissionamento-queimadores-maxon-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
