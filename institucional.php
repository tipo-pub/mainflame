<?php
include 'includes/geral.php';
$title = 'Institucional';
$description = 'A Mainflame Combustion Technology atua no mercado de combustão industrial desde 2010, oferecendo soluções completas e otimizadas para diversos processos da indústria em geral.';
$keywords = 'A Empresa, Institucional';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col">
            <h2>A Mainflame</h2>
            
            <p>A Mainflame Combustion Technology atua no mercado de combustão industrial desde 2010, oferecendo soluções completas e otimizadas para diversos processos da indústria em geral.</p>

            <p>Seu principal objetivo é garantir aos clientes serviços de qualidade, buscando maior produtividade com eficiência e baixos custos de operação e manutenção, através do controle, supervisão e gerenciamento dos sistemas aplicados.</p>

            <p>Da consultoria ao treinamento, passando por planejamento, execução e gerenciamento, atendendo do início ao fim todas as etapas do processo.</p>

            <h3>A Mainflame Combustion Technology oferece aos clientes:</h3>

            <ul>
                <li>Serviços e soluções para sistemas de combustão</li>
                <li>Consultoria técnica</li>
                <li>Projeto e fabricação de queimadores e de painéis de comando</li>
                <li>Queimadores para todo tipo de gases e líquidos combustíveis</li>
                <li>Assistência técnica especializada, manutenção preventiva e corretiva</li>
                <li>Reforma de queimadores, válvulas e componentes</li>
            </ul>

        </div>
    </div>
</div>


<?php include 'includes/footer.php' ;?>
