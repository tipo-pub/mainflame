<?php
include 'includes/geral.php';
$title = 'Manutenção';
$description = 'A MAINFLAME Combustion Technology dispõe de técnicos e engenheiros altamente capacitados e com ampla experiência em processos industriais';
$keywords = 'serviços, Manutenção, a melhor Manutenção';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/manutencao.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/manutencao.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A MAINFLAME Combustion Technology dispõe de técnicos e engenheiros altamente capacitados e com ampla experiência em processos industriais que utilizam sistemas de combustão, atuando no auxílio, orientação e acompanhamento em projetos de melhorias e em adequações as normas vigentes no país e no mundo.</p>
            
            <p class="mt-2">A MAINFLAME Combustion Technology segue rigorosamente as exigências da norma brasileira em vigor a NBR-12.313 Rev. SET/2000 – Sistemas de Combustão – Controle e Segurança para Utilização de Gases Combustíveis em Processos de Baixa e Alta Temperatura.</p>

        </div>
    </div>
    
    <?php include 'includes/cases-relacionados.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
