<?php
include 'includes/geral.php';
$title			= 'Falha ao Enviar Formulário';
$description	= 'Falha ao Enviar Formulário. Tente novamente mais tarde ou entre em contato pelos telefones disponíveis no site.';
$keywords		= '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>
				<h2 class="entry-title"> </h2>


<div class="container py-4">
	<section class="http-error">
		<div class="row justify-content-center">
			<div class="col text-center">
				<div class="http-error-main">
					<h2><i class="fa fa-exclamation-triangle fa-2x"></i></h2>
					<p class="text-10"><?=$title?></p>
					<p class="text-4 my-2">Tente novamente mais tarde ou entre em contato: <br />
						<a href="tel:<?=$linktel;?>" title="Telefone para contato"> <span>(<?=$ddd;?>) <?=$tel;?></span></a> /
						<a href="tel:<?=$linktel2;?>" title="Telefone para contato"> <span>(<?=$ddd;?>) <?=$tel2;?></span></a> /
						<a href="<?=$linkWhats;?>" title="Atendimento Online"> <span>(<?=$ddd;?>) <?=$whats;?></span> <i class="fab fa-whatsapp"></i></a> /
						<a href="mailto:<?=$email;?>" title="E-mail para contato"><span><?=$email;?></span></a>
					</p>
				</div>
			</div>	
		</div>
	</section>
</div>

<?php include "includes/footer.php";?>