<?php
include 'includes/geral.php';
$title = 'Válvula Solenoide de Vent';
$description = 'A válvula solenoide Normalmente Aberta é uma válvula de abertura automática operado com a alimentação auxiliar.';
$keywords = 'produtos, Válvula Solenoide de Vent, a melhor Válvula Solenoide de Vent';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-solenoide-descarga-automatica.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-solenoide-descarga-automatica.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A válvula solenoide Normalmente Aberta é uma válvula de abertura automática operado com a alimentação auxiliar. A unidade magnética eletromagnética fecha contra a força da mola de pressão. Se a energia auxiliar for interrompida (tensão de operação), a mola abre a válvula dentro de 1 segundo.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Normalmente aberta</li>
                <li>Conexões roscadas Rp: (DN 15 ÷ DN 50) de acordo com DIN 2999</li>
                <li>Pressão de trabalho: 200 mbar a 6 bar</li>
                <li>Temperatura ambiente: – 15 ÷ +60°C</li>
                <li>Grau de proteção: IP54, IP65.</li>
                <li>Tempo de abertura: &lt; 1 s.</li>
                <li>Tensão de alimentação: 24 VCC, 110 VCA, 220 VCA</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
