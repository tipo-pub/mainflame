<?php
include 'includes/geral.php';
$title = 'Válvula de Alivio';
$description = 'A válvula de alívio é um dispositivo de segurança (SBV), que permite a passagem de gás quando a pressão do sistema exceder o valor definido.';
$keywords = 'produtos, Válvula de Alivio, a melhor Válvula de Alivio';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-alivio.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-alivio.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A válvula de alívio é um dispositivo de segurança (SBV), que permite a passagem de gás quando a pressão do sistema exceder o valor definido, devido a eventos temporários, tais como expansão devido ao aumento de temperatura do gás, choques de pressão a jusante do regulador de pressão, causadas por mudanças bruscas de vazão abertura/fechamento dos dispositivos de bloqueio, etc.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Conexões roscadas Rp: (DN 6 ÷ DN 50) de acordo com DIN 2999</li>
                <li>Pressão de entrada: até 6 bar</li>
                <li>Pressão de alivio: 16 mbar a 500 mbar</li>
                <li>Temperatura ambiente: – 20 ÷ +60°C</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
