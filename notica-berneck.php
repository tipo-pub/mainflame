<?php
include 'includes/geral.php';
$title = 'Notícias Berneck';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4 noticias">

    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">

                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-image ml-0">
                        <a href="notica-berneck">
                            <img src="img/berneck-blog.jpg" class="img-thumbnail d-block" alt="Berneck" />
                        </a>
                    </div>

                    <div class="post-date ml-0">
                        <span class="day">28</span>
                        <span class="month">Maio</span>
                    </div>

                    <div class="post-content ml-0">

                        <h3 class="text-6 line-height-3 mb-2"><a href="<?=$canonical?>"><?=$title?></a></h3>

                        <div class="post-meta">
                            <a href="notica-berneck"><span><i class="fa fa-folder-open"></i> Berneck</span></a>
                            <a href="noticias"><span><i class="fas fa-newspaper"></i> Notícias</span></a>
                        </div>

                        <p>Visando a melhoria operacional redução do consumo de combustível acima de 40% e com a automação o correto diagnostico de falhas sem perda de tempo.</p>

                        <p>O projeto teve como base a adequação a Norma ABNT NBR 12.313 de Setembro de 2000.</p>

                    </div>
                </article>

            </div>
        </div>



    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/berneck/intro-lg-2.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/berneck/thumbs/intro-lg-2.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/berneck/intro-lg-3.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/berneck/thumbs/intro-lg-3.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/berneck/intro-lg-4.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/berneck/thumbs/intro-lg-4.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/berneck/intro-lg-5.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/berneck/thumbs/intro-lg-5.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/berneck/intro-lg-6.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/berneck/thumbs/intro-lg-6.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/berneck/intro-lg-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/berneck/thumbs/intro-lg-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>

<?php include 'includes/footer.php' ;?>
