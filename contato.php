<?php
include 'includes/geral.php';
$title = 'Fale Conosco';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3657.3158495226767!2d-46.531788!3d-23.5570971!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5de5cab21da5%3A0xb85eaf33fb4032bd!2sMAINFLAME+Combustion+Technology!5e0!3m2!1spt-BR!2sbr!4v1558641652298!5m2!1spt-BR!2sbr" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen=""></iframe>

<div class="container py-4">
    <div class="row mb-5">
        <div class="col-md-6">
            <form role="form" method="post">
                <h2><?=$title?></h2>
                <div class="form-group">
                    <input type="text" class="form-control required name" placeholder="Nome*" aria-required="true" name="nome" value="<?php echo isset($_POST['nome']) && !empty($_POST['nome']) ? $_POST['nome'] : '';?>" />
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control required" placeholder="Telefone" aria-required="true" name="telefone" value="<?php echo isset($_POST['telefone']) && !empty($_POST['telefone']) ? $_POST['telefone'] : '';?>" />
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control required email" placeholder="E-mail*" aria-required="true" name="email" value="<?php echo isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : '';?>" />
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control required" rows="4" placeholder="Mensagem*" area-required="true" name="mensagem"><?php echo isset($_POST['mensagem']) && !empty($_POST['mensagem']) ? $_POST['mensagem'] : '';?></textarea>
                </div>
                <div class="form-group col-md-7" style="float: left;">
                    <div class="g-recaptcha" data-sitekey="<?=$sitekey?>"></div>
                </div>
                <div class="form-group col-md-5 text-center" style="line-height: 78px; float: right;">
                    <button type="submit" name="submit" value="submit" class="tp-caption btn btn-primary text-uppercase font-weight-bold">Enviar</button>
                </div>
                <?php if(isset($_POST['submit'])){ require_once('php/cadastro-form.php'); } ?>
            </form>
        </div>
        <div class="col-md-6">
            <h3 class="mt-2 mb-1">Contatos</h3>
            <ul class="list list-icons list-icons-style-3 mt-2">
                <li><i class="fas fa-map-marker-alt top-6"></i> <a target="_blank" href="<?=$linkMaps;?>"> <?=$endereco;?> - <?=$bairro;?> - <?=$cidade;?> - CEP: <?=$cep;?></a></li>
                <li><i class="fas fa-phone top-6"></i> <a href="tel:<?=$linktel;?>" title="Telefone para contato"> (<?=$ddd;?>) <?=$tel;?></a></li>
                <li><i class="fas fa-phone top-6"></i> <a href="tel:<?=$linktel2;?>" title="Telefone para contato"> (<?=$ddd;?>) <?=$tel2;?></a></li>
                <li><i class="fab fa-whatsapp top-6"></i> <a href="<?=$linkWhats;?>" title="Atendimento Online"> (<?=$ddd;?>) <?=$whats;?></a></li>
                <li><i class="fas fa-envelope top-6"></i> <a href="mailto:<?=$email;?>" title="E-mail para contato"><?=$email;?></a></li>
            </ul>
        </div>
    </div>
</div>
<?php include 'includes/footer.php' ;?>
