<?php
include 'includes/geral.php';
$title = ' Sensor de Chama ';
$description = 'O Sensor de Chama ultravioleta tem a função de detectar e monitorar a presença de chama principal e piloto.';
$keywords = 'produtos, Sensor de Chama, a melhor Sensor de Chama';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/sensor-chama.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/sensor-chama.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">O Sensor de Chama ultravioleta tem a função de detectar e monitorar a presença de chama principal e piloto.</p>

        </div>
    </div>
    
    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
