<?php
include 'includes/geral.php';
$title = '';
$description = '';
$keywords = 'produtos, , a melhor ';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2"> </p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>

            </ul>

        </div>
    </div>

    <?php include 'includes/cases-relacionados.php' ;?>
    
</div>


<?php include 'includes/footer.php' ;?>
