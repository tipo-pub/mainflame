<?php
include 'includes/geral.php';
$title = 'Válvula Proporcionadora Ar/Gás';
$description = 'Válvula proporcionadora ar/gás mantem a relação ar/gás constante em 1:1.';
$keywords = 'produtos, Válvula Proporcionadora Ar/Gás ,a melhor Válvula Proporcionadora Ar/Gás ';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-proporcionadora-ar-gas.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-proporcionadora-ar-gas.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Válvula proporcionadora ar/gás mantem a relação ar/gás constante em 1:1.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Conexões roscadas Rp: (DN 15 ÷ DN 50) de acordo com DIN 2999</li>
                <li>Conexões flangeadas PN 16 (DN 40 ÷ DN 150) de acordo com ISO 7005</li>
                <li>Pressão maxima de operção: até 500 mbar</li>
                <li>Pressão de saída: 5 mbar a 200 mbar</li>
                <li>Temperatura ambiente: – 15 ÷ +70°C</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
