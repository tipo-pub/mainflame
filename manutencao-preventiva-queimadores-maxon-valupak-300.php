<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA - QUEIMADORES MAXON VALUPAK 300';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-maxon-valupak-300">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">28</span>
            <span class="month">dez</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon-valupak-300"></a>MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON VALUPAK 300</h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="<?=$canonical;?>">Nossa atuação</a> </span>
                
            </div>

        </div>

        <div class="mt-3">
            <p>– inspeção no cavalete de alimentação de gás;</p>
            <p>– queimadores retirados do local de operação e transportados até a bancada de manutenção;</p>
            <p>– realizado desmontagem dos queimadores e componentes;</p>
            <p>– verificação geral dos queimadores;</p>
            <p>– limpeza do queimador e do ventilador de ar de combustão;</p>
            <p>– limpeza e ajuste do eletrodo de ignição;</p>
            <p>– remontagem e instalação dos queimadores e seus componentes;</p>
            <p>– acendimento dos queimadores para teste;</p>
            <p>– verificação das condições operacionais (pressões Ar x Gás);</p>
            <p>– ajuste e regulagem dos queimadores, relação Ar x Gás;</p>
            <p>– testes de acendimento dos queimadores;</p>
            <p>– acompanhamento operacional.</p>
        </div>



    </article>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-valupak/manutencao-queimadores-maxon-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-valupak/thumbs/manutencao-queimadores-maxon-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-valupak/manutencao-queimadores-maxon-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-valupak/thumbs/manutencao-queimadores-maxon-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-valupak/manutencao-queimadores-maxon-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-valupak/thumbs/manutencao-queimadores-maxon-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>


</div>


<?php include 'includes/footer.php' ;?>
