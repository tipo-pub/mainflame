<?php
include 'includes/geral.php';
$title = ' Aparelho Programador De Chama ';
$description = 'Unidade de controle automático e monitoração de queimadores à gás, para operação intermitente, controle por ionização ou foto célula ultra violeta.';
$keywords = 'produtos, Aparelho Programador De Chama, o melhor Aparelho Programador De Chama';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/aparelho-programador-chama.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/aparelho-programador-chama.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Unidade de controle automático e monitoração de queimadores à gás, para operação intermitente, controle por ionização ou foto célula ultra violeta.</p>


        </div>
    </div>
    
    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
