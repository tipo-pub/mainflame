<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – FORNALHA DA PLANTA TÉRMICA- QUEIMADORES COEN ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-fornalha-planta-termica-queimadores-coen">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">08</span>
            <span class="month">jul</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-fornalha-planta-termica-queimadores-coen">MANUTENÇÃO PREVENTIVA – FORNALHA DA PLANTA TÉRMICA- QUEIMADORES COEN</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Acompanhamento/orientação da substituição de peças de reposição dos queimadores;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análises e ajustes da relação ar/combustível;</p>
            <p>– Elaboração de relatório;</p>
            <p>– Curva de desempenho dos queimadores;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento da posta em marcha.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/fornalha-planta-termica-queimadores-coen/fornalha-planta-termica-queimadores-coen-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/fornalha-planta-termica-queimadores-coen/thumbs/fornalha-planta-termica-queimadores-coen-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/fornalha-planta-termica-queimadores-coen/fornalha-planta-termica-queimadores-coen-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/fornalha-planta-termica-queimadores-coen/thumbs/fornalha-planta-termica-queimadores-coen-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/fornalha-planta-termica-queimadores-coen/fornalha-planta-termica-queimadores-coen-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/fornalha-planta-termica-queimadores-coen/thumbs/fornalha-planta-termica-queimadores-coen-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/fornalha-planta-termica-queimadores-coen/fornalha-planta-termica-queimadores-coen-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/fornalha-planta-termica-queimadores-coen/thumbs/fornalha-planta-termica-queimadores-coen-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/fornalha-planta-termica-queimadores-coen/fornalha-planta-termica-queimadores-coen-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/fornalha-planta-termica-queimadores-coen/thumbs/fornalha-planta-termica-queimadores-coen-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/fornalha-planta-termica-queimadores-coen/fornalha-planta-termica-queimadores-coen-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/fornalha-planta-termica-queimadores-coen/thumbs/fornalha-planta-termica-queimadores-coen-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
