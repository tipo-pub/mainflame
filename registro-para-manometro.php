<?php
include 'includes/geral.php';
$title = 'Registro para Manômetro';
$description = 'Operado manualmente, o dispositivo bloqueia a pressão entre a linha de medição e o instrumento medidor, de acordo com a DIN 3537-1.';
$keywords = 'produtos, Registro para Manômetro, a melhor Registro para Manômetro';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/registro-manometro.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/registro-manometro.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Operado manualmente, o dispositivo bloqueia a pressão entre a linha de medição e o instrumento medidor, de acordo com a DIN 3537-1. Quando o botão é pressionado a pressão passa para o instrumento de medição e o instrumento passa indicar a pressão. Quando o botão deixa de ser pressionado, a pressão é aliviada e o instrumento de medição deixa de indicar a pressão.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Uso: para gás e ar</li>
                <li>Pressão maxima: 5 bar</li>
                <li>Temperatura ambiente: – 15 ÷ +70°C</li>
                <li>Conexão roscada Rp 1/2″</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
