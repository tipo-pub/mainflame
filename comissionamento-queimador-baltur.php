<?php
include 'includes/geral.php';
$title = 'Comissionamento Queimador BALTUR';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="comissionamento-queimador-baltur">
                <img src="img/comissionamento-queimador-baltur.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Comissionamento Queimador BALTUR" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">02</span>
            <span class="month">dez</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="comissionamento-queimador-baltur">Comissionamento Queimador BALTUR</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="assistencia-tecnica"> Assistência Técnica</a>, <a href="noticias">Notícias</a> </span>

            </div>

            <p>– Comissionamento de 01 x Queimador Marca: BALTUR Modelo: BT 120 DSPG (diesel).</p>

            <p>– Instalado no AQUECEDOR DE FLUÍDO TÉRMICO Marca: GARIONI NAVAL Tipo: TH-V 600 – 697 KW.</p>

            <p>– A bordo do Navio Forte de São Marcos.</p>

            <h3>DESCRIÇÃO DOS SERVIÇOS EXECUTADOS:</h3>

            <p>– acompanhamento e orientações, durante a:</p>

            <ul>
                <li>fabricação de um novo flange de transição, para fixação do queimador.</li>
                <li>lançamento de um novo cabo (3x 2,5mm), para alimentação motriz (440V trifásico) do motor da bomba de diesel.</li>
                <li>instalação do bico de atomização.</li>
                <li>ajustes dos eletrodos de ignição.</li>
                <li>fixação do queimador.</li>
                <li>instalação das mangueiras flexíveis para alimentação e retorno do diesel combustível.</li>
                <li>alterações e interligações elétricas, na nova caixa do queimador.</li>
            </ul>

            <p>– acompanhamento e orientações, durante a:</p>

            <ul>
                <li>energização do painel de comando.</li>
                <li>testes elétricos.</li>
                <li>acionamento dos motores.</li>
                <li>acendimento do queimador.</li>
            </ul>

            <p>– testes operacionais.</p>



        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimador-baltur/comissionamento-queimador-baltur-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimador-baltur/thumbs/comissionamento-queimador-baltur-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimador-baltur/comissionamento-queimador-baltur-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimador-baltur/thumbs/comissionamento-queimador-baltur-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimador-baltur/comissionamento-queimador-baltur-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimador-baltur/thumbs/comissionamento-queimador-baltur-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimador-baltur/comissionamento-queimador-baltur-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimador-baltur/thumbs/comissionamento-queimador-baltur-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/comissionamento-queimador-baltur/comissionamento-queimador-baltur-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/comissionamento-queimador-baltur/thumbs/comissionamento-queimador-baltur-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
