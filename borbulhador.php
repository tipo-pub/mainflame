<?php
include 'includes/geral.php';
$title = 'Borbulhador';
$description = 'O indicador visual de vazamento (Borbulhador) é um dispositivo de segurança instalado a jusante das Válvulas de Alívio e/ou “Vent”.';
$keywords = 'produtos, Borbulhador, a melhor Borbulhador';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/borbulhador.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/borbulhador.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">O indicador visual de vazamento (Borbulhador) é um dispositivo de segurança instalado a jusante das Válvulas de Alívio e/ou “Vent” que permite a visualização do fluxo de gás descartado para a atmosfera.</p>

        </div>
    </div>
    
    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
