<?php
include 'includes/geral.php';
$title = 'Filtro para Gás';
$description = 'Dispositivo que impede a passagem de partículas de pó ou impurezas contidas no gás, protegendo assim, os dispositivos de regulagem e segurança.';
$keywords = 'produtos, , a melhor ';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/filtro-gas.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/filtro-gas.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Dispositivo que impede a passagem de partículas de pó ou impurezas contidas no gás, protegendo assim, os dispositivos de regulagem e segurança.</p>
            <p>É composto por um elemento filtrante podendo ser completamente removido para inspeção e limpeza.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Uso: Gases das três famílias (secos e não agressivos)</li>
                <li>Conexões roscadas Rp: (DN 15 ÷ DN 50) de acordo com EN 10226</li>
                <li>Conexões flangeadas PN 16 (DN 50 ÷ DN 300) de acordo com ISO 7005</li>
                <li>Sob encomenda, Conexões flangeadas ANSI 150</li>
                <li>Pressão máxima de trabalho: 2 bar ou 6 bar (verificar etiqueta do produto)</li>
                <li>Temperatura ambiente: – 20 ÷ +70°C</li>
                <li>Filtragem: 20μm – 10μm</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
