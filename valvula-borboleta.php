<?php
include 'includes/geral.php';
$title = 'Válvula Borboleta';
$description = 'Válvula borboleta para gás / ar são projetados para regulagem e controle de fluxo em processos de combustão.';
$keywords = 'produtos, Válvula Borboleta, a melhor Válvula Borboleta';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-borboleta.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-borboleta.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Válvula borboleta para gás / ar são projetados para regulagem e controle de fluxo em processos de combustão. Possibilidade de ter um ou dois tamanhos de reduções de tamanho do diâmetro nominal.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Uso: Gás ou Ar</li>
                <li>Montagem entre flanges PN 16 (DN 25 ÷ DN 150) de acordo com ISO 7005</li>
                <li>Pressão máxima de trabalho: 500 mbar</li>
                <li>Temperatura ambiente: – 15 ÷ +100°C</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
