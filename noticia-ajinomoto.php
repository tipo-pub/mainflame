<?php
include 'includes/geral.php';
$title = 'Notícias Ajinomoto';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4 noticias">

    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">

                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-image ml-0">
                        <a href="notica-ajinomoto">
                            <img src="img/ajinomoto.jpg" class="img-thumbnail d-block" alt="Berneck" />
                        </a>
                    </div>

                    <div class="post-date ml-0">
                        <span class="day">26</span>
                        <span class="month">Maio</span>
                    </div>

                    <div class="post-content ml-0">

                        <h3 class="text-6 line-height-3 mb-2"><a href="<?=$canonical?>">FORNECIMENTO DE CAVALETES DE GÁS NATURAL, PAINEL LOCAL E PAINEL DE AUTOMAÇÃO PARA UPGRADE DO SISTEMA DE COMBUSTÃO DO FORNO DE CARVÃO GAC.</a></h3>

                        <div class="post-meta">
                            <a href="notica-ajinomoto"><span><i class="fa fa-folder-open"></i> Ajinomoto</span></a>
                            <a href="noticias"><span><i class="fas fa-newspaper"></i> Notícias</span></a>
                        </div>

                        <ul>
                            <li>Adequar os sistemas de combustão a norma ABNT NBR 12.313/2000 e as normas internas AJINOMOTO. Garantindo a segurança operacional.</li>
                            <li>Melhoria operacional identificando e registrando as condições de operação, proporcionando menor tempo de parada para manutenção e eliminando situações de paradas inesperadas.</li>
                            <li>Substituição dos sistema de controle de combustão proporcionando economia de combustível (GN) e melhor eficiência energética.</li>
                            <li>Modernização do sistema de automação garantindo a segurança operacional e padronização com os equipamentos da planta.</li>
                        </ul>

                    </div>
                </article>

            </div>
        </div>



    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image001.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image001.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image002.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image002.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image003.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image003.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image004.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image004.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image005.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image005.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image006.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image006.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-4">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image007.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image007.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ajinomoto/image008.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ajinomoto/thumbs/image008.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>

<?php include 'includes/footer.php' ;?>
