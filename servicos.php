<?php
include 'includes/geral.php';
$title = 'Serviços';
$description = 'Confira nossos serviços.';
$keywords = 'serviços, manutenção engenharia e consultoria, projetos desenvolvidos, assistência técnica.';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4">



	<div class="sort-destination-loader-showing mt-4 pt-2">
		<div class="row portfolio-list sort-destination" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">							

			<div class="col-md-6 mb-4 isotope-item brands">
				<div class="portfolio-item">
					<a href="manutencao">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="img/manutencao.jpg" class="img-fluid border-radius-0" alt="Manutenção">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Manutenção</span>
									<span class="thumb-info-type">Serviços</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-md-6 mb-4 isotope-item brands">
				<div class="portfolio-item">
					<a href="engenharia-consultoria">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="img/consultoria.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Engenharia & Consultoria</span>
									<span class="thumb-info-type">Serviços</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-md-6 mb-4 isotope-item logos">
				<div class="portfolio-item">
					<a href="projetos-desenvolvidos">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="img/projetos-desenvolvidos.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Projetos desenvolvidos</span>
									<span class="thumb-info-type">Serviços</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>	

			<div class="col-md-6 mb-4 isotope-item logos">
				<div class="portfolio-item">
					<a href="assistencia-tecnica">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="img/assistencia-tecnica.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Assistência Técnica</span>
									<span class="thumb-info-type">Serviços</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

		</div>
	</div>

</div>

<?php include 'includes/footer.php' ;?>
