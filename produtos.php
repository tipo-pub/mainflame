<?php
include 'includes/geral.php';
$title = 'Produtos';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-2">

    <input type='hidden' id='current_page' />
    <input type='hidden' id='show_per_page' />
    <div class="row contProdutos" id='content'>
        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-bloqueio-com-atuador-eletromecanico-ou-pneumatico">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-atuador-eletromecanico.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Válvulas para Gás e Óleo</span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="aparelho-programador-chama">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/aparelho-programador-chama.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Aparelho Programador De Chama </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="sensor-de-chama">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/sensor-chama.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Sensor de Chama </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="queimadores-para-alta-temperatura">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/queimadora-alta-temperatura.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Queimadores Para Alta Temperatura </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-proporcionadora-ar-gas-s">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-proporcionadora-ar-gas.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Proporcionadora Ar/Gás </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="queimador-piloto">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/queimador-piloto.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Queimador Piloto </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="queimadores-para-baixa-temperatura">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/queimador-baixa-temperatura.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Queimadores Para Baixa Temperatura </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="servo-motor">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/servo-motor.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Servo Motor </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-borboleta">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-borboleta.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Borboleta </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="painel-de-comando-e-controle">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/painel-comando-controle.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Painel De Comando E Controle </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="transformador-de-ignicao">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/tranformador-ignicao.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Transformador De Ignição </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="medidor-vazao">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/medidor-vazao.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Medidor De Vazão </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-esfera">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-esfera.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Valvula de Esfera </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="filtro-para-gas">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/filtro-gas.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Filtro para Gás </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-reguladora-pressao-de-gas">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-reguladora-pressao-gas.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Reguladora de Pressão de Gás </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-de-alivio">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-alivio.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula de Alivio </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-reguladora-pressao-shut-off">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-bloqueio-sobrepressao.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula de Bloqueio por Sobrepressão – SHUT OFF </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-solenoide-de-bloqueio-automatico">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-solenoide-bloqueio-automatico.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Solenoide de Bloqueio Automatico </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="valvula-solenoide-bloqueio-vent">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-solenoide-descarga-automatica.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Solenoide de Vent </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="borbulhador">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/borbulhador.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Borbulhador</span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="pressostato-de-gas-ar">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/pressostato-gas-ar.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Pressostato De Gás E Ar </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="manometro">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/manometro.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Manômetro</span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="registro-para-manometro">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/registro-manometro.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Registro para Manômetro </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="portfolio-item">
                <a href="slate">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/slate.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Slate</span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex align-items-center justify-content-center" id='portfolioPagination'></div>
</div>
<?php include 'includes/footer.php' ;?>