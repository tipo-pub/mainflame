<?php
include 'includes/geral.php';
$title = ' PAINEL DE COMANDO, CONTROLE E SEGURANÇA DO SISTEMA DE COMBUSTÃO,';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="painel-comando-controle-seguranca-sistema-combustao-operacao-09-cavaletes-alimentacao-gas-natural">
                <img src="img/painel-comando-controle-seguranca-sistema-combustao.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">28</span>
            <span class="month">abr</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="painel-comando-controle-seguranca-sistema-combustao-operacao-09-cavaletes-alimentacao-gas-natural">PAINEL DE COMANDO, CONTROLE E SEGURANÇA DO SISTEMA DE COMBUSTÃO, PARA OPERAÇÃO DE 09 CAVALETES DE ALIMENTAÇÃO DE GÁS NATURAL</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="projetos-desenvolvidos"> Projetos desenvolvidos</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <h3>PAINEL DE COMANDO, CONTROLE E SEGURANÇA:</h3>

            <p>Painel de comando, controle e segurança do sistema de combustão de 08 cavaletes de alimentação de gás.</p>
            <p>Atendendo requisitos da norma ABNT NBR 12.313 de Setembro de 2000.</p>

            <ul>
                <li>Painel 1400x800x250mm, c/ grau de proteção IP 54, com pintura cinza RAL 7032;</li>
                <li>CLP Rockwell, Compact Logix (RS Logix 5000);</li>
                <li>Cartões I/O, montados em rack contemplando CPU e fonte de alimentação exclusiva;</li>
                <li>IHM colorida, Panel View Plus Touch Screen;</li>
                <li>Comunicação Ethernet, com o Supervisório;</li>
                <li>Histórico de Alarmes de 30 dias;</li>
                <li>Documentação do painel em CAD e diagrama Ladder comentada em CD;</li>
                <li>Régua de bornes de interligação com os componentes de campo anilhadas e identificadas.</li>
            </ul>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-07.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/painel-comando-controle-seguranca/painel-comando-controle-seguranca-08.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/painel-comando-controle-seguranca/thumbs/painel-comando-controle-seguranca-08.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
