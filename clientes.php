<?php
include 'includes/geral.php';
$title = 'Clientes';
$description = 'Para oferecer as melhores soluções tecnológicas, a maiflame Combustion Technology mantem parcerias.';
$keywords = 'clientes, parcerias';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="masonry-loader masonry-loader-showing">
        <div class="row product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/3m-do-brasil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/adonai-quimica.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/advance-textil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/ahlstrom_cef.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/ajinomoto_f.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/allevard-rejna.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/andina.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/arauco.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/armco-staco.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/artecola.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/albaugh-e1559669846880.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/avk-valvulas-do-brasil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/bann-quimica-ltda.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/berneck.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/brasjuta.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/brunnschweiler.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/cambarasa-e1559305006753.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/cargill.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/continental.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/cp-kelco.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/cpw-nestle.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/damapel.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/ecolab-nalco.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/elcano.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/fba.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/freudenberg.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/fumagalli.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/fundacao-butantan.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/gerdau.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/grupo-serveng.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/honda-itirapina.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/huhtamaki-lurgan-limited-e1559308099723.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/iastech.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/imcopa.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/imerys.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/ingredion-cabo-de-santo-agostinho-pe.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/japi.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/john-deere-cf.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/latapack.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/link-steel.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/mangels.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/mars-1.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/masisa-mais-confianca.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/max-del_d.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/maxion-sistemas-automotivos.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/maxion-wheels.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/melhoramentos-e1559310258442.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/metalfriosolutions-e1559310516341.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/nissan-do-brasil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/nova-forma.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/owens-illinois-cisper_ca.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/plastrela.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/psa-peugeot-citroen.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/quimica-amparo-ype.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/rei-abrasivos.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/reichhold.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/saint-gobain_a.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/sakura.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/sanofi.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/sanovo-greenpack.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/santa-constancia.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/si-group.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/starpac.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/sylconstec-e1559311631904.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/termomecanica.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/tetra-pak.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/toyota.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/tw-espumas.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/unilever-brasil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/unimil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/usiminas.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/valtra.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/videolar_bae.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/vidrotil.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/votorantim-metais.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/wahler_ecd.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/weg.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/wickbold.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/zilor.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/wifag-polytype.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/zamfor.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/zf-friedrichshafen.jpg">
                    </span>
                </span>

            </div>

            <div class="col-12 col-sm-6 col-lg-3 mb-4">

                <span class="product-thumb-info border-0">
                    <span class="product-thumb-info-image">
                        <img alt="" class="img-fluid" src="img/clientes/ball.jpg">
                    </span>
                </span>

            </div>


        </div>

    </div>

</div>

<?php include 'includes/footer.php' ;?>
