<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-maxon">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">09</span>
            <span class="month">jun</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon">MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Retirada, desmontagem e limpeza dos queimadores instalados;</p>
            <p>– Substituição dos filtros de ar e juntas (materiais inclusos);</p>
            <p>– Remontagem e reinstalação dos queimadores;</p>
            <p>– Inspeção mecânica dos cavaletes de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análise e ajuste da relação ar/gás;</p>
            <p>– Elaboração de relatórios;</p>
            <p>– Curva de desempenho dos queimadores;</p>
            <p>– Testes de operação;</p>
            <p>– Acompanhamento de posta em marcha;</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon/manutencao-preventiva-queimadores-maxon-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon/thumbs/manutencao-preventiva-queimadores-maxon-07.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
