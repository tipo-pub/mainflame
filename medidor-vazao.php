<?php
include 'includes/geral.php';
$title = 'Medidor De Vazão';
$description = 'O medidor de vazão tipo turbina é um equipamento que mede diretamente a vazão dos gases nas condições de medição.';
$keywords = 'produtos, Medidor De Vazão, a melhor Medidor De Vazão';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/medidor-vazao.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/medidor-vazao.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2"> O medidor de vazão tipo turbina é um equipamento que mede diretamente a vazão dos gases nas condições de medição. Estas medidas são visualizadas num totalizador eletrônico.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Range de medição: 2,5 a 25.000 m³/h</li>
                <li>Diâmetro nominal: 25 a 600</li>
                <li>Conexões: roscadas, flangeadas ou montada entre flanges</li>
                <li>Grau de proteção: IP65</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
