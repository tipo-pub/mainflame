<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA FORNO DE TRATAMENTO TÉRMICO SERMEP';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-forno-tratamento-termico-sermep">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">26</span>
            <span class="month">fev</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-forno-tratamento-termico-sermep">MANUTENÇÃO PREVENTIVA FORNO DE TRATAMENTO TÉRMICO SERMEP</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica dos queimadores instalados;</p>
            <p>– Inspeção mecânica do cavalete de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análise e ajuste da relação ar/gás;</p>
            <p>– Elaboração de relatório;</p>
            <p>– Curva de desempenho dos queimadores;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento de posta em marcha.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/manutencao-preventiva-forno-tratamento-termico-sermep-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/thumbs/manutencao-preventiva-forno-tratamento-termico-sermep-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/manutencao-preventiva-forno-tratamento-termico-sermep-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/thumbs/manutencao-preventiva-forno-tratamento-termico-sermep-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/manutencao-preventiva-forno-tratamento-termico-sermep-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/thumbs/manutencao-preventiva-forno-tratamento-termico-sermep-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/manutencao-preventiva-forno-tratamento-termico-sermep-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/thumbs/manutencao-preventiva-forno-tratamento-termico-sermep-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/manutencao-preventiva-forno-tratamento-termico-sermep-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-forno-tratamento-termico-sermep/thumbs/manutencao-preventiva-forno-tratamento-termico-sermep-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
