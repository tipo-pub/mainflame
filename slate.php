<?php
include 'includes/geral.php';
$title = 'Slate';
$description = 'A revolucionaria integração de segurança configurável e lógica programável em uma única plataforma.';
$keywords = 'produtos, Slate, a melhor Slate';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/slate.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/slate.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A revolucionaria integração de segurança configurável e lógica programável em uma única plataforma, o SLATE permite que você ofereça soluções diferenciadas, de uma forma rápida, fácil e mais econômica.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Pode ser personalizada para praticamente qualquer aplicação;</li>
                <li>Menos complexo;</li>
                <li>Fácil instalação;</li>
                <li>Touch screen.</li>
            </ul>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Modulo de controle do queimador;</li>
                <li>Modulo do amplificador de chama UV;</li>
                <li>Modulo de controle da proporção ar combustível;</li>
                <li>Modulo de controle de limite;</li>
                <li>Modulo de anunciador;</li>
                <li>Modulo de I/O digital;</li>
                <li>Modulo de I/O analógico.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
