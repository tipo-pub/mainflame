<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA – SISTEMA DE COMBUSTÃO COM QUEIMADOR ECLIPSE TJ-100';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-sistema-combustao-queimador-eclipse-tj-100">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">11</span>
            <span class="month">abr</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-sistema-combustao-queimador-eclipse-tj-100">MANUTENÇÃO PREVENTIVA – SISTEMA DE COMBUSTÃO COM QUEIMADOR ECLIPSE TJ-100</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica do queimador instalado;</p>
            <p>– Inspeção mecânica do cavalete de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análises e ajustes da relação ar/gás;</p>
            <p>– Elaboração de relatório;</p>
            <p>– Curva de desempenho do queimador;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento da posta em marcha;2</p>

        </div>
    </article>

    

</div>


<?php include 'includes/footer.php' ;?>
