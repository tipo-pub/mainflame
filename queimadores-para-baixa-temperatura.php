<?php
include 'includes/geral.php';
$title = 'Queimadores Para Baixa Temperatura';
$description = 'Queimadores que proporcionam uma descarga de alta velocidade saiba mais.';
$keywords = 'produtos, , a melhor ';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/queimador-baixa-temperatura.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/queimador-baixa-temperatura.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Queimadores que proporcionam uma descarga de alta velocidade que promove a agitação dentro do forno para melhorar a uniformidade da temperatura e a penetração da carga de trabalho.</p>

        </div>
    </div>
    
    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
