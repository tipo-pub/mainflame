<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO PREVENTIVA – CAVALETE DE GÁS NATURAL – CALDEIRA ALLBORG';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-cavalete-gas-nattural-caldeira-allborg">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">29</span>
            <span class="month">jul</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-cavalete-gas-nattural-caldeira-allborg">MANUTENÇÃO PREVENTIVA – CAVALETE DE GÁS NATURAL – CALDEIRA ALLBORG</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>

                <p>– Inspeção mecânica do queimador instalado;</p>

                <p>– Inspeção mecânica do cavalete de alimentação de gás;</p>

                <p>– Fornecimento e instalação das seguintes peças sobressalentes:</p>

                <p>– Elemento filtrante para filtro de gás;</p>

                <ul>
                    <li>Kit de reparo para válvula shut off;</li>
                    <li>Kit de reparo para válvula reguladora de pressão;</li>
                    <li>Kit de switches (chave fim de curso) para o servo motor Honeywell existente.</li>
                </ul>

                <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>

                <p>– Teste nas lógicas de segurança;</p>

                <p>– Análise e ajuste da relação ar/gás;</p>

                <p>– Elaboração de relatório;</p>

                <p>– Curva de desempenho do queimador;</p>

                <p>– Teste de operação;</p>
                
                <p>– Acompanhamento de posta em marcha.</p>
                
            </div>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/cavalete-gas-nattural-caldeira-allborg/cavalete-gas-nattural-caldeira-allborg-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/cavalete-gas-nattural-caldeira-allborg/thumbs/cavalete-gas-nattural-caldeira-allborg-07.jpg" alt="<?=$title;?>">
            </a>
        </div>        
    </div>

</div>


<?php include 'includes/footer.php' ;?>
