<?php
if(!empty($menuTopo)){
    $classe_LI = 'class="dropdown"';
    $classe_LI_A = 'class="dropdown-item dropdown-toggle"';
    $classe_UL_SubMenu =  'class="dropdown-menu bg-light"';
    $classe_LI_UL_LI_A = 'class="dropdown-item"';
    $LI_mapa_site = false;
} elseif(!empty($menuRodape)) {
    $classe_LI = '';
    $classe_LI_A = 'class="text-4 link-hover-style-1"';
    $classe_UL_SubMenu =  '';
    $classe_LI_UL_LI_A = '';
    $LI_mapa_site = true;
    $submenu_mapa_site = false;
} elseif(!empty($menuMapaSite)) {
    $classe_LI = '';
    $classe_LI_A = '';
    $classe_UL_SubMenu =  '';
    $classe_LI_UL_LI_A = '';
    $LI_mapa_site = false;
    $submenu_mapa_site = true;
}
?>

<li><a href="<?=$url;?>" <?php echo $classe_LI_A?> title="Home">Home</a></li>
<li><a href="<?=$url;?>produtos" <?php echo $classe_LI_A?> title="Produtos">Produtos</a>
    <ul <?php echo $classe_UL_SubMenu?>>
        <li><a href="<?=$url?>aparelho-programador-chama" title="Aparelho Programador de Chama">Aparelho Programador de Chama</a></li>
        <li><a href="<?=$url?>sensor-de-chama" title="Sensor de Chama">Sensor de Chama</a></li>
        <li><a href="<?=$url?>valvula-proporcionadora-ar-gas-s" title="Válvula Proporcionadora Ar/Gás">Válvula Proporcionadora Ar/Gás</a></li>
        <li><a href="<?=$url?>queimadores-para-baixa-temperatura" title="Queimadores para Baixa Temperatura">Queimadores para Baixa Temperatura</a></li>
        <li><a href="<?=$url?>queimadores-para-alta-temperatura" title="Queimadores para Alta Temperatura">Queimadores para Alta Temperatura</a></li>
        <li><a href="<?=$url?>queimador-piloto" title="Queimador Piloto">Queimador Piloto</a></li>
        <li><a href="<?=$url?>servo-motor" title="Servo Motor">Servo Motor</a></li>
        <li><a href="<?=$url?>valvula-borboleta" title="Válvula Borboleta">Válvula Borboleta</a></li>
        <li><a href="<?=$url?>painel-de-comando-e-controle" title="Painel de Comando e Controle">Painel de Comando e Controle</a></li>
        <li><a href="<?=$url?>transformador-de-ignicao" title="Transformador de Ignição">Transformador de Ignição</a></li>
        <li><a href="<?=$url?>medidor-vazao" title="Medidor de Vazão">Medidor de Vazão</a></li>
        <li><a href="<?=$url?>valvula-esfera" title="Válvula de Esfera">Válvula de Esfera</a></li>
        <li><a href="<?=$url?>filtro-para-gas" title="Filtro para Gás">Filtro para Gás</a></li>
        <li><a href="<?=$url?>valvula-reguladora-pressao-de-gas" title="Válvula Reguladora de Pressão de Gás">Válvula Reguladora de Pressão de Gás</a></li>
        <li><a href="<?=$url?>valvula-de-alivio" title="Válvula de  Alivio">Válvula de  Alivio</a></li>
        <li><a href="<?=$url?>valvula-reguladora-pressao-shut-off" title="Válvula de Bloqueio por Sobrepressão - SHUT OFF">Válvula de Bloqueio por Sobrepressão - SHUT OFF</a></li>
        <li><a href="<?=$url?>valvula-solenoide-de-bloqueio-automatico" title="Válvula Solenoide de Bloqueio Automatico">Válvula Solenoide de Bloqueio Automatico</a></li>
        <li><a href="<?=$url?>valvula-bloqueio-com-atuador-eletromecanico-ou-pneumatico" title="Válvula de Bloqueio com Atuador Eletromecânico ou Pneumático">Válvula de Bloqueio com Atuador Eletromecânico ou Pneumático</a></li>
        <li><a href="<?=$url?>valvula-solenoide-bloqueio-vent" title="Válvula Solenoide de Descarga Automática "Vent"">Válvula Solenoide de Descarga Automática "Vent"</a></li>
        <li><a href="<?=$url?>borbulhador" title="Borbulhador">Borbulhador</a></li>
        <li><a href="<?=$url?>pressostato-de-gas-ar" title="Pressostato de Gás e Ar">Pressostato de Gás e Ar</a></li>
        <li><a href="<?=$url?>manometro" title="Manômetro">Manômetro</a></li>
        <li><a href="<?=$url?>registro-para-manometro" title="Registro para Manômetro">Registro para Manômetro</a></li>
        <li><a href="<?=$url?>slate" title="Slate">Slate</a></li>
    </ul>
</li>
<li><a href="<?=$url;?>servicos" <?php echo $classe_LI_A?> title="Serviços">Serviços</a>
    <ul <?php echo $classe_UL_SubMenu?>>
        <li><a href="<?=$url?>manutencao" title="Manutenção">Manutenção</a></li>
        <li><a href="<?=$url?>engenharia-consultoria" title="Engenharia / Consultoria">Engenharia / Consultoria</a></li>
        <li><a href="<?=$url?>projetos-desenvolvidos" title="Projetos Desenvolvidos">Projetos Desenvolvidos</a></li>
        <li><a href="<?=$url?>assistencia-tecnica" title="Assistência Técnica">Assistência Técnica</a></li>
    </ul>
</li>
<li><a href="<?=$url;?>projetos" <?php echo $classe_LI_A?> title="Projetos">Projetos</a>
    <ul <?php echo $classe_UL_SubMenu?>>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-maxon-valupak-300" title="Manutenção Preventiva - Queimadores Maxon Valupak 300">Manutenção Preventiva - Queimadores Maxon Valupak 300</a></li>
        <li><a href="<?=$url?>comissionamento-queimador-baltur" title="Comissionamento Queimador Baltur">Comissionamento Queimador Baltur</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-painel-eletrico-forno-fusao-vidro" title="Manutenção Corretiva no Painel Elétrico do Forno de Fusão de Vídro">Manutenção Corretiva no Painel Elétrico do Forno de Fusão de Vídro</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-estufas-secagem" title="Manutenção Preventiva nas Estufas de Secagem">Manutenção Preventiva nas Estufas de Secagem</a></li>
        <li><a href="<?=$url?>queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807" title="Queimador, Cavalete de Gás Natural, Painel de Comando, Instrumentação de Campo para o Sistema de Combustão da Caldeira ATA MP-807">Queimador, Cavalete de Gás Natural, Painel de Comando, Instrumentação de Campo para o Sistema de Combustão da Caldeira ATA MP-807</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-impressora-vtv-11-queimador-maxon" title="Manutenção Preventiva Impressora VTV-11 - Queimador Maxon">Manutenção Preventiva Impressora VTV-11 - Queimador Maxon</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-boiler-weco-queimador-oertli" title="Manutenção Preventiva Boiler Weco - Queimador Oertli">Manutenção Preventiva Boiler Weco - Queimador Oertli</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-cavalete-gas-nattural-caldeira-allborg" title="Manutenção Preventiva - Cavalete de Gás Natural - Caldeira Aalborg">Manutenção Preventiva - Cavalete de Gás Natural - Caldeira Aalborg</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-sistema-combustao-instalados-estufas-boiler" title="Manutenção Preventiva - Sistemas de Combustão Instalados nas Estufas e no Boiler">Manutenção Preventiva - Sistemas de Combustão Instalados nas Estufas e no Boiler</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-forno-recozimento-tubos-otto-junker-queimadores-widemann" title="Manutenção Preventiva - Forno de Recozimento de Tubos Otto Junker - Queimadores Wiedemann">Manutenção Preventiva - Forno de Recozimento de Tubos Otto Junker - Queimadores Wiedemann</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-sistemas-combustao" title="Manutenção Preventiva - Queimadores e Sistemas de Combustão">Manutenção Preventiva - Queimadores e Sistemas de Combustão</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-fornalha-planta-termica-queimadores-coen" title="Manutenção Preventiva - Fornalha da Planta Térmica - Queimadores Coen">Manutenção Preventiva - Fornalha da Planta Térmica - Queimadores Coen</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-queimadores-setor-pintura" title="Manutenção Corretiva - Queimadores do Setor de Pintura">Manutenção Corretiva - Queimadores do Setor de Pintura</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-fundacao-butantan" title="Manutenção Preventiva">Manutenção Preventiva</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimador-maxon-ovenpakebc-4sp" title="Manutenção Preventiva - Queimador Maxon Ovenpak EBC-4SP">Manutenção Preventiva - Queimador Maxon Ovenpak EBC-4SP</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-sistema-combustao-instalado-spray-dryer" title="Manutenção Corretiva - Sistema de Combustão Instalado no Spray-Dryer">Manutenção Corretiva - Sistema de Combustão Instalado no Spray-Dryer</a></li>
        <li><a href="<?=$url?>manutencao corretiva-caldeira-ata-mp-10-b02u" title="Manutenção Corretiva - Caldeira ATA MP-10 - B02U">Manutenção Corretiva - Caldeira ATA MP-10 - B02U</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-maxon" title="Manutenção Preventiva - Queimadores Maxon">Manutenção Preventiva - Queimadores Maxon</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-caldeira" title="Manutenção Preventiva - Queimador da Caldeira">Manutenção Preventiva - Queimador da Caldeira</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-queimador-maxon-valupak" title="Manutenção Corretiva - Queimador Maxon Valupak">Manutenção Corretiva - Queimador Maxon Valupak</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-rexam-extrema-mg" title="Manutenção Preventiva">Manutenção Preventiva</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-industriais" title="Manutenção Preventiva - Queimadores Industriaisa">Manutenção Preventiva - Queimadores Industriais</a></li>
        <li><a href="<?=$url?>manutencao-queimadores-ray-burners" title="manutenção - Queimadores Ray Burners">manutenção - Queimadores Ray Burners</a></li>
        <li><a href="<?=$url?>painel-comando-controle-seguranca-sistema-combustao-operacao-09-cavaletes-alimentacao-gas-natural" title="Painel de Comando, Controle e Segurança do Sistema de Combustão, para Operação de 09 Cavaletes de Alimentação de Gás Natural">Painel de Comando, Controle e Segurança do Sistema de Combustão, para Operação de 09 Cavaletes de Alimentação de Gás Natural</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-john-deere" title="Manutenção Corretiva">Manutenção Corretiva</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-instalados" title="Manutenção Preventiva - Queimadores Instalados">Manutenção Preventiva - Queimadores Instalados</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimador-maxon-ovenpak" title="Manutenção Preventiva - Queimador Maxon Ovenpak">Manutenção Preventiva - Queimador Maxon Ovenpak</a></li>
        <li><a href="<?=$url?>ajuste-regulagem-queimadores-forno-banho-zinco" title="Ajuste/Regulagem de Queimadores - Forno de Banho de Zinco">Ajuste/Regulagem de Queimadores - Forno de Banho de Zinco</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-caldeira-aalborg" title="Manutenção Corretiva Caldeira Aalborg">Manutenção Corretiva Caldeira Aalborg</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-sistema-combustao-queimador-eclipse-tj-100" title="Manutenção Preventiva - Sistema de Combustão com Queimador Eclipse TJ-100">Manutenção Preventiva - Sistema de Combustão com Queimador Eclipse TJ-100</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-maxon-ovenpak" title="Manutenção Preventiva - Queimadores Maxon Ovenpak">Manutenção Preventiva - Queimadores Maxon Ovenpak</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-link-steel" title="Manutenção Corretiva">Manutenção Corretiva</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimadores-maxon-rexam-df" title="Manutenção Preventiva dos Queimadores Maxon - Rexam - DF">Manutenção Preventiva dos Queimadores Maxon - Rexam - DF</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimador-thermotec" title="Manutenção Preventiva Queimador Thermotec">Manutenção Preventiva Queimador Thermotec</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-maxion-wheels" title="Manutenção Corretiva">Manutenção Corretiva</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-forno-tratamento-termico-sermep" title="Manutenção Preventiva Forno de Tratamento Térmico Sermep">Manutenção Preventiva Forno de Tratamento Térmico Sermep</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimador-maxon-ovenpak-melhoramentos" title="Manutenção Preventiva - Queimador Maxon Ovenpak">Manutenção Preventiva - Queimador Maxon Ovenpak</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-caldeira-b02u" title="Manutenção Corretiva Caldeira B02U">Manutenção Corretiva Caldeira B02U</a></li>
        <li><a href="<?=$url?>comissionamento-queimadores-maxon" title="Comissionamento Queimadores Maxon">Comissionamento Queimadores Maxon</a></li>
        <li><a href="<?=$url?>queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas" title="Queimador, Cavalete de GLP e Painel para Estufa Contínua de Pintura de Válvulas">Queimador, Cavalete de GLP e Painel para Estufa Contínua de Pintura de Válvulas</a></li>
        <li><a href="<?=$url?>manutencao-corretiva-freudenberg" title="Manutenção Corretiva - Queimador Thermotec">Manutenção Corretiva - Queimador Thermotec</a></li>
        <li><a href="<?=$url?>servico-especializado-montagem-painel-comando" title="Serviço Especializado de Montagem de Painel de Comando">Serviço Especializado de Montagem de Painel de Comando</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-queimador-maxon" title="Manutenção Preventiva Queimador Maxon">Manutenção Preventiva Queimador Maxon</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-mars" title="Manutenção Preventiva">Manutenção Preventiva</a></li>
        <li><a href="<?=$url?>manutencao-preventiva-weg" title="Manutenção Preventiva">Manutenção Preventiva</a></li>
    </ul>
</li>
<li <?php echo $classe_LI;?>><a href="<?=$url;?>institucional" <?php echo $classe_LI_A;?> title="Institucional">Institucional</a>
    <ul <?php echo $classe_UL_SubMenu?>>
        <li><a href="<?=$url;?>pdf/folder-mainflame-alta.pdf" target="_blank" <?php echo $classe_LI_UL_LI_A?> title="Portfolio">Portfolio</a></li>
        <li><a href="<?=$url;?>parceiros" <?php echo $classe_LI_UL_LI_A?> title="Parceiros">Parceiros</a></li>
        <!-- <li><a href="<?=$url;?>representantes" <?php echo $classe_LI_UL_LI_A?> title="Representantes">Representantes</a></li> -->
        <li><a href="<?=$url;?>clientes" <?php echo $classe_LI_UL_LI_A?> title="Clientes">Clientes</a></li>
        <li><a href="<?=$url;?>noticias" <?php echo $classe_LI_UL_LI_A?> title="Notícias">Notícias</a></li>
    </ul>
</li>
<li <?php echo $classe_LI;?>><a href="<?=$url;?>galeria" <?php echo $classe_LI_A;?> title="Galeria">Galeria</a>
    <ul <?php echo $classe_UL_SubMenu?>>
        <li><a href="<?=$url;?>fotos" <?php echo $classe_LI_UL_LI_A?> title="Fotos">Fotos</a></li>
        <li><a href="<?=$url;?>videos" <?php echo $classe_LI_UL_LI_A?> title="Vídeos">Vídeos</a></li>
    </ul>
</li>
<?php if(empty($menuRodape)) {?>
<li><a href="<?=$url;?>contato" <?php echo $classe_LI_A?> title="Contato">Contato</a></li>
<?php } if(!empty($LI_mapa_site)) {?>
</ul>
<ul class="lista-rodape">
    <li><a href="<?=$url;?>contato" <?php echo $classe_LI_A?> title="Contato">Contato</a></li>
    <li><a href="<?=$url;?>mapa-site" <?php echo $classe_LI_A?> title="Mapa do Site">Mapa do Site</a></li>
<?php } elseif(!empty($submenu_mapa_site)) {?>
</ul>
<ul class="lista-mapa-site">
    <li><a href="<?=$url;?>mapa-site" <?php echo $classe_LI_A?> title="Outros Produtos">Outros Produtos</a>
        <ul>
            <?php
            $keys = array_rand($LinksPalavras, 192);

            $i = 0;
            foreach ($keys as $link) {
                $palavra = $LinksPalavras[$link];
                echo "<li><a href=\"$url$link\" title=\"$palavra\">$palavra</a></li>";

                $i++;
            }
            ?>
        </ul>
    </li>
<?php }?>