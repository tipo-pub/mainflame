<?php if(isset($urlGaleria) && ($urlGaleria == 'index') || ($urlGaleria = '')) {?>
<div role="main" class="main">
	<div class="slider-container rev_slider_wrapper">
		<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 9000, 'gridwidth': 1170, 'gridheight': 700, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500], 'parallax': { 'type': 'scroll', 'origo': 'enterpoint', 'speed': 1000, 'levels': [2,3,4,5,6,7,8,9,12,50], 'disable_onmobile': 'on' }}">
			<ul>
				<li class="slide-overlay" data-transition="fade">
					<img src="img/banner-01.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

					<div class="tp-caption" data-x="center" data-hoffset="['-165','-165','-165','-215']" data-y="center" data-voffset="['-110','-110','-110','-135']" data-start="1000" data-transform_in="x:[-300%];opacity:0;s:500;" data-transform_idle="opacity:0.2;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center" data-voffset="['-110','-110','-110','-135']" data-start="700" data-fontsize="['22','22','22','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;">Veja todos os projetos</div>

					<div class="tp-caption" data-x="center" data-hoffset="['165','165','165','215']" data-y="center" data-voffset="['-110','-110','-110','-135']" data-start="1000" data-transform_in="x:[300%];opacity:0;s:500;" data-transform_idle="opacity:0.2;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<h1 class="tp-caption font-weight-extra-bold text-uppercase text-color-light negative-ls-2" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-60','-60','-60','-85']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Desenvolvidos pela Mainflame</h1>

					<div class="tp-caption font-weight-light text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2500,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-10','-10','-10','-25']" data-fontsize="['18','18','18','50']" data-lineheight="['29','29','29','40']" style="color: #ffffff;">como manutenção preventiva, manutenção corretiva e assistência técnica... </div>

					<a class="tp-caption btn btn-light font-weight-bold text-uppercase text-color-primary" href="#" data-frames='[{"delay":5500,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"y:0;o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['65','65','65','210']" data-paddingtop="['15','15','15','30']" data-paddingbottom="['15','15','15','30']" data-paddingleft="['33','33','33','50']" data-paddingright="['33','33','33','50']" data-fontsize="['13','13','13','25']" data-lineheight="['20','20','20','25']">Conheça nossos Projetos</a>
				</li>
				<li class="slide-overlay" data-transition="fade">
					<img src="img/banner-02.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

					<div class="tp-caption font-weight-extra-bold text-uppercase text-color-light negative-ls-2" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-60','-60','-60','-85']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Conheça nossos Serviços</div>

					<div class="tp-caption font-weight-light text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2800,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-5','-5','-5','-20']" data-fontsize="['18','18','18','50']" data-lineheight="['29','29','29','40']" style="color: #ffffff;">Manutenção - Engenharia &amp; Consultoria - Assistência Técnica</div>

					<a class="tp-caption btn btn-primary text-uppercase font-weight-bold" href="#" data-frames='[{"delay":5000,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"y:0;o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['70','70','70','215']" data-paddingtop="['15','15','15','30']" data-paddingbottom="['15','15','15','30']" data-paddingleft="['33','33','33','50']" data-paddingright="['33','33','33','50']" data-fontsize="['13','13','13','25']" data-lineheight="['20','20','20','25']">Clique Aqui</a>
				</li>
				<li class="slide-overlay" data-transition="fade">
					<img src="img/banner-03.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

					<div class="tp-caption font-weight-extra-bold text-uppercase text-color-light negative-ls-2" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-60','-60','-60','-85']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Conheça nossos Produtos</div>

					<div class="tp-caption font-weight-light text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2800,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-5','-5','-5','-20']" data-fontsize="['18','18','18','50']" data-lineheight="['29','29','29','40']" style="color: #ffffff;">Variedade de peças para manutenção e correção preventiva em seu aparelho.</div>

					<a class="tp-caption btn btn-light font-weight-bold text-uppercase text-color-primary" href="#" data-frames='[{"delay":5500,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"y:0;o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['70','70','70','215']" data-paddingtop="['15','15','15','30']" data-paddingbottom="['15','15','15','30']" data-paddingleft="['33','33','33','50']" data-paddingright="['33','33','33','50']" data-fontsize="['13','13','13','25']" data-lineheight="['20','20','20','25']">Confira</a>
				</li>

				<li class="slide-overlay" data-transition="fade">
					<img src="img/blank.gif" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

					<div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-videowidth="100%" data-videoheight="100%" data-videomp4="video/caldeira_awn_20_mars.mp4" data-videopreload="preload" data-videoloop="loop" data-forceCover="1" data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="false" data-nextslideatend="false"></div>

					<div class="tp-caption font-weight-extra-bold text-uppercase text-color-primary negative-ls-2" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-120','-120','-120','-135']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']"><?=$NomeEmpresa?></div>

					<div class="tp-caption font-weight-bold text-color-primary text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2500,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-70','-70','-70','-85']" data-fontsize="['18','18','18','50']" data-lineheight="['29','29','29','40']"><?=$ramo?></div>

					<div class="tp-caption font-weight-normal text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":3500,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-10','-10','-10','-25']" data-fontsize="['18','18','18','50']" data-lineheight="['29','29','29','40']"><a href="tel:<?=$linktel;?>" class="text-color-primary d-inline-block">(<?=$ddd;?>) <?=$tel;?></a> <a href="tel:<?=$linktel2;?>" class="text-color-primary d-inline-block">(<?=$ddd;?>) <?=$tel2;?></a> <a href="<?=$linkWhats;?>" class="text-color-primary d-inline-block">(<?=$ddd;?>) <?=$whats;?></a> <br /><a href="mailto:<?=$email;?>" class="text-color-primary d-inline-block"><?=$email;?></a></div>

					<a class="tp-caption btn btn-primary text-uppercase font-weight-bold" href="#" data-frames='[{"delay":6800,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"y:0;o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['65','65','65','210']" data-paddingtop="['15','15','15','30']" data-paddingbottom="['15','15','15','30']" data-paddingleft="['33','33','33','50']" data-paddingright="['33','33','33','50']" data-fontsize="['13','13','13','25']" data-lineheight="['20','20','20','25']">Faça seu Orçamento</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<?php } else { ?>
<div class="container">
	<img src="<?=$url?>img/engenharia-manutencao/<?=$imagem?>" class="img-responsive" alt="<?=$title;?>" itemprop="image" />
	<?php /*
    <div class="col-md-4">

        <form class="p-20 m-r-20 form-palavras" role="form" method="post">
            
            <h3>Faça seu orçamento:</h3>

            <input type="hidden" name="form_orcamento" value="true" />

            <div class="form-group">
                <input type="text" class="form-control required name" placeholder="Nome*" aria-required="true" name="nome" value="<?php echo isset($_POST['nome']) && !empty($_POST['nome']) ? $_POST['nome'] : '';?>" />
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <input type="text" class="form-control required" placeholder="Telefone" aria-required="true" name="telefone" value="<?php echo isset($_POST['telefone']) && !empty($_POST['telefone']) ? $_POST['telefone'] : '';?>" />
                </div>

                <div class="form-group col-md-6">
                    <input type="email" class="form-control required email" placeholder="E-mail*" aria-required="true" name="email" value="<?php echo isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : '';?>" />
                </div>
            </div>

            <div class="form-group">
                <textarea class="form-control required" rows="6" placeholder="Mensagem*" area-required="true" name="mensagem"><?php echo isset($_POST['mensagem']) && !empty($_POST['mensagem']) ? $_POST['mensagem'] : '';?></textarea>
            </div>

            <div class="form-group col-md-7" style="float: left;">
                <div class="g-recaptcha" data-sitekey="<?=$sitekey?>"></div>
            </div>

            <div class="form-group col-md-5 text-center" style="line-height: 78px; float: right;">
                <button type="submit" name="submit" value="submit" class="btn btn-dark">Enviar</button>
            </div>
            <?php if(isset($_POST['submit'])){ require_once('php/cadastro-form.php'); } ?>
        </form>
    </div>
    */?>
</div>
<?php } ?>