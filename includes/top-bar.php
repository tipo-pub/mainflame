<div class="header-top header-top-default">
    <div class="container">
        <div class="header-row">
            <div class="header-column justify-content-start">
                <div class="header-row">
                    <nav class="header-nav-top">
                        <ul class="nav nav-pills">
                            <li class="nav-item d-none d-md-block">
                                <a href="mailto:<?=$email;?>"><i class="fa fa-envelope text-grey"></i> <?=$email;?></a>
                            </li>                            
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="header-column justify-content-end">
                <div class="header-row">
                    <nav class="header-nav-top">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a href="tel:<?=$linktel;?>"><i class="fas fa-phone icone-invertido text-grey"></i> (<?=$ddd;?>) <?=$tel;?></a>
                            </li>
                            <li class="nav-item">
                                <a href="tel:<?=$linktel2;?>">(<?=$ddd;?>) <?=$tel2;?></a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=$linkWhats;?>"><i class="fab fa-whatsapp text-grey"></i> (<?=$ddd;?>) <?=$whats;?></a>
                            </li>                            
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
