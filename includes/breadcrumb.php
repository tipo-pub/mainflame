<section class="page-header page-header-modern page-header-md">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-1 text-center">
                        <div class="overflow-hidden pb-4">
                            <h1 class="text-light text-uppercase negative-ls-1"><?=$title;?></h1>
                        </div>
                    </div>
                    <div class="col-md-12 align-self-center order-2">
                        <ul class="breadcrumb breadcrumb-light d-block text-center">
                            <li><a href="<?=$url;?>">Home</a></li>
                            <li><a href="<?=$canonical;?>"><?=$title;?></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
