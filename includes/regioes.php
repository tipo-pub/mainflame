<?php

$substituir = array("");
$mudar = array("");

$keyregiao = str_replace($substituir, $mudar, $title);
?>
<div class="container py-4">
	<div class="row">
		<h3 class="title-divider text-uppercase">Onde Atendemos</h3>
		<div class="tabs">
			<ul class="nav nav-tabs">
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#SP">SP</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#RJ">RJ</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#MG">MG</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#PR">PR</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#SC">SC</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#RS">RS</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#GO">GO</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#PE">PE</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#CE">CE</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#BA">BA</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#Demais-Estados">Demais Estados/Capitais</a></li>
			</ul>

			<div class="tab-content">
				<div id="SP" class="tab-pane fade">
					<ul class="nav nav-tabs">
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#RegiaoCentral">Região Central</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#ZonaNorte">Zona Norte</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#ZonaSul">Zona Sul</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#ZonaLeste">Zona Leste</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#ZonaOeste">Zona Oeste</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#GdeSaoPaulo">Gde São Paulo</a></li>
						<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#Litoral">Litoral</a></li>
					</ul>

					<div class="tab-content">
						<div id="RegiaoCentral" class="tab-pane fade">
							<h4><?=$keyregiao;?> Na Região Central</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> na Aclimação</li>
								<li><strong><?=$keyregiao;?></strong> na Bela Vista</li>
								<li><strong><?=$keyregiao;?></strong> no Bom Retiro</li>
								<li><strong><?=$keyregiao;?></strong> no Brás</li>
								<li><strong><?=$keyregiao;?></strong> no Cambuci</li>
								<li><strong><?=$keyregiao;?></strong> no Centro</li>
								<li><strong><?=$keyregiao;?></strong> na Consolação</li>
								<li><strong><?=$keyregiao;?></strong> em Higienópolis</li>
								<li><strong><?=$keyregiao;?></strong> no Glicério</li>
								<li><strong><?=$keyregiao;?></strong> na Liberdade</li>
								<li><strong><?=$keyregiao;?></strong> na Luz</li>
								<li><strong><?=$keyregiao;?></strong> no Pari</li>
								<li><strong><?=$keyregiao;?></strong> na República</li>
								<li><strong><?=$keyregiao;?></strong> na Santa Cecília</li>
								<li><strong><?=$keyregiao;?></strong> na Santa Efigênia</li>
								<li><strong><?=$keyregiao;?></strong> na Sé</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Buarque</li>
							</ul>
						</div>
						<div id="ZonaNorte" class="tab-pane fade">
							<h4><?=$keyregiao;?> Na Zona Norte</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> na Brasilândia</li>
								<li><strong><?=$keyregiao;?></strong> em Cachoeirinha</li>
								<li><strong><?=$keyregiao;?></strong> na Casa Verde</li>
								<li><strong><?=$keyregiao;?></strong> no Imirim</li>
								<li><strong><?=$keyregiao;?></strong> no Jaçanã</li>
								<li><strong><?=$keyregiao;?></strong> no Jd São Paulo</li>
								<li><strong><?=$keyregiao;?></strong> em Lauzane</li>
								<li><strong><?=$keyregiao;?></strong> no Mandaqui</li>
								<li><strong><?=$keyregiao;?></strong> em Santana</li>
								<li><strong><?=$keyregiao;?></strong> no Tremembé</li>
								<li><strong><?=$keyregiao;?></strong> no Tucuruvi</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Guilherme</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Gustavo</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Maria</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Medeiros</li>
							</ul>
						</div>

						<div id="ZonaOeste" class="tab-pane fade">
							<h4><?=$keyregiao;?> Na Zona Oeste</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> na Água Branca</li>
								<li><strong><?=$keyregiao;?></strong> no Bairro do Limão</li>
								<li><strong><?=$keyregiao;?></strong> na Barra Funda</li>
								<li><strong><?=$keyregiao;?></strong> no Alto da Lapa</li>
								<li><strong><?=$keyregiao;?></strong> no Alto Pinheiros</li>
								<li><strong><?=$keyregiao;?></strong> no Butantã</li>
								<li><strong><?=$keyregiao;?></strong> na Freguesia do Ó</li>
								<li><strong><?=$keyregiao;?></strong> no Jaguaré</li>
								<li><strong><?=$keyregiao;?></strong> no Jaraguá</li>
								<li><strong><?=$keyregiao;?></strong> no Jd Bonfiglioli</li>
								<li><strong><?=$keyregiao;?></strong> na Lapa</li>
								<li><strong><?=$keyregiao;?></strong> no Pacaembú</li>
								<li><strong><?=$keyregiao;?></strong> em Perdizes</li>
								<li><strong><?=$keyregiao;?></strong> no Perús</li>
								<li><strong><?=$keyregiao;?></strong> em Pinheiros</li>
								<li><strong><?=$keyregiao;?></strong> em Pirituba</li>
								<li><strong><?=$keyregiao;?></strong> na Raposo</li>
								<li><strong><?=$keyregiao;?></strong> no Rio Pequeno</li>
								<li><strong><?=$keyregiao;?></strong> em São Domingos</li>
								<li><strong><?=$keyregiao;?></strong> no Sumaré</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Leopoldina</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Sonia</li>
							</ul>
						</div>

						<div id="ZonaSul" class="tab-pane fade">
							<h4><?=$keyregiao;?> Na Zona Sul</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> no Aeroporto</li>
								<li><strong><?=$keyregiao;?></strong> na Água Funda</li>
								<li><strong><?=$keyregiao;?></strong> no Brooklin</li>
								<li><strong><?=$keyregiao;?></strong> no Campo Belo</li>
								<li><strong><?=$keyregiao;?></strong> no Campo Grande</li>
								<li><strong><?=$keyregiao;?></strong> no Campo Limpo</li>
								<li><strong><?=$keyregiao;?></strong> no Capão Redondo</li>
								<li><strong><?=$keyregiao;?></strong> na Cidade Ademar</li>
								<li><strong><?=$keyregiao;?></strong> na Cidade Dutra</li>
								<li><strong><?=$keyregiao;?></strong> na Cidade Jardim</li>
								<li><strong><?=$keyregiao;?></strong> no Grajaú</li>
								<li><strong><?=$keyregiao;?></strong> no Ibirapuera</li>
								<li><strong><?=$keyregiao;?></strong> em Interlagos</li>
								<li><strong><?=$keyregiao;?></strong> no Ipiranga</li>
								<li><strong><?=$keyregiao;?></strong> no Itaim Bibi</li>
								<li><strong><?=$keyregiao;?></strong> no Jabaquara</li>
								<li><strong><?=$keyregiao;?></strong> no Jd Ângela</li>
								<li><strong><?=$keyregiao;?></strong> no Jd América</li>
								<li><strong><?=$keyregiao;?></strong> no Jd Europa</li>
								<li><strong><?=$keyregiao;?></strong> no Jd Paulista</li>
								<li><strong><?=$keyregiao;?></strong> no Jd Paulistano</li>
								<li><strong><?=$keyregiao;?></strong> no Jd São Luiz</li>
								<li><strong><?=$keyregiao;?></strong> no Jardins</li>
								<li><strong><?=$keyregiao;?></strong> no Jockey Club</li>
								<li><strong><?=$keyregiao;?></strong> no M'Boi Mirim</li>
								<li><strong><?=$keyregiao;?></strong> em Moema</li>
								<li><strong><?=$keyregiao;?></strong> no Morumbi</li>
								<li><strong><?=$keyregiao;?></strong> em Parelheiros</li>
								<li><strong><?=$keyregiao;?></strong> em Pedreira</li>
								<li><strong><?=$keyregiao;?></strong> no Sacomã</li>
								<li><strong><?=$keyregiao;?></strong> em Santo Amaro</li>
								<li><strong><?=$keyregiao;?></strong> na Saúde</li>
								<li><strong><?=$keyregiao;?></strong> em Socorro</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Andrade</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Mariana</li>
							</ul>
						</div>


						<div id="ZonaLeste" class="tab-pane fade">
							<h4><?=$keyregiao;?> Na Zona Leste</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> na Água Rasa</li>
								<li><strong><?=$keyregiao;?></strong> em Anália Franco</li>
								<li><strong><?=$keyregiao;?></strong> em Aricanduva</li>
								<li><strong><?=$keyregiao;?></strong> em Artur Alvim</li>
								<li><strong><?=$keyregiao;?></strong> em Belém</li>
								<li><strong><?=$keyregiao;?></strong> na Cidade Patriarca</li>
								<li><strong><?=$keyregiao;?></strong> na Cidade Tiradentes</li>
								<li><strong><?=$keyregiao;?></strong> Engenheiro Goulart</li>
								<li><strong><?=$keyregiao;?></strong> em Ermelino</li>
								<li><strong><?=$keyregiao;?></strong> em Guianazes</li>
								<li><strong><?=$keyregiao;?></strong> em Itaim Paulista</li>
								<li><strong><?=$keyregiao;?></strong> em Itaquera</li>
								<li><strong><?=$keyregiao;?></strong> em Jd Iguatemi</li>
								<li><strong><?=$keyregiao;?></strong> em José Bonifácio</li>
								<li><strong><?=$keyregiao;?></strong> na Moóca</li>
								<li><strong><?=$keyregiao;?></strong> no Pq do Carmo</li>
								<li><strong><?=$keyregiao;?></strong> no Pq São Lucas</li>
								<li><strong><?=$keyregiao;?></strong> no Pq São Rafael</li>
								<li><strong><?=$keyregiao;?></strong> na Penha</li>
								<li><strong><?=$keyregiao;?></strong> na Ponte Rasa</li>
								<li><strong><?=$keyregiao;?></strong> em São Mateus</li>
								<li><strong><?=$keyregiao;?></strong> em São Miguel</li>
								<li><strong><?=$keyregiao;?></strong> em Sapopemba</li>
								<li><strong><?=$keyregiao;?></strong> no Tatuapé</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Carrão</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Curuçá</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Esperança</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Formosa</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Matilde</li>
								<li><strong><?=$keyregiao;?></strong> na Vila Prudente</li>
							</ul>
						</div>


						<div id="GdeSaoPaulo" class="tab-pane fade">
							<h4><?=$keyregiao;?> Na Grande São Paulo</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> em São Caetano</li>
								<li><strong><?=$keyregiao;?></strong> em São Bernardo</li>
								<li><strong><?=$keyregiao;?></strong> em Santo André</li>
								<li><strong><?=$keyregiao;?></strong> em Diadema</li>
								<li><strong><?=$keyregiao;?></strong> em Guarulhos</li>
								<li><strong><?=$keyregiao;?></strong> em Suzano</li>
								<li><strong><?=$keyregiao;?></strong> em Ribeirão Pires</li>
								<li><strong><?=$keyregiao;?></strong> em Mauá</li>
								<li><strong><?=$keyregiao;?></strong> em Embu</li>
								<li><strong><?=$keyregiao;?></strong> em Embu Guaçú</li>
								<li><strong><?=$keyregiao;?></strong> em Embu das Artes</li>
								<li><strong><?=$keyregiao;?></strong> em Itapecerica</li>
								<li><strong><?=$keyregiao;?></strong> em Osasco</li>
								<li><strong><?=$keyregiao;?></strong> em Barueri</li>
								<li><strong><?=$keyregiao;?></strong> em Jandira</li>
								<li><strong><?=$keyregiao;?></strong> em Cotia</li>
								<li><strong><?=$keyregiao;?></strong> em Itapevi</li>
								<li><strong><?=$keyregiao;?></strong> Santana de Parnaíba</li>
								<li><strong><?=$keyregiao;?></strong> em Caierias</li>
								<li><strong><?=$keyregiao;?></strong> em Franco da Rocha</li>
								<li><strong><?=$keyregiao;?></strong> em Taboão da Serra</li>
								<li><strong><?=$keyregiao;?></strong> em Cajamar</li>
								<li><strong><?=$keyregiao;?></strong> em Arujá</li>
								<li><strong><?=$keyregiao;?></strong> em Alphaville</li>
								<li><strong><?=$keyregiao;?></strong> em Mairiporã</li>
							</ul>
						</div>

						<div id="Litoral" class="tab-pane fade">
							<h4><?=$keyregiao;?> No Litoral</h4>
							<ul class="regioes-tabs">
								<li><strong><?=$keyregiao;?></strong> em Bertioga</li>
								<li><strong><?=$keyregiao;?></strong> em Cananéia</li>
								<li><strong><?=$keyregiao;?></strong> em Caraguatatuba</li>
								<li><strong><?=$keyregiao;?></strong> em Cubatão</li>
								<li><strong><?=$keyregiao;?></strong> no Guarujá</li>
								<li><strong><?=$keyregiao;?></strong> em Ilha Comprida</li>
								<li><strong><?=$keyregiao;?></strong> em Iguape</li>
								<li><strong><?=$keyregiao;?></strong> em Ilhabela</li>
								<li><strong><?=$keyregiao;?></strong> em Itanhaém</li>
								<li><strong><?=$keyregiao;?></strong> em Mongaguá</li>
								<li><strong><?=$keyregiao;?></strong> em Riviera</li>
								<li><strong><?=$keyregiao;?></strong> em Santos</li>
								<li><strong><?=$keyregiao;?></strong> em São Vicente</li>
								<li><strong><?=$keyregiao;?></strong> em Praia Grande</li>
								<li><strong><?=$keyregiao;?></strong> em Ubatuba</li>
								<li><strong><?=$keyregiao;?></strong> em São Sebastião</li>
								<li><strong><?=$keyregiao;?></strong> em Peruíbe</li>
							</ul>
						</div>
					</div>
				</div>
				<div id="Demais-Estados" class="tab-pane fade">
					<h4><?=$keyregiao;?> em Demais Estados/Capitais</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Rio Branco - AC</li>
						<li><strong><?=$keyregiao;?></strong> em Campo Grande - MS</li>
						<li><strong><?=$keyregiao;?></strong> em Cuiabá - MT</li>
						<li><strong><?=$keyregiao;?></strong> em Maceió - AL</li>
						<li><strong><?=$keyregiao;?></strong> em Vitória - ES</li>
						<li><strong><?=$keyregiao;?></strong> no Macapá - AP</li>
						<li><strong><?=$keyregiao;?></strong> em Manaus - AM</li>
						<li><strong><?=$keyregiao;?></strong> em Aracaju - SE</li>
						<li><strong><?=$keyregiao;?></strong> em Palmas - TO</li>
						<li><strong><?=$keyregiao;?></strong> em Brasília - DF</li>
						<li><strong><?=$keyregiao;?></strong> em São Luís - MA</li>
						<li><strong><?=$keyregiao;?></strong> em Belém - PA</li>
						<li><strong><?=$keyregiao;?></strong> em João Pessoa - PB</li>
						<li><strong><?=$keyregiao;?></strong> em Teresina - PI</li>
						<li><strong><?=$keyregiao;?></strong> em Natal - RN</li>
						<li><strong><?=$keyregiao;?></strong> em Porto Velho - RO</li>
						<li><strong><?=$keyregiao;?></strong> em Boa Vista - RR</li>
					</ul>
				</div>
				<div id="PE" class="tab-pane fade">
					<h4><?=$keyregiao;?> em Pernambuco</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> no Recife</li>
						<li><strong><?=$keyregiao;?></strong> em Jaboatão dos Guararapes</li>
						<li><strong><?=$keyregiao;?></strong> em Olinda</li>
						<li><strong><?=$keyregiao;?></strong> em Bandeira Caruaru</li>
						<li><strong><?=$keyregiao;?></strong> em Caruaru</li>
						<li><strong><?=$keyregiao;?></strong> em Petrolina</li>
						<li><strong><?=$keyregiao;?></strong> em Paulista</li>
						<li><strong><?=$keyregiao;?></strong> em Cabo de Santo Agostinho</li>
						<li><strong><?=$keyregiao;?></strong> em Camaragibe</li>
						<li><strong><?=$keyregiao;?></strong> em Garanhuns</li>
						<li><strong><?=$keyregiao;?></strong> em Vitória de Santo Antão</li>
						<li><strong><?=$keyregiao;?></strong> em Igarassu</li>
						<li><strong><?=$keyregiao;?></strong> em São Lourenço da Mata</li>
						<li><strong><?=$keyregiao;?></strong> em Santa Cruz do Capibaribe</li>
						<li><strong><?=$keyregiao;?></strong> em Abreu e Lima</li>
						<li><strong><?=$keyregiao;?></strong> em Ipojuca</li>
						<li><strong><?=$keyregiao;?></strong> em Serra Talhada</li>
						<li><strong><?=$keyregiao;?></strong> em Araripina</li>
						<li><strong><?=$keyregiao;?></strong> em Gravatá</li>
						<li><strong><?=$keyregiao;?></strong> em Carpina</li>
						<li><strong><?=$keyregiao;?></strong> em Goiana</li>
						<li><strong><?=$keyregiao;?></strong> em Belo Jardim</li>
						<li><strong><?=$keyregiao;?></strong> em Arcoverde</li>
						<li><strong><?=$keyregiao;?></strong> em Ouricuri</li>
						<li><strong><?=$keyregiao;?></strong> em Escada</li>
						<li><strong><?=$keyregiao;?></strong> em Pesqueira</li>
						<li><strong><?=$keyregiao;?></strong> em Surubim</li>
						<li><strong><?=$keyregiao;?></strong> em Palmares</li>
						<li><strong><?=$keyregiao;?></strong> em Moreno</li>
						<li><strong><?=$keyregiao;?></strong> em Bezerros</li>
						<li><strong><?=$keyregiao;?></strong> em Salgueiro</li>
						<li><strong><?=$keyregiao;?></strong> em São Bento do Una</li>
						<li><strong><?=$keyregiao;?></strong> em Buíque</li>
						<li><strong><?=$keyregiao;?></strong> em Limoeiro</li>
						<li><strong><?=$keyregiao;?></strong> em Paudalho</li>
						<li><strong><?=$keyregiao;?></strong> em Timbaúba</li>
					</ul>
				</div>

				<div id="CE" class="tab-pane fade">
					<h4><?=$keyregiao;?> em Ceará</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Fortaleza</li>
						<li><strong><?=$keyregiao;?></strong> em Caucaia</li>
						<li><strong><?=$keyregiao;?></strong> em Juazeiro do Norte</li>
						<li><strong><?=$keyregiao;?></strong> em Maracanaú</li>
						<li><strong><?=$keyregiao;?></strong> em Sobral</li>
						<li><strong><?=$keyregiao;?></strong> em Crato</li>
						<li><strong><?=$keyregiao;?></strong> em Itapipoca</li>
						<li><strong><?=$keyregiao;?></strong> em Maranguape</li>
						<li><strong><?=$keyregiao;?></strong> em Iguatu</li>
						<li><strong><?=$keyregiao;?></strong> em Quixadá</li>
						<li><strong><?=$keyregiao;?></strong> em Pacatuba</li>
						<li><strong><?=$keyregiao;?></strong> em Aquiraz</li>
						<li><strong><?=$keyregiao;?></strong> em Quixeramobim</li>
						<li><strong><?=$keyregiao;?></strong> no Canindé</li>
						<li><strong><?=$keyregiao;?></strong> em Russas</li>
						<li><strong><?=$keyregiao;?></strong> em Crateús</li>
						<li><strong><?=$keyregiao;?></strong> em Tianguá</li>
						<li><strong><?=$keyregiao;?></strong> em Aracati</li>
						<li><strong><?=$keyregiao;?></strong> em Cascavel</li>
						<li><strong><?=$keyregiao;?></strong> em Pacajus</li>
						<li><strong><?=$keyregiao;?></strong> em Icó</li>
						<li><strong><?=$keyregiao;?></strong> no Horizonte</li>
						<li><strong><?=$keyregiao;?></strong> em Camocim</li>
						<li><strong><?=$keyregiao;?></strong> em Morada Nova</li>
						<li><strong><?=$keyregiao;?></strong> em Acaraú</li>
						<li><strong><?=$keyregiao;?></strong> em Viçosa do Ceará</li>
						<li><strong><?=$keyregiao;?></strong> em Barbalha</li>
						<li><strong><?=$keyregiao;?></strong> em Limoeiro do Norte</li>
						<li><strong><?=$keyregiao;?></strong> em Tauá</li>
						<li><strong><?=$keyregiao;?></strong> em Trairi</li>
						<li><strong><?=$keyregiao;?></strong> na Granja</li>
						<li><strong><?=$keyregiao;?></strong> em Boa Viagem</li>
						<li><strong><?=$keyregiao;?></strong> em Acopiara</li>
						<li><strong><?=$keyregiao;?></strong> em Beberibe</li>
						<li><strong><?=$keyregiao;?></strong> em Eusébio</li>
					</ul>
				</div>

				<div id="BA" class="tab-pane fade">
					<h4><?=$keyregiao;?> na Bahia</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Salvador</li>
						<li><strong><?=$keyregiao;?></strong> em Feira de Santana</li>
						<li><strong><?=$keyregiao;?></strong> em Vitória da Conquista</li>
						<li><strong><?=$keyregiao;?></strong> em Camaçari</li>
						<li><strong><?=$keyregiao;?></strong> em Itabuna</li>
						<li><strong><?=$keyregiao;?></strong> em Juazeiro</li>
						<li><strong><?=$keyregiao;?></strong> em Lauro de Freitas</li>
						<li><strong><?=$keyregiao;?></strong> em Ilhéus</li>
						<li><strong><?=$keyregiao;?></strong> em Jequié</li>
						<li><strong><?=$keyregiao;?></strong> em Teixeira de Freitas</li>
						<li><strong><?=$keyregiao;?></strong> em Alagoinhas</li>
						<li><strong><?=$keyregiao;?></strong> em Porto Seguro</li>
						<li><strong><?=$keyregiao;?></strong> em Simões Filho</li>
						<li><strong><?=$keyregiao;?></strong> em Paulo Afonso</li>
						<li><strong><?=$keyregiao;?></strong> em Eunápolis</li>
						<li><strong><?=$keyregiao;?></strong> em Santo Antônio de Jesus</li>
						<li><strong><?=$keyregiao;?></strong> em Valença</li>
						<li><strong><?=$keyregiao;?></strong> em Candeias</li>
						<li><strong><?=$keyregiao;?></strong> em Guanambi</li>
						<li><strong><?=$keyregiao;?></strong> em Jacobina</li>
						<li><strong><?=$keyregiao;?></strong> em Serrinha</li>
						<li><strong><?=$keyregiao;?></strong> em Senhor do Bonfim</li>
						<li><strong><?=$keyregiao;?></strong> em Luís Eduardo Magalhães</li>
						<li><strong><?=$keyregiao;?></strong> em Dias d'Ávila</li>
						<li><strong><?=$keyregiao;?></strong> em Itapetinga</li>
						<li><strong><?=$keyregiao;?></strong> em Irecê</li>
						<li><strong><?=$keyregiao;?></strong> em Campo Formoso</li>
						<li><strong><?=$keyregiao;?></strong> em Casa Nova</li>
						<li><strong><?=$keyregiao;?></strong> em Bom Jesus da Lapa</li>
						<li><strong><?=$keyregiao;?></strong> em Brumado</li>
						<li><strong><?=$keyregiao;?></strong> em Conceição do Coité</li>
						<li><strong><?=$keyregiao;?></strong> em Itamaraju</li>
						<li><strong><?=$keyregiao;?></strong> em Itaberaba</li>
						<li><strong><?=$keyregiao;?></strong> em Cruz das Almas</li>
						<li><strong><?=$keyregiao;?></strong> em Ipirá</li>
						<li><strong><?=$keyregiao;?></strong> em Santo Amaro</li>
						<li><strong><?=$keyregiao;?></strong> em Euclides da Cunha</li>
						<li><strong><?=$keyregiao;?></strong> em Araci</li>
						<li><strong><?=$keyregiao;?></strong> em Tucano</li>
						<li><strong><?=$keyregiao;?></strong> em Catu</li>
						<li><strong><?=$keyregiao;?></strong> em Jaguaquara</li>
						<li><strong><?=$keyregiao;?></strong> em Monte Santo</li>
						<li><strong><?=$keyregiao;?></strong> na Barra</li>
						<li><strong><?=$keyregiao;?></strong> em Santo Estêvão</li>
						<li><strong><?=$keyregiao;?></strong> em Caetité</li>
						<li><strong><?=$keyregiao;?></strong> em Ribeira do Pombal</li>
						<li><strong><?=$keyregiao;?></strong> em Macaúbas</li>
					</ul>
				</div>

				<div id="GO" class="tab-pane fade">
					<h4><?=$keyregiao;?> em Goias</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Goiânia</li>
						<li><strong><?=$keyregiao;?></strong> em Aparecida de Goiânia</li>
						<li><strong><?=$keyregiao;?></strong> em Anápolis</li>
						<li><strong><?=$keyregiao;?></strong> em Rio Verde</li>
						<li><strong><?=$keyregiao;?></strong> em Luziânia</li>
						<li><strong><?=$keyregiao;?></strong> em Águas Lindas de Goiás</li>
						<li><strong><?=$keyregiao;?></strong> em Trindade</li>
						<li><strong><?=$keyregiao;?></strong> em Formosa</li>
						<li><strong><?=$keyregiao;?></strong> em Novo Gama</li>
						<li><strong><?=$keyregiao;?></strong> em Itumbiara</li>
						<li><strong><?=$keyregiao;?></strong> em Senador Canedo</li>
						<li><strong><?=$keyregiao;?></strong> em Catalão</li>
						<li><strong><?=$keyregiao;?></strong> em Jataí</li>
						<li><strong><?=$keyregiao;?></strong> em Planaltina</li>
						<li><strong><?=$keyregiao;?></strong> em Caldas Novas</li>
						<li><strong><?=$keyregiao;?></strong> em Santo Antônio do Descoberto</li>
						<li><strong><?=$keyregiao;?></strong> em Goianésia</li>
						<li><strong><?=$keyregiao;?></strong> em Cidade Ocidental</li>
						<li><strong><?=$keyregiao;?></strong> em Mineiros</li>
						<li><strong><?=$keyregiao;?></strong> em Cristalina</li>
						<li><strong><?=$keyregiao;?></strong> em Inhumas</li>
					</ul>
				</div>

				<div id="MG" class="tab-pane fade">
					<h4><?=$keyregiao;?> em Minas Gerais</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Belo Horizonte</li>
						<li><strong><?=$keyregiao;?></strong> em Uberlândia</li>
						<li><strong><?=$keyregiao;?></strong> em Contagem</li>
						<li><strong><?=$keyregiao;?></strong> em Juiz de Fora</li>
						<li><strong><?=$keyregiao;?></strong> em Uberaba</li>
						<li><strong><?=$keyregiao;?></strong> em Governador Valadares</li>
						<li><strong><?=$keyregiao;?></strong> em Betim</li>
						<li><strong><?=$keyregiao;?></strong> em Montes Claros</li>
						<li><strong><?=$keyregiao;?></strong> em Ribeirão das Neves</li>
						<li><strong><?=$keyregiao;?></strong> em Ipatinga</li>
						<li><strong><?=$keyregiao;?></strong> em Sete Lagoas</li>
						<li><strong><?=$keyregiao;?></strong> em Divinópolis</li>
						<li><strong><?=$keyregiao;?></strong> em Santa Luzia</li>
						<li><strong><?=$keyregiao;?></strong> em Ibirité</li>
						<li><strong><?=$keyregiao;?></strong> em Poços de Caldas</li>
						<li><strong><?=$keyregiao;?></strong> em Patos de Minas</li>
						<li><strong><?=$keyregiao;?></strong> em Pouso Alegre</li>
						<li><strong><?=$keyregiao;?></strong> em Teófilo Otoni</li>
						<li><strong><?=$keyregiao;?></strong> em Barbacena</li>
						<li><strong><?=$keyregiao;?></strong> em Sabará</li>
						<li><strong><?=$keyregiao;?></strong> em Varginha</li>
						<li><strong><?=$keyregiao;?></strong> em Conselheiro Lafaiete</li>
						<li><strong><?=$keyregiao;?></strong> em Vespasiano</li>
						<li><strong><?=$keyregiao;?></strong> em Itabira</li>
						<li><strong><?=$keyregiao;?></strong> em Araguari</li>
						<li><strong><?=$keyregiao;?></strong> em Passos</li>
						<li><strong><?=$keyregiao;?></strong> em Ubá</li>
						<li><strong><?=$keyregiao;?></strong> em Coronel Fabriciano</li>
						<li><strong><?=$keyregiao;?></strong> em Muriaé</li>
						<li><strong><?=$keyregiao;?></strong> em Ituiutaba</li>
						<li><strong><?=$keyregiao;?></strong> em Araxá</li>
						<li><strong><?=$keyregiao;?></strong> em Lavras</li>
						<li><strong><?=$keyregiao;?></strong> em Itajubá</li>
						<li><strong><?=$keyregiao;?></strong> em Itaúna</li>
						<li><strong><?=$keyregiao;?></strong> em Pará de Minas</li>
						<li><strong><?=$keyregiao;?></strong> em Paracatu</li>
						<li><strong><?=$keyregiao;?></strong> em Caratinga</li>
						<li><strong><?=$keyregiao;?></strong> em Nova Lima</li>
						<li><strong><?=$keyregiao;?></strong> em Nova Serrana</li>
						<li><strong><?=$keyregiao;?></strong> em São João del Rei</li>
						<li><strong><?=$keyregiao;?></strong> em Patrocínio</li>
						<li><strong><?=$keyregiao;?></strong> em Timóteo</li>
						<li><strong><?=$keyregiao;?></strong> em Manhuaçu</li>
						<li><strong><?=$keyregiao;?></strong> em Unaí</li>
						<li><strong><?=$keyregiao;?></strong> em Curvelo</li>
						<li><strong><?=$keyregiao;?></strong> em Alfenas</li>
						<li><strong><?=$keyregiao;?></strong> em João Monlevade</li>
						<li><strong><?=$keyregiao;?></strong> em Três Corações</li>
						<li><strong><?=$keyregiao;?></strong> em Viçosa</li>
						<li><strong><?=$keyregiao;?></strong> em Cataguases</li>
						<li><strong><?=$keyregiao;?></strong> em Ouro Preto</li>
						<li><strong><?=$keyregiao;?></strong> em Janaúba</li>
						<li><strong><?=$keyregiao;?></strong> em São Sebastião do Paraíso</li>
						<li><strong><?=$keyregiao;?></strong> em Januária</li>
						<li><strong><?=$keyregiao;?></strong> em Formiga</li>
						<li><strong><?=$keyregiao;?></strong> em Esmeraldas</li>
						<li><strong><?=$keyregiao;?></strong> em Pedro Leopoldo</li>
						<li><strong><?=$keyregiao;?></strong> em Ponte Nova</li>
						<li><strong><?=$keyregiao;?></strong> em Lagoa Santa</li>
						<li><strong><?=$keyregiao;?></strong> em Mariana</li>
						<li><strong><?=$keyregiao;?></strong> em Frutal</li>
						<li><strong><?=$keyregiao;?></strong> em Três Pontas</li>
						<li><strong><?=$keyregiao;?></strong> em São Francisco</li>
						<li><strong><?=$keyregiao;?></strong> em Pirapora</li>
						<li><strong><?=$keyregiao;?></strong> em Campo Belo</li>
						<li><strong><?=$keyregiao;?></strong> em Leopoldina</li>
						<li><strong><?=$keyregiao;?></strong> em Congonhas</li>
						<li><strong><?=$keyregiao;?></strong> em Guaxupé</li>
						<li><strong><?=$keyregiao;?></strong> em Lagoa da Prata</li>
					</ul>
				</div>

				<div id="PR" class="tab-pane fade">
					<h4><?=$keyregiao;?> no Paraná</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Curitiba</li>
						<li><strong><?=$keyregiao;?></strong> em Londrina</li>
						<li><strong><?=$keyregiao;?></strong> em Maringá</li>
						<li><strong><?=$keyregiao;?></strong> em Ponta Grossa</li>
						<li><strong><?=$keyregiao;?></strong> em Cascavel</li>
						<li><strong><?=$keyregiao;?></strong> em São José dos Pinhais</li>
						<li><strong><?=$keyregiao;?></strong> em Foz do Iguaçu</li>
						<li><strong><?=$keyregiao;?></strong> em Colombo</li>
						<li><strong><?=$keyregiao;?></strong> em Guarapuava</li>
						<li><strong><?=$keyregiao;?></strong> em Paranaguá</li>
						<li><strong><?=$keyregiao;?></strong> em Araucária</li>
						<li><strong><?=$keyregiao;?></strong> em Toledo</li>
						<li><strong><?=$keyregiao;?></strong> em Apucarana</li>
						<li><strong><?=$keyregiao;?></strong> em Pinhais</li>
						<li><strong><?=$keyregiao;?></strong> em Campo Largo</li>
						<li><strong><?=$keyregiao;?></strong> em Arapongas</li>
						<li><strong><?=$keyregiao;?></strong> em Almirante Tamandaré</li>
						<li><strong><?=$keyregiao;?></strong> em Umuarama</li>
						<li><strong><?=$keyregiao;?></strong> em Piraquara</li>
						<li><strong><?=$keyregiao;?></strong> em Cambé</li>
						<li><strong><?=$keyregiao;?></strong> em Campo Mourão</li>
						<li><strong><?=$keyregiao;?></strong> em Fazenda Rio Grande</li>
						<li><strong><?=$keyregiao;?></strong> em Sarandi</li>
						<li><strong><?=$keyregiao;?></strong> em Fazenda Rio Grande</li>
						<li><strong><?=$keyregiao;?></strong> em Paranavaí</li>
						<li><strong><?=$keyregiao;?></strong> em Francisco Beltrão</li>
						<li><strong><?=$keyregiao;?></strong> em Pato Branco</li>
						<li><strong><?=$keyregiao;?></strong> em Cianorte</li>
						<li><strong><?=$keyregiao;?></strong> em Telêmaco Borba</li>
						<li><strong><?=$keyregiao;?></strong> em Castro</li>
						<li><strong><?=$keyregiao;?></strong> em Rolândia</li>
						<li><strong><?=$keyregiao;?></strong> em Irati</li>
						<li><strong><?=$keyregiao;?></strong> em União da Vitória</li>
						<li><strong><?=$keyregiao;?></strong> em Ibiporã</li>
						<li><strong><?=$keyregiao;?></strong> em Prudentópolis</li>
						<li><strong><?=$keyregiao;?></strong> em Marechal Cândido Rondon</li>
						<li><strong><?=$keyregiao;?></strong> em Cornélio Procópio</li>
					</ul>
				</div>

				<div id="RJ" class="tab-pane fade">
					<h4><?=$keyregiao;?> no Rio de Janeiro</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em São Gonçalo</li>
						<li><strong><?=$keyregiao;?></strong> em Duque de Caxias</li>
						<li><strong><?=$keyregiao;?></strong> em Nova Iguaçu </li>
						<li><strong><?=$keyregiao;?></strong> em Niterói</li>
						<li><strong><?=$keyregiao;?></strong> em Campos dos Goytacazes</li>
						<li><strong><?=$keyregiao;?></strong> em Belford Roxo</li>
						<li><strong><?=$keyregiao;?></strong> em São João de Meriti</li>
						<li><strong><?=$keyregiao;?></strong> em Petrópolis</li>
						<li><strong><?=$keyregiao;?></strong> em Volta Redonda</li>
						<li><strong><?=$keyregiao;?></strong> em Magé</li>
						<li><strong><?=$keyregiao;?></strong> em Macaé</li>
						<li><strong><?=$keyregiao;?></strong> em Itaboraí</li>
						<li><strong><?=$keyregiao;?></strong> em Cabo Frio</li>
						<li><strong><?=$keyregiao;?></strong> em Angra dos Reis</li>
						<li><strong><?=$keyregiao;?></strong> em Nova Friburgo</li>
						<li><strong><?=$keyregiao;?></strong> em Barra Mansa</li>
						<li><strong><?=$keyregiao;?></strong> em Teresópolis</li>
						<li><strong><?=$keyregiao;?></strong> em Mesquita</li>
						<li><strong><?=$keyregiao;?></strong> em Nilópolis</li>
						<li><strong><?=$keyregiao;?></strong> em Maricá</li>
						<li><strong><?=$keyregiao;?></strong> em Queimados</li>
						<li><strong><?=$keyregiao;?></strong> em Rio das Ostras</li>
						<li><strong><?=$keyregiao;?></strong> em Resende</li>
						<li><strong><?=$keyregiao;?></strong> em Araruama</li>
						<li><strong><?=$keyregiao;?></strong> em Itaguaí</li>
						<li><strong><?=$keyregiao;?></strong> em Japeri</li>
						<li><strong><?=$keyregiao;?></strong> em Itaperuna</li>
						<li><strong><?=$keyregiao;?></strong> em São Pedro da Aldeia</li>
						<li><strong><?=$keyregiao;?></strong> em Barra do Piraí</li>
						<li><strong><?=$keyregiao;?></strong> em Seropédica</li>
						<li><strong><?=$keyregiao;?></strong> em Saquarema</li>
						<li><strong><?=$keyregiao;?></strong> em Três Rios</li>
						<li><strong><?=$keyregiao;?></strong> em Valença</li>
						<li><strong><?=$keyregiao;?></strong> em Rio Bonito</li>
						<li><strong><?=$keyregiao;?></strong> em Guapimirim</li>
						<li><strong><?=$keyregiao;?></strong> em Cachoeiras de Macacu</li>
						<li><strong><?=$keyregiao;?></strong> em Paracambi</li>
						<li><strong><?=$keyregiao;?></strong> em Paraíba do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em São Francisco de Itabapoana</li>
					</ul>
				</div>

				<div id="RS" class="tab-pane fade">
					<h4><?=$keyregiao;?> no Rio Grande do Sul</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Porto Alegre</li>
						<li><strong><?=$keyregiao;?></strong> em Caxias do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Pelotas</li>
						<li><strong><?=$keyregiao;?></strong> em Canoas</li>
						<li><strong><?=$keyregiao;?></strong> em Santa Maria</li>
						<li><strong><?=$keyregiao;?></strong> em Gravataí</li>
						<li><strong><?=$keyregiao;?></strong> em Viamão</li>
						<li><strong><?=$keyregiao;?></strong> em Novo Hamburgo</li>
						<li><strong><?=$keyregiao;?></strong> em São Leopoldo</li>
						<li><strong><?=$keyregiao;?></strong> em Rio Grande</li>
						<li><strong><?=$keyregiao;?></strong> em Alvorada</li>
						<li><strong><?=$keyregiao;?></strong> em Passo Fundo</li>
						<li><strong><?=$keyregiao;?></strong> em Sapucaia do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Uruguaiana</li>
						<li><strong><?=$keyregiao;?></strong> em Santa Cruz do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Cachoeirinha</li>
						<li><strong><?=$keyregiao;?></strong> em Bagé</li>
						<li><strong><?=$keyregiao;?></strong> em Bento Gonçalves</li>
						<li><strong><?=$keyregiao;?></strong> em Erechim</li>
						<li><strong><?=$keyregiao;?></strong> em Guaíba</li>
						<li><strong><?=$keyregiao;?></strong> em Cachoeira do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Esteio</li>
						<li><strong><?=$keyregiao;?></strong> em Santana do Livramento</li>
						<li><strong><?=$keyregiao;?></strong> em Ijuí</li>
						<li><strong><?=$keyregiao;?></strong> em Sapiranga</li>
						<li><strong><?=$keyregiao;?></strong> em Santo Ângelo</li>
						<li><strong><?=$keyregiao;?></strong> em Alegrete</li>
						<li><strong><?=$keyregiao;?></strong> em Lajeado</li>
						<li><strong><?=$keyregiao;?></strong> em Santa Rosa</li>
						<li><strong><?=$keyregiao;?></strong> em Venâncio Aires</li>
						<li><strong><?=$keyregiao;?></strong> em Farroupilha</li>
						<li><strong><?=$keyregiao;?></strong> em Camaquã</li>
						<li><strong><?=$keyregiao;?></strong> em Vacaria</li>
						<li><strong><?=$keyregiao;?></strong> em Campo Bom</li>
						<li><strong><?=$keyregiao;?></strong> em Cruz Alta</li>
						<li><strong><?=$keyregiao;?></strong> em Montenegro</li>
						<li><strong><?=$keyregiao;?></strong> em São Borja</li>
						<li><strong><?=$keyregiao;?></strong> em São Gabriel</li>
						<li><strong><?=$keyregiao;?></strong> em Carazinho</li>
						<li><strong><?=$keyregiao;?></strong> em Taquara</li>
						<li><strong><?=$keyregiao;?></strong> em Canguçu</li>
						<li><strong><?=$keyregiao;?></strong> em Parobé</li>
						<li><strong><?=$keyregiao;?></strong> em Santiago</li>
					</ul>
				</div>

				<div id="SC" class="tab-pane fade">
					<h4><?=$keyregiao;?> em Santa Catarina</h4>
					<ul class="regioes-tabs">
						<li><strong><?=$keyregiao;?></strong> em Joinville</li>
						<li><strong><?=$keyregiao;?></strong> em Florianópolis</li>
						<li><strong><?=$keyregiao;?></strong> em Blumenau</li>
						<li><strong><?=$keyregiao;?></strong> em São José</li>
						<li><strong><?=$keyregiao;?></strong> em Criciúma</li>
						<li><strong><?=$keyregiao;?></strong> em Chapecó</li>
						<li><strong><?=$keyregiao;?></strong> em Itajaí</li>
						<li><strong><?=$keyregiao;?></strong> em Jaraguá do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Lages</li>
						<li><strong><?=$keyregiao;?></strong> em Palhoça</li>
						<li><strong><?=$keyregiao;?></strong> em Balneário Camboriú</li>
						<li><strong><?=$keyregiao;?></strong> em Brusque</li>
						<li><strong><?=$keyregiao;?></strong> em Tubarão</li>
						<li><strong><?=$keyregiao;?></strong> em São Bento do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Caçador</li>
						<li><strong><?=$keyregiao;?></strong> em Camboriú</li>
						<li><strong><?=$keyregiao;?></strong> em Navegantes</li>
						<li><strong><?=$keyregiao;?></strong> em Concórdia</li>
						<li><strong><?=$keyregiao;?></strong> em Rio do Sul</li>
						<li><strong><?=$keyregiao;?></strong> em Araranguá</li>
						<li><strong><?=$keyregiao;?></strong> em Gaspar</li>
						<li><strong><?=$keyregiao;?></strong> em Biguaçu</li>
						<li><strong><?=$keyregiao;?></strong> em Indaial</li>
						<li><strong><?=$keyregiao;?></strong> em Itapema</li>
						<li><strong><?=$keyregiao;?></strong> em Mafra</li>
						<li><strong><?=$keyregiao;?></strong> em Canoinhas</li>
						<li><strong><?=$keyregiao;?></strong> em Içara</li>
						<li><strong><?=$keyregiao;?></strong> em Videira</li>
					</ul>
				</div>
			</div>
		</div>


		<?php /*
		----------------------------------------------
		--------- Listagem de QUEM ATENDEMOS ---------
		----------------------------------------------

		<h4 class="titles-includes"><span>Quem Atendemos</span></h4>
		<div class="tabs">
			<ul class="nav nav-tabs">
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Construtoras">Construtoras</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Incorporadoras">Incorporadoras</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Empresas">Empresas</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Indústrias">Indústrias</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Redes de Varejo">Redes de Varejo</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Hotéis">Hotéis</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Fast Food">Fast Food</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Food Service">Food Service</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Supermercados">Supermercados</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Shoppings">Shoppings</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Parques">Parques</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Restaurantes">Restaurantes</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Bares">Bares</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Padarias">Padarias</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link nav-link2" href="#Residências de Alto Padrão">Residências de Alto Padrão</a></li>
			</ul>
		</div>
		*/?>
	</div>
</div>



