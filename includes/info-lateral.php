<div class="col-lg-3 order-lg-2">
            <p>Pesquisar por:</p>
            <aside class="sidebar noticias">
                <form action="#" method="get">
                    <div class="input-group mb-3 pb-1">
                        <input class="form-control text-1" name="s" id="s" type="text">

                    </div>
                    <span class="input-group-append">
                        <button type="button" class="btn btn-modern btn-green btn-primary mb-2" style="width: 100%">Pesquisar</button>
                    </span>
                </form>

                <h4 class="mb-5 pt-4">Fique por dentro</h4>

                <h5 class="font-weight">Ahlstrom</h5>
                <a href="noticia-ahlstrom"><img src="img/ahlstrom-300x61.jpg" alt="Ahlstrom" width="215"></a>
                <hr class="separator-line mb-5 align_left" style="background-image: -webkit-linear-gradient(left, grey, transparent); background-image: linear-gradient(to right, grey, transparent);">

                <h5 class="font-weight">Ajinomoto</h5>
                <a href="noticia-ajinomoto"><img src="img/ajinomoto-300x62.jpg" alt="Ahlstrom" width="215"></a>
                <hr class="separator-line  align_left" style="background-image: -webkit-linear-gradient(left, grey, transparent); background-image: linear-gradient(to right, grey, transparent);">

                <h5 class="font-weight">Berneck</h5>
                <a href="noticia-berneck"><img src="img/berneck-blog-300x61.jpg" alt="Ahlstrom" width="215"></a>
                <hr class="separator-line  mb-5 align_left" style="background-image: -webkit-linear-gradient(left, grey, transparent); background-image: linear-gradient(to right, grey, transparent);">

                <h5 class="font-weight">Mars</h5>
                <a href="noticia-mars"><img src="img/mars-300x62.jpg" alt="Ahlstrom" width="215"></a>
                <hr class="separator-line  mb-5 align_left" style="background-image: -webkit-linear-gradient(left, grey, transparent); background-image: linear-gradient(to right, grey, transparent);">

                <h5 class="font-weight">Notícias recentes</h5>

                <p><a href="manutencao-preventiva-queimadores-maxon-valupak-300"><i class="fas fa-angle-right"></i> MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON VALUPAK 300</a></p>
                <hr class="short">
                <p><a href="comissionamento-queimador-baltur"><i class="fas fa-angle-right"></i> Comissionamento Queimador BALTUR</a></p>
                <hr class="short">
                <p><a href="manutencao-corretiva-painel-eletrico-forno-fusao-vidro"><i class="fas fa-angle-right"></i> MANUTENÇÃO CORRETIVA NO PAINEL ELÉTRICO DO FORNO DE FUSÃO DE VIDRO</a></p>
                <hr class="short">

            </aside>
        </div>