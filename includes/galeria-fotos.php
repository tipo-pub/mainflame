<section class="section section-light border-0 p-0">
    <div class="container">
        <div class="heading-pages-sub">
            <h2><?=$tituloGaleria?></h2>
        </div>

        <input type='hidden' id='current_page' />
        <input type='hidden' id='show_per_page' />

        <div class="row contGaleria" id='content'>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-01.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-01.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-02.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-02.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-03.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-03.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-04.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-04.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-05.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-05.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-06.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-06.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-07.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-07.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-08.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-08.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-09.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-09.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-10.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-10.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-11.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-11.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-12.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-12.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-13.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-13.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-14.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-14.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-15.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-15.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-16.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-16.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-17.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-17.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-18.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-18.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-19.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-19.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-20.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-20.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-21.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-21.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-22.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-22.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-23.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-23.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-24.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-24.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-25.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-25.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-26.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-26.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-27.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-27.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-28.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-28.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-29.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-29.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-30.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-30.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-31.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-31.jpg" alt="Project Image" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="<?=$url?>img/galeria/galeria-32.jpg" data-plugin-options="{'type':'image'}">
                    <img class="img-fluid" src="<?=$url?>img/galeria/thumbs/galeria-32.jpg" alt="Project Image" />
                </a>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-center" id='portfolioPagination'></div>
    </div>
</section>