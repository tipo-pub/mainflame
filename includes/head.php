<!DOCTYPE html>
<html>

<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?=$title;?> | <?=$NomeEmpresa;?></title>

	<!-- SEO -->
	<meta name="title" content="<?=$title ." - ". $NomeEmpresa?>" />
	<meta name="description" content="<?=$description;?>">
	<meta name="keywords" content="<?=$keywords;?>" />
	<meta name="language" content="PT-BR" />

	<meta name="author" content="<?=$author;?>">
	<meta name="copyright" content="<?=$NomeEmpresa;?>" />
	<meta name="distribution" content="global" />
	<meta name="audience" content="all" />
	<meta name="url" content="<?=$url;?>" />
	<meta name="classification" content="<?=$ramo;?>" />
	<meta name="category" content="<?=$ramo;?>" />
	<meta name="Page-Topic" content="<?=$title ." - ". $NomeEmpresa?>" />
	<meta name="rating" content="general" />
	<meta name="fone" content="<?='' . $ddd . ' ' . $tel;?>" />
	<meta name="city" content="<?=$uf;?>" />
	<meta name="country" content="Brasil" />
	<meta property="publisher" content="<?=$creditos;?>" />

	<!-- Google -->
	<link rel="canonical" href="<?=$canonical;?>" />
	<meta name="robots" content="index,follow" />
	<meta name="googlebot" content="index,follow" />
	<meta name="geo.placename" content="Brasil" />
	<meta name="geo.region" content="<?=$uf;?>" />
	<meta itemprop="name" content="<?=$NomeEmpresa;?>" />
	<meta itemprop="description" content="<?=$description;?>" />
	<meta itemprop="image" content="<?=$card;?>" />

	<!-- Twitter -->
	<meta name="twitter:card" content="<?=$TwitterCard;?>">
	<meta name="twitter:site" content="<?=$canonical;?>">
	<meta name="twitter:title" content="<?=$title;?>">
	<meta name="twitter:description" content="<?=$description;?>">
	<meta name="twitter:creator" content="<?=$twitter?>">
	<meta name="twitter:image:src" content="<?=$card;?>">

	<!-- Facebook -->
	<meta property="og:title" content="<?=$title ." - ". $NomeEmpresa?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?=$canonical;?>" />
	<meta property="og:site_name" content="<?=$NomeEmpresa?>">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:image" content="<?=$card;?>">
	<meta property="og:image:type" content="image/jpg">
	<meta property="og:image:width" content="470">
	<meta property="og:image:height" content="256">
	<meta property="og:description" content="<?=$description;?>">
	<meta property="fb:admins" content="<?=$CodFanpage;?>" />
	<link href="<?=$card;?>" rel="image_src" />

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/favicon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skin-corporate-7.css">

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
