<div class="owl-carousel owl-theme owl-loaded owl-drag owl-carousel-init mt-5" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 4}, '1199': {'items': 4}}, 'loop': false, 'autoHeight': true, 'margin': 10}" style="height: auto;">
    
    <h4 class="mb-5 text-6">Cases <strong>Relacionados</strong></h4>

    <div class="owl-stage-outer owl-height" style="height: 141.667px;">
        <div class="owl-stage" style="transform: translate3d(-1061px, 0px, 0px); transition: all 0.25s ease 0s; width: 1517px;">

            <div class="portfolio-item owl-item">
                <a href="valvula-bloqueio-com-atuador-eletromecanico-ou-pneumatico">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-atuador-eletromecanico.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Registro para Manômetro</span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="aparelho-programador-chama">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/aparelho-programador-chama.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Aparelho Programador De Chama </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="sensor-de-chama">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/sensor-chama.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Sensor de Chama </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="queimadores-para-alta-temperatura">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/queimadora-alta-temperatura.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Queimadores Para Alta Temperatura </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="valvula-proporcionadora-ar-gas-s">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-proporcionadora-ar-gas.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Proporcionadora Ar/Gás </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="queimador-piloto">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/queimador-piloto.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Queimador Piloto </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="queimadores-para-baixa-temperatura">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/queimador-baixa-temperatura.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Queimadores Para Baixa Temperatura </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="servo-motor">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/servo-motor.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Servo Motor </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="valvula-borboleta">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/valvula-borboleta.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Válvula Borboleta </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="portfolio-item owl-item">
                <a href="painel-de-comando-e-controle">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/produtos/thumbs/painel-comando-controle.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner"> Painel De Comando E Controle </span>
                                <span class="thumb-info-type">Produtos</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-9"><i class="fa fa-link"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>

</div>
