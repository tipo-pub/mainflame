
<div id="ssb-container" class="ssb-btns-right ssb-anim-icons" style="z-index: 13">
    <ul class="ssb-dark-hover">
	    <li id="ssb-btn-0">
            <p>
                <a href="<?=$linkWhats;?>" target="_blank"><span class="text-color-light fab fa-whatsapp"></span>&nbsp;</a>
            </p>
        </li>
		 <li id="ssb-btn-2" class="bg-primary">
            <p>
                <a href="mailto:<?=$email?>" title="<?=$email?>"><span class="text-color-light fa fa-envelope"></span>&nbsp;</a>
            </p>
        </li>
	</ul>
</div>
