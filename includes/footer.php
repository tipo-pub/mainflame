<footer id="footer">
	<div class="container mt-4">
		<div class="row py-5">
			<div class="col-md-5 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-6 text-transform-none text-color-light ls-0 mb-3">Contato</h5>
				<p class="text-4 mb-4"><a target="_blank" href="<?=$linkMaps;?>"> <?=$endereco;?> - <?=$bairro;?> <br /> <?=$cidade;?> - CEP: <?=$cep;?></a></p>
				
				<p class="text-5 mb-0 pt-2">Telefone: <a href="tel:<?=$linktel;?>">(<?=$ddd;?>) <?=$tel;?></a></p>
				<p class="text-5 mb-0 pt-2">Telefone: <a href="tel:<?=$linktel2;?>">(<?=$ddd;?>) <?=$tel2;?></a></p>
				<p class="text-5 mb-0 pt-2">Telefone: <a target="_blank" href="tel:<?=$linkWhats;?>">(<?=$ddd;?>) <?=$whats;?></a></p>
				<p class="text-5 mb-0 pt-2">Email: <a href="mailto:<?=$email;?>"><?=$email;?></a></p>
			</div>
			<div class="col-md-5 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-6 text-transform-none text-color-light ls-0 mb-3">Institucional</h5>
				<div class="row">
					<ul class="lista-rodape">
					<?php $menuRodape = true; $menuTopo = false; include "includes/menu.php";?>
					</ul>
				</div>
			</div>
			<div class="col-md-5 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-6 text-transform-none text-color-light ls-0 mb-3">Newsletter</h5>
				<div class="alert alert-success d-none" id="newsletterSuccess">
					<strong>Success!</strong> You've been added to our email list.
				</div>
				<div class="alert alert-danger d-none" id="newsletterError"></div>
				<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST" class="mw-100">
					<div class="input-group input-group-rounded">
						<input class="form-control form-control-sm bg-light px-4 text-3" placeholder="Endereço de email..." name="newsletterEmail" id="newsletterEmail" type="text">
						<span class="input-group-append">
							<button class="btn btn-primary text-uppercase text-color-light text-2 py-3 px-4" type="submit"><strong>Inscreva-se</strong></button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="footer-copyright footer-copyright-style-2">
		<div class="container py-4">
			<div class="row">
				<div class="col-md-8 d-flex align-items-center justify-content-center mb-4 mb-lg-0">
					<p>Copyright &copy; <?php echo date("Y") . '&nbsp;&nbsp;' . $NomeEmpresa ?> | Todos os Direitos Reservados</p>
				</div>
				<div class="col-md-4">
					<p>Desenvolvido por <a target="_blank" href="http://www.tipopublicidade.com.br" title="Tipo Publicidade"><img src="img/tipo.png" style="width:20px" alt="Tipo Publicidade"></a></p>
				</div>
			</div>
		</div>
	</div>
	<?php include 'includes/btn-lateral.php' ;?>
</footer>


<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/jquery.vide.min.js"></script>
<script src="vendor/vivus/vivus.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<script src="https://www.google.com/recaptcha/api.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-72058135-63"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-72058135-63');
</script>

</body>

</html>
