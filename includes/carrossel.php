<div class="owl-carousel owl-theme full-width" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
	<?php
	$keys = array_rand($LinksPalavras, 20);

	$i = 0;
	foreach ($keys as $link) {
		$palavra = $LinksPalavras[$link];
		echo "<div>";
		echo "<a href=\"$url$link\" title=\"$palavra\">";
		echo "<span class=\"thumb-info thumb-info-centered-info thumb-info-no-borders\">";
		echo "<span class=\"thumb-info-wrapper\">";
		echo "<img src=\"".$url."img/engenharia-manutencao/carrossel/".$link."-01.jpg\" class=\"img-fluid\" alt=\"$palavra\" />";
		echo "<span class=\"thumb-info-title\">";
		echo "<span class=\"thumb-info-inner\">$palavra</span>";
		echo "</span>";
		echo "<span class=\"thumb-info-action\">";
		echo "<span class=\"thumb-info-action-icon\"><i class=\"fas fa-plus\"></i></span>";
		echo "</span>";
		echo "</span>";
		echo "</span>";
		echo "</a>";
		echo "</div>";
		$i++;
	}
	?>
</div>
