<?php
include 'includes/geral.php';
$title = 'QUEIMADOR, CAVALETE DE GÁS NATURAL';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
                    <div class="post-image">
                        <a href="queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807">
                            <img src="img/queimador-cavalete-de-gas-natural-painel-de-comando-instrume.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">11</span>
                        <span class="month">out</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807">QUEIMADOR, CAVALETE DE GÁS NATURAL, PAINEL DE COMANDO, INSTRUMENTAÇÃO DE CAMPO PARA O SISTEMA DE COMBUSTÃO DA CALDEIRA ATA MP-807</a></h3>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="projetos-desenvolvidos"> Projetos desenvolvidos</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>
                        
                        <h3>DADOS DE PROJETO PARA APLICAÇÃO:</h3>
                        
                        <p>Caldeira: ATA</p>
                        
                        <p>Modelo: MP-807</p>
                        
                        <p>Ano fabricação: 1990</p>
                        
                        <p>Superfície de vaporização: 46 m²</p>
                        
                        <p>Produção de vapor: 4.000 kg/h</p>
                        
                        <p>M.P.T.A.: 256 psig (18.00 kgf/cm²)</p>
                        
                        <p>Teste hidrostático: 384 psig (27.00 kgf/cm²)</p>
                        
                        <p>Número de ordem: 8495</p>
                        
                        <p>Ventilador de ar de combustão: ATA</p>
                        
                        <h3>CONDIÇÕES DE OPERAÇÃO:</h3>
                        
                        <p>Combustível: Gás Natural</p>
                        
                        <p>Poder calorífico inferior (PCI): 9.000 kcal/Nm3</p>
                        
                        <p>Peso específico: 0,79 kg/m3</p>
                        
                        <p>Densidade relativa ao ar: 0,645</p>
                        
                        <p>Temperatura do combustível: 25°C</p>
                        
                        <p>Volume de ar requerido (estequiométrico): 10 m3/m3</p>
                        
                        <p>Pressão da rede de alimentação de gás: 2 bar</p>
                        
                        <p>Liberação de calor aplicada (máximo): 3.000.000 kcal/h</p>
                        
                        <p>Vazão de gás máxima: 334 Nm3/h</p>
                        
                        <p>Quantidade de queimadores: 1 (um)</p>
                        
                        <p>Tipo de regulagem ou controle: modulante (dual looping)</p>
                        
                        <p>Faixa de regulagem: 1:5</p>
                        
                        <p>Excesso de ar de combustão: 20%</p>
                        
                        <p>Temperatura do ar de combustão: 25°C</p>
                        
                        <p>Partida: Automática</p>
                        
                        <p>Tensão de comando: 220 Volts / 60Hz</p>

                    </div>
                </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-caldeira-ata-mp-807/sistema-combustao-caldeira-ata-mp-807-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-caldeira-ata-mp-807/thumbs/sistema-combustao-caldeira-ata-mp-807-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-caldeira-ata-mp-807/sistema-combustao-caldeira-ata-mp-807-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-caldeira-ata-mp-807/thumbs/sistema-combustao-caldeira-ata-mp-807-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-caldeira-ata-mp-807/sistema-combustao-caldeira-ata-mp-807-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-caldeira-ata-mp-807/thumbs/sistema-combustao-caldeira-ata-mp-807-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-caldeira-ata-mp-807/sistema-combustao-caldeira-ata-mp-807-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-caldeira-ata-mp-807/thumbs/sistema-combustao-caldeira-ata-mp-807-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-caldeira-ata-mp-807/sistema-combustao-caldeira-ata-mp-807-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-caldeira-ata-mp-807/thumbs/sistema-combustao-caldeira-ata-mp-807-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-caldeira-ata-mp-807/sistema-combustao-caldeira-ata-mp-807-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-caldeira-ata-mp-807/thumbs/sistema-combustao-caldeira-ata-mp-807-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
</div>


<?php include 'includes/footer.php' ;?>
