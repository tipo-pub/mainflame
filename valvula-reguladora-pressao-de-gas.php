<?php
include 'includes/geral.php';
$title = 'Válvula Reguladora de Pressão de Gás';
$description = 'A válvula reguladora de pressão tem a finalidade de reduzir a pressão de entrada de gás, saiba mais.';
$keywords = 'produtos, Válvula Reguladora de Pressão de Gás, a melhor Válvula Reguladora de Pressão de Gás';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-reguladora-pressao-gas.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-reguladora-pressao-gas.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A válvula reguladora de pressão tem a finalidade de reduzir a pressão de entrada de gás que a companhia distribuidora fornece para a pressão de trabalho do queimador. Deve possuir recursos para um caso de pane impeça o vazamento de gás para dentro do recinto. Alguns modelos possuem válvula de bloqueio automático (shut off) incorporadas. O mesmo se aplica para válvula de alivio.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Conexões roscadas Rp: (DN 15 ÷ DN 50) de acordo com DIN 2999</li>
                <li>Conexões flangeadas PN 16 (DN 40 ÷ DN 150) de acordo com ISO 7005</li>
                <li>Pressão de entrada: 500 mbar a 6 bar</li>
                <li>Pressão de saída: 7 mbar a 600 mbar</li>
                <li>Temperatura ambiente: – 20 ÷ +60°C</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
