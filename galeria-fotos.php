<?php
$title = 'GALERIA DE FOTOS';
$description ="";
$keywords ="";


include ("includes/geral.php");
include ("includes/head.php");
include ("includes/header.php"); 
?> 
<main class="site-main page-spacing">
	<?php include "includes/banner_page.php";?>
	<!-- Portfolio Section -->
	<div class="container-fuild no-padding project-portfolio portfolio-section">
		<div class="container">
			<div class="row">

				<ul class="portfolio-list no-padding">
					<li class="col-md-4 col-sm-4 col-xs-6 chemical construction">
						<div class="image-block">
							<img src="images/project-1.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-1.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 automotive construction">
						<div class="image-block">
							<img src="images/project-2.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-2.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 chemical energy exploration">
						<div class="image-block">
							<img src="images/project-3.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-3.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 automotive chemical exploration">
						<div class="image-block">
							<img src="images/project-4.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-4.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 agrar energy exploration">
						<div class="image-block">
							<img src="images/project-5.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-5.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 automotive chemical">
						<div class="image-block">
							<img src="images/project-6.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-6.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 agrar construction energy">
						<div class="image-block">
							<img src="images/project-7.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-7.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 automotive chemical exploration">
						<div class="image-block">
							<img src="images/project-8.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-8.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
					<li class="col-md-4 col-sm-4 col-xs-6 agrar construction exploration">
						<div class="image-block">
							<img src="images/project-9.jpg" alt="project" width="370" height="220"/>
							<a href="images/project-9.jpg" class="zoom" title="Project"><span class="icon icon-Goto"></span></a>
						</div>
					</li>
				</ul>

				<nav class="ow-pagination">
					<ul class="pagination">
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">Próximo</a></li>
					</ul>
				</nav>
			</div>

		</div>
	</div><!-- Portfolio Section /- -->
</main>

<?php include ("includes/footer.php");?>