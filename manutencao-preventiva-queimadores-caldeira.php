<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – QUEIMADOR DA CALDEIRA ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-caldeira">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">25</span>
            <span class="month">maio</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-caldeira">MANUTENÇÃO PREVENTIVA – QUEIMADOR DA CALDEIRA</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica do queimador;</p>
            <p>– Inspeção mecânica do cavalete de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análise e ajuste da relação ar/gás;</p>
            <p>– Elaboração de relatório;</p>
            <p>– Curva de desempenho do queimador;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento de posta em marcha;</p>

        </div>
    </article>



</div>


<?php include 'includes/footer.php' ;?>
