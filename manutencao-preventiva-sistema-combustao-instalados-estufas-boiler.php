<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – SISTEMAS DE COMBUSTÃO INSTALADOS NAS ESTUFAS NO BOILER ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-sistema-combustao-instalados-estufas-boiler">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">28</span>
            <span class="month">jul</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-sistema-combustao-instalados-estufas-boiler">MANUTENÇÃO PREVENTIVA – SISTEMAS DE COMBUSTÃO INSTALADOS NAS ESTUFAS NO BOILER</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>

                <p>– Desmontagem do queimador;</p>

                <p>– Inspeção dos componentes do queimador;</p>

                <p>– Limpeza e lubrificação dos componentes;</p>

                <p>– Remontagem do queimador;</p>

                <p>– Desmontagem do ventilador de ar de combustão (quando aplicável);</p>

                <p>– Inspeção dos componentes do ventilador (quando aplicável);</p>

                <p>– Limpeza e lubrificação do ventilador (quando aplicável);</p>

                <p>– Remontagem do ventilador (quando aplicável);</p>

                <p>– Desmontagem do Cavalete de alimentação de gás natural;</p>

                <p>– Inspeção dos componentes do cavalete;</p>

                <p>– Limpeza e lubrificação dos componentes;</p>

                <p>– Remontagem do cavalete;</p>

                <p>– Treinamento de manutenção / operação no site;</p>

                <p>– Levantamento fotográfico (com prévia autorização do cliente).</p>

                <ul>
                    <li>Teste nas lógicas de segurança;</li>
                    <li>Análises e ajustes da relação ar/gás;</li>
                    <li>Elaboração de relatório;</li>
                    <li>Curva de desempenho do queimador;</li>
                    <li>Teste de operação;</li>
                    <li>Acompanhamento da posta em marcha.</li>
                </ul>

            </div>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-07.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/sistema-combustao-instalados-estufa-boiler/sistema-combustao-instalados-estufa-boiler-08.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/sistema-combustao-instalados-estufa-boiler/thumbs/sistema-combustao-instalados-estufa-boiler-08.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
