<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA DOS QUEIMADORES MAXON – REXAM – DF ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-maxon-rexam-df">
                <img src="img/manutencao-preventiva-dos-queimadores-maxon-rexam-df.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">29</span>
            <span class="month">mar</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon-rexam-df">MANUTENÇÃO PREVENTIVA DOS QUEIMADORES MAXON – REXAM – DF</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <h3>SERVIÇOS DE ASSISTÊNCIA TÉCNICA ESPECIALIZADA PARA MANUTENÇÃO PREVENTIVA NOS QUEIMADORES</h3>

            <p><strong>Escopo dos Serviços</strong></p>

            <ul>
                <li>Inspeção mecânica dos queimadores instalados;</li>
                <li>Substituição de peças danificadas dos queimadores;</li>
                <li>Inspeção mecânica dos cavaletes de alimentação de gás;</li>
                <li>Levantamento fotográfico (com prévia autorização do cliente);</li>
                <li>Teste nas lógicas de segurança;</li>
                <li>Análise e ajuste da relação ar/gás;</li>
                <li>Elaboração de relatórios;</li>
                <li>Curva de desempenho dos queimadores;</li>
                <li>Testes de operação;</li>
                <li>Acompanhamento de posta em marcha.</li>
            </ul>

        </div>
    </article>
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-07.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-08.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-08.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-09.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-09.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/manutencao-preventiva-queimadores-maxon-rexam-df-10.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimadores-maxon-rexam-df/thumbs/manutencao-preventiva-queimadores-maxon-rexam-df-10.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
