<?php
include 'includes/geral.php';
$title = 'Galeria de Fotos e Vídeos';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4">
   <div class="sort-destination-loader-showing mt-4 pt-2">
		<div class="row portfolio-list sort-destination" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">							

			<div class="col-md-6 mb-4 isotope-item brands">
				<div class="portfolio-item">
					<a href="fotos">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="img/galeria/capa-fotos.jpg" class="img-fluid border-radius-0" alt="Fotos" />

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Fotos</span>
									<span class="thumb-info-type">Galeria</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-md-6 mb-4 isotope-item brands">
				<div class="portfolio-item">
					<a href="videos">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="img/galeria/capa-videos.jpg" class="img-fluid border-radius-0" alt="Vídeos" />

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Vídeos</span>
									<span class="thumb-info-type">Galeria</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'includes/footer.php' ;?>
