<?php
include 'includes/geral.php';
$title = 'Válvula de Bloqueio por Sobrepressão – SHUT OFF';
$description = '';
$keywords = 'produtos, Válvula de Bloqueio por Sobrepressão – SHUT OFF, a melhor Válvula de Bloqueio por Sobrepressão – SHUT OFF';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-bloqueio-sobrepressao.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-bloqueio-sobrepressao.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A válvula de bloqueio por sobre pressão (shut off) é um dispositivo de segurança que foi concebido para bloquear a passagem de gás em caso de elevação de pressão protegendo assim os demais componentes da instalação.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Conexões roscadas Rp: (DN 15 ÷ DN 50) de acordo com DIN 2999</li>
                <li>Conexões flangeadas PN 16 (DN 40 ÷ DN 150) de acordo com ISO 7005</li>
                <li>Pressão de entrada: 1 bar a 6 bar</li>
                <li>Pressão de bloqueio: 30 mbar a 800 mbar</li>
                <li>Temperatura ambiente: – 20 ÷ +60°C</li>
                <li>Tempo de fechamento: &lt; 1 s.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
