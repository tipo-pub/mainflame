<?php
include 'includes/geral.php';
$title = 'Home';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/slider.php';
?>
<section class="section section-light">
    <div class="container">
        <div class="text-center">
            <div class="font-weight-bold text-9">Distribuidor Autorizado</div>

            <div class="owl-carousel owl-theme" data-plugin-options="{'items': 6}">
                <div>
                    <img class="img-fluid" src="img/logos/honeywell.png" alt="Honeywell" />
                </div>
                <div>
                    <img class="img-fluid" src="img/logos/maxon.jpg" alt="Maxon" />
                </div>
                <div>
                    <img class="img-fluid" src="img/logos/siemens.jpg" alt="Siemens" />
                </div>
                <div>
                    <img class="img-fluid" src="img/logos/eclipse.jpg" alt="Eclipse" />
                </div>
                <div>
                    <img class="img-fluid" src="img/logos/hauck.jpg" alt="Hauck" />
                </div>
                <div>
                    <img class="img-fluid" src="img/logos/rockwell.jpg" alt="Rockwell Automation" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row clearfix">
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 reverse appear-animation" data-appear-animation="fadeInRightShorter">
                            <div class="feature-box-icon">
                                <i class="fas fa-cog text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Manutenção</h4>
                                <p class="mb-4">A MAINFLAME Combustion Technology realiza manutenções preventivas e corretivas em sistemas de combustão industrial aplicados em diversos processos industriais</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInLeftShorter">
                            <div class="feature-box-icon">
                                <i class="fa fa-laptop text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Engenharia / Consultoria</h4>
                                <p class="mb-4">A MAINFLAME Combustion Technology dispõe de técnicos e engenheiros altamente capacitados e com ampla experiência em processos industriais que utilizam sistemas de combustão.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 reverse appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                            <div class="feature-box-icon">
                                <i class="fa fa-suitcase text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Projetos Desenvolvidos</h4>
                                <p class="mb-4">A MAINFLAME Combustion Technology desenvolve projetos elétricos e mecânicos de maneira a atender as necessidades dos mais variados.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
                            <div class="feature-box-icon">
                                <i class="fa fa-user text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Assistência Técnica</h4>
                                <p class="mb-4">A MAINFLAME Combustion Technology possui uma equipe de técnicos e engenheiros altamente especializados e com ampla experiência em processos industriais.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col text-center">
                <a href="<?=$url?>servicos" class="btn btn-primary text-uppercase btn-px-5 py-3 font-weight-semibold text-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">Saiba Mais</a>
            </div>
        </div>
    </div>
</section>

<section class="section section-primary border-0 py-0 appear-animation" style="margin: 100px 0;" data-appear-animation="fadeIn">
    <div class="container">
        <div class="row align-items-center justify-content-center justify-content-lg-between pb-5 pb-lg-0">
            <div class="col-lg-5 order-2 order-lg-1 pt-4 pt-lg-0 pb-5 pb-lg-0 mt-5 mt-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                <h2 class="font-weight-bold text-color-light text-uppercase text-7 mb-2">A Empresa</h2>
                <p class="text-3">A MAINFLAME COMBUSTION TECHNOLOGY atua no mercado de combustão industrial desde 2010, oferecendo soluções completas e otimizadas para diversos processos da indústria em geral.</p>

                <p class="text-3">Seu principal objetivo é garantir aos clientes serviços de qualidade, buscando maior produtividade com eficiência e baixos custos de operação e manutenção, através do controle, supervisão e gerenciamento dos sistemas aplicados.</p>

                <p class="text-3">Da consultoria ao treinamento, passando por planejamento, execução e gerenciamento, atendendo do início ao fim todas as etapas do processo.</p>
                
                <a href="<?=$url?>institucional" class="btn btn-light text-primary text-uppercase btn-px-5 btn-py-2 text-2">Saiba mais <i class="fa fa-chevron-right"></i></a>
            </div>
            <div class="col-9 offset-lg-1 col-lg-5 order-1 order-lg-2 scale-2">
                <img class="img-fluid box-shadow-3 my-2 border-radius" src="img/img-empresa-home.jpg" alt="<?=$NomeEmpresa;?>">
            </div>
        </div>
    </div>
</section>

<?php $tituloGaleria = "Galeria"; include 'includes/galeria-fotos.php' ;?>

<?php include 'includes/footer.php' ;?>