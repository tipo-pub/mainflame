<?php
include 'includes/geral.php';
$title = 'Galeria de Vídeos';
$description = 'A mainflame combustion technology atua no mercado de combustão industrial desde 2010, oferecendo soluções completas e otimizadas para diversos processos da indústria em geral.';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4">
	<div class="row">
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/3OA-nxlUXzM" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/qrXiR9HqNsE" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/cw-VY4liwHs" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/WDqfT_V9TRI" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/c4ytVZrPxFM" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/rHNsueLvwAg" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/hdRiyAzmefI" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/4i28iDQBiZQ" allowfullscreen=""></iframe>
		</div>
		<div class="col-md-4">
			<iframe class="iframeVideos" src="https://www.youtube.com/embed/C4FNK6vB0t4" allowfullscreen=""></iframe>
		</div>
	</div>
</div>

<?php include 'includes/footer.php' ;?>