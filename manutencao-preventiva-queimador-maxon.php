<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA QUEIMADOR MAXON ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimador-maxon">
                <img src="img/manutencao-preventiva-queimador-maxon.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">28</span>
            <span class="month">maio</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-maxon">MANUTENÇÃO PREVENTIVA QUEIMADOR MAXON</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– desmontagem do queimador;</p>
            <p>– desmontagem do ventilador de ar de combustão.</p>
            <p>– inspeção visual da tubulação do tanque.</p>
            <p>– constatado grande acumulo de sujeira (poeira) no interior do queimador e do ventilador de ar de combustão.</p>
            <p>– inspeção e limpeza do queimador.</p>
            <p>– limpeza do eletrodo de ignição.</p>
            <p>– inspeção do ventilador de ar de combustão.</p>
            <p>– limpeza e lubrificação do ventilador de ar de combustão.</p>
            <p>– remontagem do queimador e ventilador ar de combustão.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimador-maxon/manutencao-preventiva-queimador-maxon-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimador-maxon/thumbs/manutencao-preventiva-queimador-maxon-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimador-maxon/manutencao-preventiva-queimador-maxon-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimador-maxon/thumbs/manutencao-preventiva-queimador-maxon-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-preventiva-queimador-maxon/manutencao-preventiva-queimador-maxon-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-preventiva-queimador-maxon/thumbs/manutencao-preventiva-queimador-maxon-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
