<?php
include 'includes/geral.php';
$title = 'Nossa Atuação';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>
<div class="container py-4">

    <div class="row">

        <?php include 'includes/info-lateral.php' ;?>

        <div class="col-lg-9 order-lg-1">
            
            <input type='hidden' id='current_page' />
            <input type='hidden' id='show_per_page' />

            <div class="blog-posts contProjetos" id='content'>


                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-maxon-valupak-300">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">dez</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon-valupak-300"></a>MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON VALUPAK 300</h3>
                        <p>– inspeção no cavalete de alimentação de gás; – queimadores retirados do local de operação e transportados até a bancada de manutenção; – realizado desmontagem dos queimadores e componentes; – verificação geral dos queimadores; – limpeza do queimador e do ventilador de ar de combustão; – limpeza e ajuste do eletrodo de ignição; – remontagem [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="<?=$canonical;?>">Nossa atuação</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-maxon-valupak-300"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="comissionamento-queimador-baltur">
                            <img src="img/comissionamento-queimador-baltur.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Comissionamento Queimador BALTUR" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">02</span>
                        <span class="month">dez</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="comissionamento-queimador-baltur">Comissionamento Queimador BALTUR</a></h3>
                        <p>– Comissionamento de 01 x Queimador Marca: BALTUR Modelo: BT 120 DSPG (diesel). – Instalado no AQUECEDOR DE FLUÍDO TÉRMICO Marca: GARIONI NAVAL Tipo: TH-V 600 – 697 KW. – A bordo do Navio Forte de São Marcos. DESCRIÇÃO DOS SERVIÇOS EXECUTADOS: – acompanhamento e orientações, durante a: fabricação de um novo flange de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="assistencia-tecnica"> Assistência Técnica</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="comissionamento-queimador-baltur"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-painel-eletrico-forno-fusao-vidro">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">25</span>
                        <span class="month">nov</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-painel-eletrico-forno-fusao-vidro">MANUTENÇÃO CORRETIVA NO PAINEL ELÉTRICO DO FORNO DE FUSÃO DE VIDRO</a></h3>
                        <p>DESCRIÇÃO DOS SERVIÇOS EXECUTADOS: – Realizada vistoria do painel elétrico. – Checagem das interligações elétricas dos componentes de campo. – Constatamos sinaleiro estava danificado em curto circuito, causando o desarme do painel elétrico. – Realizado a substituição do sinaleiro no painel elétrico. – Acendimento do queimador. – Acompanhamento do processo. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-painel-eletrico-forno-fusao-vidro"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-estufas-secagem">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">30</span>
                        <span class="month">out</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-estufas-secagem">MANUTENÇÃO PREVENTIVA NAS ESTUFAS DE SECAGEM</a></h3>
                        <p>– inspeção mecânica do queimador instalado; – inspeção mecânica do cavalete de alimentação de gás; – teste nas lógicas de segurança; – análise e ajuste da relação ar/gás; – elaboração de relatório; – curva de desempenho do queimador; – teste de operação; – acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-estufas-secagem"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807">
                            <img src="img/queimador-cavalete-de-gas-natural-painel-de-comando-instrume.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">11</span>
                        <span class="month">out</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807">QUEIMADOR, CAVALETE DE GÁS NATURAL, PAINEL DE COMANDO, INSTRUMENTAÇÃO DE CAMPO PARA O SISTEMA DE COMBUSTÃO DA CALDEIRA ATA MP-807</a></h3>
                        <p>DADOS DE PROJETO PARA APLICAÇÃO: Caldeira: ATA Modelo: MP-807 Ano fabricação: 1990 Superfície de vaporização: 46 m² Produção de vapor: 4.000 kg/h M.P.T.A.: 256 psig (18.00 kgf/cm²) Teste hidrostático: 384 psig (27.00 kgf/cm²) Número de ordem: 8495 Ventilador de ar de combustão: ATA CONDIÇÕES DE OPERAÇÃO: Combustível: Gás Natural Poder calorífico inferior (PCI): 9.000 kcal/Nm3 Peso específico: 0,79 kg/m3 Densidade relativa ao ar: [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="projetos-desenvolvidos"> Projetos desenvolvidos</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="queimador-cavalete-gas-natural-painel-comando-instrumentacao-campo-para-sistema-combustao-caldeira-ata-mp-807"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-impressora-vtv-11-queimador-maxon">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">23</span>
                        <span class="month">ago</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-impressora-vtv-11-queimador-maxon">MANUTENÇÃO PREVENTIVA IMPRESSORA VTV-11 – QUEIMADOR MAXON</a></h3>
                        <p> Inspeção mecânica do queimador instalado; Inspeção mecânica do cavalete de alimentação de gás; Levantamento fotográfico (com prévia autorização do cliente); Teste nas lógicas de segurança; Análise e ajuste da relação ar/gás; Elaboração de relatório; Curva de desempenho do queimador; Teste de operação; Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-impressora-vtv-11-queimador-maxon"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-boiler-weco-queimador-oertli">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">02</span>
                        <span class="month">ago</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-boiler-weco-queimador-oertli">MANUTENÇÃO PREVENTIVA BOLIER WECO – QUEIMADOR OERTLI</a></h3>
                        <p> Inspeção mecânica do queimador instalado; Inspeção mecânica do cavalete de alimentação de gás; Mão de obra para substituição do aparelho de teste de estanqueidade; Levantamento fotográfico (com prévia autorização do cliente); Teste nas lógicas de segurança; Análise e ajuste da relação ar/gás; Elaboração de relatório; Curva de desempenho do queimador; Teste de operação; Acompanhamento de posta em marcha. [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-boiler-weco-queimador-oertli"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-cavalete-gas-nattural-caldeira-allborg">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">29</span>
                        <span class="month">jul</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-cavalete-gas-nattural-caldeira-allborg">MANUTENÇÃO PREVENTIVA – CAVALETE DE GÁS NATURAL – CALDEIRA ALLBORG</a></h3>
                        <p>– Inspeção mecânica do queimador instalado; – Inspeção mecânica do cavalete de alimentação de gás; – Fornecimento e instalação das seguintes peças sobressalentes: – Elemento filtrante para filtro de gás; Kit de reparo para válvula shut off; Kit de reparo para válvula reguladora de pressão; Kit de switches (chave fim de curso) para o servo [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-cavalete-gas-nattural-caldeira-allborg"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-sistema-combustao-instalados-estufas-boiler">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">jul</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-sistema-combustao-instalados-estufas-boiler">MANUTENÇÃO PREVENTIVA – SISTEMAS DE COMBUSTÃO INSTALADOS NAS ESTUFAS NO BOILER</a></h3>
                        <p>– Desmontagem do queimador; – Inspeção dos componentes do queimador; – Limpeza e lubrificação dos componentes; – Remontagem do queimador; – Desmontagem do ventilador de ar de combustão (quando aplicável); – Inspeção dos componentes do ventilador (quando aplicável); – Limpeza e lubrificação do ventilador (quando aplicável); – Remontagem do ventilador (quando aplicável); – Desmontagem do Cavalete de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-sistema-combustao-instalados-estufas-boiler"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-forno-recozimento-tubos-otto-junker-queimadores-widemann">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-forno-recozimento-tubos-otto-junker-queimadores-widemann">MANUTENÇÃO PREVENTIVA – FORNO DE RECOZIMENTO DE TUBOS OTTO JUNKER – QUEIMADORES WIEDEMANN</a></h3>
                        <p>– Inspeção mecânica dos queimador instalado; – Inspeção mecânica dos cavaletes de alimentação de gás natural; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-forno-recozimento-tubos-otto-junker-queimadores-widemann"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="">
                            <img src="img/.jpg" class="img-thumbnail d-block" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="notica-berneck"></a></h3>
                        <p></p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="notica-berneck"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-sistemas-combustao">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">21</span>
                        <span class="month">jul</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-sistemas-combustao">MANUTENÇÃO PREVENTIVA – QUEIMADORES E SISTEMAS DE COMBUSTÃO</a></h3>
                        <p>– Inspeção mecânica dos queimadores instalados; – Substituição de peças de reposição dos queimadores; – Inspeção mecânica dos cavaletes de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-sistemas-combustao"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-fornalha-planta-termica-queimadores-coen">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">08</span>
                        <span class="month">jul</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-fornalha-planta-termica-queimadores-coen">MANUTENÇÃO PREVENTIVA – FORNALHA DA PLANTA TÉRMICA- QUEIMADORES COEN</a></h3>
                        <p>– Acompanhamento/orientação da substituição de peças de reposição dos queimadores; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/combustível; – Elaboração de relatório; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-fornalha-planta-termica-queimadores-coen"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-queimadores-setor-pintura">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">06</span>
                        <span class="month">jul</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-queimadores-setor-pintura">MANUTENÇÃO CORRETIVA – QUEIMADORES DO SETOR DE PINTURA</a></h3>
                        <p>– Inspeção mecânica do queimador instalado; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração de relatório; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento de posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-queimadores-setor-pintura"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-fundacao-butantan">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">01</span>
                        <span class="month">jul</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-fundacao-butantan">MANUTENÇÃO PREVENTIVA</a></h3>
                        <p>– Levantamento fotográfico (com prévia autorização do cliente); – Análise dos gases residuais de combustão; – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatório. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-fundacao-butantan"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimador-maxon-ovenpakebc-4sp">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">22</span>
                        <span class="month">jun</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-maxon-ovenpakebc-4sp">MANUTENÇÃO PREVENTIVA – QUEIMADOR MAXON OVENPAK EBC 4SP</a></h3>
                        <p>– Inspeção mecânica do queimador instalado; – Substituição do cone de mistura; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração de relatório; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimador-maxon-ovenpakebc-4sp"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-sistema-combustao-instalado-spray-dryer">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">17</span>
                        <span class="month">jun</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-sistema-combustao-instalado-spray-dryer">MANUTENÇÃO CORRETIVA – SISTEMA DE COMBUSTÃO INSTALADO NO SPRAY-DRYER</a></h3>
                        <p>– Inspeção mecânica dos queimador instalado; – Inspeção mecânica dos cavaletes de alimentação de gás natural; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha. [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-sistema-combustao-instalado-spray-dryer"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>
                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-maxon">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">09</span>
                        <span class="month">jun</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon">MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON</a></h3>
                        <p>– Retirada, desmontagem e limpeza dos queimadores instalados; – Substituição dos filtros de ar e juntas (materiais inclusos); – Remontagem e reinstalação dos queimadores; – Inspeção mecânica dos cavaletes de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-maxon"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-caldeira">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">25</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-caldeira">MANUTENÇÃO PREVENTIVA – QUEIMADOR DA CALDEIRA</a></h3>
                        <p>– Inspeção mecânica do queimador; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração de relatório; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento de posta em marcha; </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-caldeira"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-queimador-maxon-valupak">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">10</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-queimador-maxon-valupak">MANUTENÇÃO CORRETIVA – QUEIMADOR MAXON VALUPAK</a></h3>
                        <p>– Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-queimador-maxon-valupak"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-industriais">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">06</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-industriais">MANUTENÇÃO PREVENTIVA – QUEIMADORES INDUSTRIAISs</a></h3>
                        <p>– Verificação das condições operacionais; – Verificação da curva de relação ar/gás; – Medição e análise dos gases resultantes da combustão; – Testes operacionais. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-industriais"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-queimadores-ray-burners">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">03</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-queimadores-ray-burners">MANUTENÇÃO – QUEIMADORES RAY BURNERS</a></h3>
                        <p>– Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise dos gases de combustão; – Ajustes da relação ar/gás; – Elaboração de relatório; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-queimadores-ray-burners"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="painel-comando-controle-seguranca-sistema-combustao-operacao-09-cavaletes-alimentacao-gas-natural">
                            <img src="img/painel-comando-controle-seguranca-sistema-combustao.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">abr</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="painel-comando-controle-seguranca-sistema-combustao-operacao-09-cavaletes-alimentacao-gas-natural">PAINEL DE COMANDO, CONTROLE E SEGURANÇA DO SISTEMA DE COMBUSTÃO, PARA OPERAÇÃO DE 09 CAVALETES DE ALIMENTAÇÃO DE GÁS NATURAL</a></h3>
                        <p>PAINEL DE COMANDO, CONTROLE E SEGURANÇA: Painel de comando, controle e segurança do sistema de combustão de 08 cavaletes de alimentação de gás. Atendendo requisitos da norma ABNT NBR 12.313 de Setembro de 2000. Painel 1400x800x250mm, c/ grau de proteção IP 54, com pintura cinza RAL 7032; CLP Rockwell, Compact Logix (RS Logix [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="projetos-desenvolvidos"> Projetos desenvolvidos</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="painel-comando-controle-seguranca-sistema-combustao-operacao-09-cavaletes-alimentacao-gas-natural"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>
                
                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-john-deere">
                            <img src="img/manutencao-corretiva-john-deere.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">27</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-john-deere">MANUTENÇÃO CORRETIVA JOHN DEERE</a></h3>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="assistencia-tecnica"> Assistência Técnica</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="notica-berneck"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-freudenberg">
                            <img src="img/manutencao-corretiva-john-deere.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">27</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-freudenberg">MANUTENÇÃO CORRETIVA</a></h3>
                        <p>– Inspeção mecânica do queimador; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatório; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="assistencia-tecnica"> Assistência Técnica</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-freudenberg"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-instalados">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">27</span>
                        <span class="month">abr</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-instalados">MANUTENÇÃO PREVENTIVA – QUEIMADORES INSTALADOS</a></h3>
                        <p>– Inspeção e limpeza dos queimadores instalados; – Substituição de peças danificadas dos queimadores – Inspeção mecânica dos cavaletes de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Testes de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-instalados"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                
                <article class="post post-large">
                    <div class="post-image">
                        <a href="ajuste-regulagem-queimadores-forno-banho-zinco">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">26</span>
                        <span class="month">abr</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="ajuste-regulagem-queimadores-forno-banho-zinco">AJUSTE/REGULAGEM DE QUEIMADORES – FORNO DE BANHO DE ZINCO</a></h3>
                        <p>– Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha; </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="ajuste-regulagem-queimadores-forno-banho-zinco"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimador-maxon-ovenpak">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">26</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-maxon-ovenpak">MANUTENÇÃO PREVENTIVA – QUEIMADOR OVENPAK</a></h3>
                        <p>– Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha.   </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimador-maxon-ovenpak"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-caldeira-aalborg">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">15</span>
                        <span class="month">abr</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-caldeira-aalborg">MANUTENÇÃO CORRETIVA – CALDEIRA AALBORG</a></h3>
                        <p>– Inspeção mecânica do queimador instalado; – Inspeção mecânica do cavalete de alimentação de gás; – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento da posta em marcha; </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-caldeira-aalborg"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>
                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-sistema-combustao-queimador-eclipse-tj-100">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">11</span>
                        <span class="month">abr</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-sistema-combustao-queimador-eclipse-tj-100">MANUTENÇÃO PREVENTIVA – SISTEMA DE COMBUSTÃO COM QUEIMADOR ECLIPSE TJ-100</a></h3>
                        <p>MANUTENÇÃO PREVENTIVA – SISTEMA DE COMBUSTÃO COM QUEIMADOR ECLIPSE TJ-100</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-sistema-combustao-queimador-eclipse-tj-100"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-maxon-ovenpak">
                            <img src="img/manutencao-preventiva-queimadores-maxon-ovenpak.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon-ovenpak">MANUTENÇÃO PREVENTIVA – QUEIMADORES MAXON OVENPAK</a></h3>
                        <p>– Inspeção mecânica e limpeza dos queimadores instalados; – Inspeção mecânica e limpeza dos cavaletes de alimentação de gás; – Troca dos elementos filtrantes dos filtros de gás (as peças serão fornecidas pela MAINFLAME); – Troca da junta grafitada do tubo de chama dos queimadores (as peças serão fornecidas pela MAINFLAME); – Inspeção [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-maxon-ovenpak"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimadores-maxon-rexam-df">
                            <img src="img/manutencao-preventiva-dos-queimadores-maxon-rexam-df.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">29</span>
                        <span class="month">mar</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-maxon-rexam-df">MANUTENÇÃO PREVENTIVA DOS QUEIMADORES MAXON – REXAM – DF</a></h3>
                        <p>SERVIÇOS DE ASSISTÊNCIA TÉCNICA ESPECIALIZADA PARA MANUTENÇÃO PREVENTIVA NOS QUEIMADORES Escopo dos Serviços Inspeção mecânica dos queimadores instalados; Substituição de peças danificadas dos queimadores; Inspeção mecânica dos cavaletes de alimentação de gás; Levantamento fotográfico (com prévia autorização do cliente); Teste nas lógicas de segurança; Análise e ajuste da relação ar/gás; Elaboração de relatórios; Curva de desempenho dos queimadores; Testes [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimadores-maxon-rexam-df"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimador-thermotec">
                            <img src="img/manutencao-preventiva-queimador-thermotec.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">22</span>
                        <span class="month">mar</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-thermotec">MANUTENÇÃO PREVENTIVA QUEIMADOR THERMOTEC</a></h3>
                        <p>– Inspeção mecânica do queimador; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatório; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento da posta em marcha. [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimador-thermotec"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-forno-tratamento-termico-sermep">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">26</span>
                        <span class="month">fev</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-forno-tratamento-termico-sermep">MANUTENÇÃO PREVENTIVA FORNO DE TRATAMENTO TÉRMICO SERMEP</a></h3>
                        <p>– Inspeção mecânica dos queimadores instalados; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração de relatório; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento de posta em marcha.   [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-forno-tratamento-termico-sermep"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimador-maxon-ovenpak-melhoramentos">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">24</span>
                        <span class="month">fev</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-maxon-ovenpak-melhoramentos">MANUTENÇÃO PREVENTIVA QUEIMADOR MAXON OVENPAK</a></h3>
                        <p>– Inspeção mecânica do queimador instalado; – Inspeção mecânica do cavalete de alimentação de gás; – Substituição do kit de juntas do queimador; – Substituição do elemento filtrante do filtro de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análise e ajuste da relação ar/gás; – Elaboração de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimador-maxon-ovenpak-melhoramentos"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>
                
                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-caldeira-b02u">
                            <img src="img/manutencao-corretiva-caldeira-b02u.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">23</span>
                        <span class="month">fev</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-caldeira-b02u">MANUTENÇÃO CORRETIVA CALDEIRA B02U</a></h3>
                        <p>– Inspeção mecânica do queimador instalado; – Inspeção mecânica dos cavaletes de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatórios; – Curva de desempenho dos queimadores; – Teste de operação; – Acompanhamento da posta em marcha. [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-caldeira-b02u"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>


                <article class="post post-large">
                    <div class="post-image">
                        <a href="comissionamento-queimadores-maxon">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">12</span>
                        <span class="month">fev</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="comissionamento-queimadores-maxon">COMISSIONAMENTO QUEIMADORES MAXON</a></h3>
                        <p>COMISSIONAMENTO E START-UP – Acendimento do queimador; – Ajustes de operação do queimador; – Teste das lógicas de segurança; – Operação assistida. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="comissionamento-queimadores-maxon"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas">
                            <img src="img/queimador-cavalete-de-glp-e-painel-para-estufa-continua-de-pintura.jpg" style="width: 100%" class="img-thumbnail d-block" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">10</span>
                        <span class="month">fez</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas">QUEIMADOR, CAVALETE DE GLP E PAINEL PARA ESTUFA CONTÍNUA DE PINTURA DE VÁLVULAS</a></h3>
                        <p>QUEIMADOR PARA GÁS GLP Queimador de Duto montado em Gerador de Ar Quente Liberação de calor máxima: 250.000 kcal/h Vazão de gás máxima: 10 Nm³/h Sistema de controle: modulante (ar fixo) Eletrodo de ignição Sensor de chama ultravioleta para operação contínua Ventilador de ar de combustão com motor elétrico Pressostato de baixa pressão de ar Pressostato diferencial para [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="queimador-cavalete-glp-painel-estufa-continua-pintura-valvulas"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-corretiva-freudenberg">
                            <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">14</span>
                        <span class="month">jan</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-freudenberg">MANUTENÇÃO CORRETIVA – QUEIMADOR THERMOTEC</a></h3>
                        <p>– Inspeção mecânica do queimador; – Inspeção mecânica do cavalete de alimentação de gás; – Levantamento fotográfico (com prévia autorização do cliente); – Teste nas lógicas de segurança; – Análises e ajustes da relação ar/gás; – Elaboração de relatório; – Curva de desempenho do queimador; – Teste de operação; – Acompanhamento da posta em marcha. </p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-corretiva-freudenberg"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>

                <article class="post post-large">
                    <div class="post-image">
                        <a href="servico-especializado-montagem-painel-comando">
                            <img src="img/servico-especializado-de-montagem-de-painel-de-comando.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">06</span>
                        <span class="month">jun</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="servico-especializado-montagem-painel-comando">SERVIÇO ESPECIALIZADO DE MONTAGEM DE PAINEL DE COMANDO</a></h3>
                        <p>Um painel elétrico de comando e montagem industrial pode ser definido como um compartimento modular utilizado para alocar dispositivos eletrônicos em seu interior. Geralmente, os painéis são construídos em estruturas em chapa metálica, com perfis de dobras perfurados ou não, possuindo fechamentos em chapas e portas com sistema de [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="servico-especializado-montagem-painel-comando"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>                
                
                <article class="post post-large">
                    <div class="post-image">
                        <a href="manutencao-preventiva-queimador-maxon">
                            <img src="img/manutencao-preventiva-queimador-maxon.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
                        </a>
                    </div>

                    <div class="post-date">
                        <span class="day">28</span>
                        <span class="month">maio</span>
                    </div>

                    <div class="post-content">

                        <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimador-maxon">MANUTENÇÃO PREVENTIVA QUEIMADOR MAXON</a></h3>
                        <p>– desmontagem do queimador; – desmontagem do ventilador de ar de combustão. – inspeção visual da tubulação do tanque. – constatado grande acumulo de sujeira (poeira) no interior do queimador e do ventilador de ar de combustão. – inspeção e limpeza do queimador. – limpeza do eletrodo de ignição. – inspeção do ventilador de ar [...]</p>

                        <div class="post-meta">
                            <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="manutencao-preventiva-queimador-maxon"><button type="button" class="btn btn-modern btn-green mb-2">Leia mais...</button></a></span>
                        </div>

                    </div>
                </article>               

            <div class="d-flex align-items-center justify-content-center" id='portfolioPagination'></div>
        </div>
    </div>
</div>

</div>

<?php include 'includes/footer.php' ;?>
