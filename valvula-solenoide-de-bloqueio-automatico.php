<?php
include 'includes/geral.php';
$title = 'Válvula Solenoide de Bloqueio Automatico';
$description = 'A válvula solenoide para gás é uma válvula de bloqueio automático, normalmente fechada, que quando desenergizada.';
$keywords = 'produtos, Válvula Solenoide de Bloqueio Automatico, a melhor Válvula Solenoide de Bloqueio Automatico';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-solenoide-bloqueio-automatico.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-solenoide-bloqueio-automatico.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A válvula solenoide para gás é uma válvula de bloqueio automático, normalmente fechada, que quando desenergizada, obstrui a passagem de gás em um tempo 	&rsaquo; 1s.</p> <h3>CARACTERÍSTICAS TÉCNICAS</h3>

                    <ul>
                        <li>Conexões roscadas Rp: (DN 10 ÷ DN 50) de acordo com DIN 2999</li>
                        <li>Conexões flangeadas PN 16 (DN 20 ÷ DN 200) de acordo com ISO 7005</li>
                        <li>Pressão de trabalho: 200 mbar a 500 mbar</li>
                        <li>Temperatura ambiente: – 15 ÷ +60°C</li>
                        <li>Grau de proteção: IP54, IP65.</li>
                        <li>Tempo de fechamento: &lt; 1 s.</li>
                        <li>Opcional: com ajuste de vazão.</li>
                        <li>Tensão de alimentação: 24 VCC, 110 VCA, 220 VCA</li>
                    </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
