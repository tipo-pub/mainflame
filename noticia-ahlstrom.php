<?php
include 'includes/geral.php';
$title = 'Notícias Ahlstrom';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4 noticias">

    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">

                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-image ml-0">
                        <a href="notica-ahlstrom">
                            <img src="img/ahlstrom.jpg" class="img-thumbnail d-block" alt="Ahlstrom" />
                        </a>
                    </div>

                    <div class="post-date ml-0">
                        <span class="day">28</span>
                        <span class="month">Maio</span>
                    </div>

                    <div class="post-content ml-0">

                        <h3 class="text-6 line-height-3 mb-2"><a href="<?=$canonical?>"> FORNECIMENTO DE CAVALETES DE GÁS NATURAL, PAINEL LOCAL E PAINEL DE AUTOMAÇÃO PARA UPGRADE DO SISTEMA DE COMBUSTÃO DO FORNO DE CARVÃO GAC </a></h3>

                        <div class="post-meta">
                            <a href="notica-ahlstrom"><span><i class="fa fa-folder-open"></i> Ahlstrom</span></a>
                            <a href="noticias"><span><i class="fas fa-newspaper"></i> Notícias</span></a>
                        </div>

                        <h4>FORNECIMENTO DE CAVALETES DE GÁS NATURAL, PAINEL LOCAL E PAINEL DE AUTOMAÇÃO PARA UPGRADE DO SISTEMA DE COMBUSTÃO DO FORNO DE CARVÃO GAC</h4>

                        <ul>
                            <li>Adequar os sistemas de combustão a norma ABNT NBR 12.313/2000 e as normas internas AJINOMOTO. Garantindo a segurança operacional.</li>
                            <li>Melhoria operacional identificando e registrando as condições de operação, proporcionando menor tempo de parada para manutenção e eliminando situações de paradas inesperadas.</li>
                            <li>Substituição dos sistema de controle de combustão proporcionando economia de combustível (GN) e melhor eficiência energética.</li>
                            <li>Modernização do sistema de automação garantindo a segurança operacional e padronização com os equipamentos da planta.</li>
                        </ul>

                    </div>
                </article>

            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ahlstrom/image001-2.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ahlstrom/thumbs/image001-2.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ahlstrom/image002-1-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ahlstrom/thumbs/image002-1-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ahlstrom/image003-2.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ahlstrom/thumbs/image003-2.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ahlstrom/image004-2.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ahlstrom/thumbs/image004-2.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/noticias/ahlstrom/image005-1.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/noticias/ahlstrom/thumbs/image005-1.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>

<?php include 'includes/footer.php' ;?>
