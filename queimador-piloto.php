<?php
include 'includes/geral.php';
$title = 'Queimador Piloto';
$description = 'O queimador piloto MAINFLAME é projetado para operar com gás combustível (Gás Natural ou GLP).';
$keywords = 'produtos, Queimador Piloto, a melhor Queimador Piloto';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/queimador-piloto.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/queimador-piloto.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">O queimador piloto MAINFLAME é projetado para operar com gás combustível (Gás Natural ou GLP). O ar de combustão para este queimador é fornecido pelo próprio ventilador de ar de combustão do queimador principal, dispensando alimentações de ar externas (ar comprimido).</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Capacidade: 45.000 kcal/h.</li>
                <li>Com transformador de ignição incorporado.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
