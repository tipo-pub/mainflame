<?php
include 'includes/geral.php';
$title			= 'Erro 404';
$description	= 'Erro 404';
$keywords		= '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4">
	<section class="http-error">
		<div class="row justify-content-center">
			<div class="col text-center">
				<div class="http-error-main">
					<h2>404</h2>
					<p class="text-10"><?=$title?></p>
					<p class="lead"><a href="mapa-site"><button type="button" class="tp-caption btn btn-primary text-uppercase font-weight-bold btn-rounded"><i class="fa fa-arrow-right"></i> <span>Voltar para o site</span> <i class="fa fa-arrow-left"></i></button></a></p>
				</div>
			</div>	
		</div>
	</section>
</div>
<?php include "includes/footer.php";?>