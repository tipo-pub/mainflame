<?php
include 'includes/geral.php';
$title = 'Servo Motor';
$description = 'Os Servo Motores são utilizados em conjuntos de damper ou válvulas que transmite um movimento de rotação de 0 a até 160º.';
$keywords = 'produtos, Servo Motor, a melhor Servo Motor';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/servo-motor.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/servo-motor.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Os Servo Motores são utilizados em conjuntos de damper ou válvulas que transmite um movimento de rotação de 0 a até 160º. Os motores aceitam um sinal de corrente ou de tensão de um controlador eletrônico para posicionar um damper ou válvula em qualquer ponto entre aberto e fechado.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Tempo de operação: 30s a 60s. Outras temporizações disponíveis</li>
                <li>Tensão de alimentação: a partir de 24 Vac.</li>
                <li>Grau de proteção: Nema 3.</li>
                <li>Faixa de entrada: 4 a 20 mA, Zero/Nulo, 2 a 10 Vdc.</li>
                <li>Impedância de entrada: 100 ohms, 400K ohms.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
