<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO CORRETIVA CALDEIRA B02U ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-corretiva-caldeira-b02u">
                <img src="img/manutencao-corretiva-caldeira-b02u.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">23</span>
            <span class="month">fev</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-caldeira-b02u">MANUTENÇÃO CORRETIVA CALDEIRA B02U</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica do queimador instalado;</p>
            <p>– Inspeção mecânica dos cavaletes de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análises e ajustes da relação ar/gás;</p>
            <p>– Elaboração de relatórios;</p>
            <p>– Curva de desempenho dos queimadores;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento da posta em marcha.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-caldeira-b02u/manutencao-corretiva-caldeira-b02u-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-caldeira-b02u/thumbs/manutencao-corretiva-caldeira-b02u-07.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
