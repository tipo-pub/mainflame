<?php
include 'includes/geral.php';
$title = 'Assistência Técnica';
$description = 'A Assistência Técnica da MAINFLAME está disponível 24 horas por dia, 7 dias por semana. Isso para garantir total assistência ao cliente.';
$keywords = 'produtos, Assistência Técnica, a melhor Assistência Técnica';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/assistencia-tecnica.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/assistencia-tecnica.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A MAINFLAME Combustion Technology possui uma equipe de técnicos e engenheiros altamente especializados e com ampla experiência em processos industriais que utilizam sistemas de combustão, atuando a mais de 15 anos nos setores da indústria, em geral.</p>

            <p class="mt-2">A Assistência Técnica da MAINFLAME Combustion Technology está disponível 24 horas por dia, 7 dias por semana. Isso para garantir total assistência ao cliente. A equipe de assistência técnica é especializada e capacitada à atender todas as demandas da empresa.</p>

            <p>A nossa equipe de Assistência Técnica está preparada e disponível para realizar:</p>

            <ul>
                <li>Supervisão de Montagens Elétricas e Mecânicas</li>
                <li>Comissionamento e Partida</li>
                <li>Treinamento e Suporte Técnico</li>
                <li>Operação Assistida</li>
                <li>Atendimento em Todo Território Nacional e América-Latina</li>
            </ul>

            <p class="mt-2"><a href="<?=$url?>pdf/condicoes-gerais-de-fornecimento-assistencia-tecnica.pdf" target="_blank" class="text-primary" title="Condições Gerais de Fornecimento e de Assistência Técnica">Condições Gerais de Fornecimento e de Assistência Técnica.</a></p>
        </div>

    </div>

    <?php include 'includes/cases-relacionados.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
