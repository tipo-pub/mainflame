<?php
include 'includes/geral.php';
$title = 'Transformador De Ignição';
$description = 'Os transformadores de ignição geram uma faísca elétrica forte e estável, usada para obter-se uma ignição segura nos queimadores a gás ou óleo.';
$keywords = 'produtos, Transformador De Ignição, a melhor Transformador De Ignição';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/tranformador-ignicao.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/tranformador-ignicao.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Os transformadores de ignição geram uma faísca elétrica forte e estável, usada para obter-se uma ignição segura nos queimadores a gás ou óleo. São dotados de uma blindagem interna supressora de interferências eletromagnéticas.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Tensão de ignição: 1,5 a 8 kV</li>
                <li>Ciclo: 19% ED ou operação continua;</li>
                <li>Tensão de alimentação: 115 Vca ou 230Vca.</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
