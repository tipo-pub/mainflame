<?php
include 'includes/geral.php';
$title = ' SERVIÇO ESPECIALIZADO DE MONTAGEM DE PAINEL DE COMANDO ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="servico-especializado-montagem-painel-comando">
                <img src="img/servico-especializado-de-montagem-de-painel-de-comando.jpg" class="img-thumbnail d-block" style="width: 100%" alt="" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">06</span>
            <span class="month">jun</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="servico-especializado-montagem-painel-comando">SERVIÇO ESPECIALIZADO DE MONTAGEM DE PAINEL DE COMANDO</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>Um painel elétrico de comando e montagem industrial pode ser definido como um compartimento modular utilizado para alocar dispositivos eletrônicos em seu interior. Geralmente, os painéis são construídos em estruturas em chapa metálica, com perfis de dobras perfurados ou não, possuindo fechamentos em chapas e portas com sistema de fecho.</p>

            <p>Um painel elétrico de comando é utilizado para controlar uma máquina e/ou equipamento. Neste caso, os componentes de um painel de controle dependem da máquina que estão sendo comandadas e do tipo de controle que é necessário. Dificilmente, na literatura, será possível encontrar um assunto que fale sobre todos os tipos de comandos, uma vez que é a exigência do sistema que orienta o controle. Na montagem do painel elétrico de comando são utilizadas chaves eletrônicas, inversores, contatores, CLPs e dispositivos de entrada de sinais (sensores, medidores, etc) com o objetivo de controlar outros dispositivos eletrônicos.</p>

        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-04.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-04.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-05.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-05.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-06.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-06.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-07.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-07.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-08.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-08.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-09.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-09.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-09.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-09.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-10.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-10.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-11.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-11.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-12.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-12.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/servico-especializado-montagem-painel-comando/servico-especializado-montagem-painel-comando-13.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/servico-especializado-montagem-painel-comando/thumbs/servico-especializado-montagem-painel-comando-13.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
