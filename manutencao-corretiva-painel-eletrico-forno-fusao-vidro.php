<?php
include 'includes/geral.php';
$title = 'MANUTENÇÃO CORRETIVA NO PAINEL ELÉTRICO DO FORNO DE FUSÃO DE VIDRO';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-corretiva-painel-eletrico-forno-fusao-vidro">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">25</span>
            <span class="month">nov</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-corretiva-painel-eletrico-forno-fusao-vidro">MANUTENÇÃO CORRETIVA NO PAINEL ELÉTRICO DO FORNO DE FUSÃO DE VIDRO</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
               
            </div>

            <h3 class="mt-5">DESCRIÇÃO DOS SERVIÇOS EXECUTADOS:</h3>

            <p>– Realizada vistoria do painel elétrico.</p>

            <p>– Checagem das interligações elétricas dos componentes de campo.</p>

            <p>– Constatamos sinaleiro estava danificado em curto circuito, causando o desarme do painel elétrico.</p>

            <p>– Realizado a substituição do sinaleiro no painel elétrico.</p>

            <p>– Acendimento do queimador.</p>

            <p>– Acompanhamento do processo.</p>



        </div>
    </article>

    <div class="row mt-5">
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-painel-eletrico-forno-fusao-vidro/manutencao-painel-eletrico-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-painel-eletrico-forno-fusao-vidro/thumbs/manutencao-painel-eletrico-01.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-painel-eletrico-forno-fusao-vidro/manutencao-painel-eletrico-02.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-painel-eletrico-forno-fusao-vidro/thumbs/manutencao-painel-eletrico-02.jpg" alt="<?=$title;?>">
            </a>
        </div>
        <div class="col-lg-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon lightbox" href="img/projetos/manutencao-corretiva-painel-eletrico-forno-fusao-vidro/manutencao-painel-eletrico-03.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/projetos/manutencao-corretiva-painel-eletrico-forno-fusao-vidro/thumbs/manutencao-painel-eletrico-03.jpg" alt="<?=$title;?>">
            </a>
        </div>
    </div>

</div>


<?php include 'includes/footer.php' ;?>
