<?php
include 'includes/geral.php';
$title			= 'Formulário enviado com Sucesso';
$description	= 'Formulário Enviado com Sucesso. Em breve entraremos em contato!';
$keywords		= '';
include 'includes/head.php';
if (($_SERVER['HTTP_HOST'] == "www.mainflame.com.br") || ($_SERVER['HTTP_HOST'] == "mainflame.com.br") || ($_SERVER['HTTP_HOST'] == "www.tipotemporario.com.br") || ($_SERVER['HTTP_HOST'] == "tipotemporario.com.br")) {
?>
<meta http-equiv="refresh" content="5; url=<?=$url?>" />
<?php
}
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4">
	<section class="http-error">
		<div class="row justify-content-center">
			<div class="col text-center">
				<div class="http-error-main">
					<h2><i class="fa fa-paper-plane fa-2x"></i></h2>
					<p class="text-10"><?=$title?></p>
					<p class="lead">Em breve entraremos em contato!</p>
				</div>
			</div>	
		</div>
	</section>
</div>
<?php include "includes/footer.php";?>