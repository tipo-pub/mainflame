<?php
include 'includes/geral.php';
$title = 'Mapa do Site';
$description = 'Navegue por todas as nossas páginas';
$keywords = 'mapa do site, mapa site, área de navegação';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
;?>


<div class="container py-4 d-table">
	<ul class="lista-mapa-site">
		<?php $menuMapaSite = true; $menuTopo = false; include 'includes/menu.php';?>
	</ul>
</div>


<?php include 'includes/footer.php' ;?>
