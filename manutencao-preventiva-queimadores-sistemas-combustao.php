<?php
include 'includes/geral.php';
$title = ' MANUTENÇÃO PREVENTIVA – QUEIMADORES E SISTEMAS DE COMBUSTÃO ';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <article class="post post-large">
        <div class="post-image">
            <a href="manutencao-preventiva-queimadores-sistemas-combustao">
                <img src="img/latestnew-3.jpg" class="img-thumbnail d-block" style="width: 100%" alt="Mainflame" />
            </a>
        </div>

        <div class="post-date">
            <span class="day">21</span>
            <span class="month">jul</span>
        </div>

        <div class="post-content">

            <h3 class="text-6 line-height-3 mb-3"><a href="manutencao-preventiva-queimadores-sistemas-combustao">MANUTENÇÃO PREVENTIVA – QUEIMADORES E SISTEMAS DE COMBUSTÃO</a></h3>

            <div class="post-meta">
                <span><i class="fa fa-folder-open"></i> <a href="manutencao"> Manutenção</a>, <a href="noticias">Notícias</a> </span>
            </div>

            <p>– Inspeção mecânica dos queimadores instalados;</p>
            <p>– Substituição de peças de reposição dos queimadores;</p>
            <p>– Inspeção mecânica dos cavaletes de alimentação de gás;</p>
            <p>– Levantamento fotográfico (com prévia autorização do cliente);</p>
            <p>– Teste nas lógicas de segurança;</p>
            <p>– Análises e ajustes da relação ar/gás;</p>
            <p>– Elaboração de relatórios;</p>
            <p>– Curva de desempenho dos queimadores;</p>
            <p>– Teste de operação;</p>
            <p>– Acompanhamento da posta em marcha.</p>

        </div>
    </article>



</div>


<?php include 'includes/footer.php' ;?>
