<?php
include 'includes/geral.php';
$title = 'Valvula de Esfera';
$description = 'A válvula de esfera é para bloqueio manual em linhas de gás e ar. Possuem curso para abertura ou fechamento de 90º com limitadores mecânicos de curso.';
$keywords = 'produtos, Valvula de Esfera, a melhor Valvula de Esfera';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-esfera.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-esfera.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">A válvula de esfera é para bloqueio manual em linhas de gás e ar. Possuem curso para abertura ou fechamento de 90º com limitadores mecânicos de curso. Testadas e aprovadas pela CE.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Uso: Gases das três famílias (secos e não agressivos)</li>
                <li>Conexões roscadas Rp: (DN 8 ÷ DN 50) de acordo com DIN 2999</li>
                <li>Conexões flangeadas PN 16 (DN 25 ÷ DN 200) de acordo com ISO 7005</li>
                <li>Pressão máxima de trabalho: 5 bar ou 16 bar</li>
                <li>Temperatura ambiente: – 20 ÷ +60°C</li>
            </ul>

        </div>
    </div>

    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
