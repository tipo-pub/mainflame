<?php
include 'includes/geral.php';
$title = ' Válvula Solenoide de Bloqueio Automatico ';
$description = 'Válvula de bloqueio automatico atuadas eletricamente ou pneumaticamente com fechamento por mola em menos de 1 segundo.';
$keywords = 'produtos, Válvula de bloqueio, a melhor Válvula de bloqueio';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>


<div class="container">

    <div class="row">
        <div class="col-md-4">
            <a class="img-thumbnail img-thumbnail-no-borders d-block lightbox" href="img/produtos/valvula-atuador-eletromecanico-01.jpg" data-plugin-options="{'type':'image'}">
                <img class="img-fluid" src="img/produtos/valvula-atuador-eletromecanico-01.jpg" alt="<?=$title;?>">
                <span class="zoom">
                    <i class="fas fa-search"></i>
                </span>
            </a>
        </div>

        <div class="col-md-8">
            <p class="mt-2">Válvula de bloqueio automatico atuadas eletricamente ou pneumaticamente com fechamento por mola em menos de 1 segundo.</p>

            <h3>CARACTERÍSTICAS TÉCNICAS</h3>

            <ul>
                <li>Conexões de 3/4” a 6”</li>
                <li>Pressão de trabalho: até 8 bar.</li>
                <li>Temperatura ambiente: – 29 ÷ +60°C</li>
                <li>Grau de proteção: Nema 4.</li>
                <li>Opção com rearme manual</li>
                <li>Versões disponíveis normalmente fechada e normalmente aberta.</li>
                <li>Tensão de alimentação: 24 VCC, 110 VCA, 220 VCA</li>
            </ul>

        </div>
    </div>
    
    <?php include 'includes/produtos-home.php' ;?>

</div>


<?php include 'includes/footer.php' ;?>
