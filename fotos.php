<?php
include 'includes/geral.php';
$title = 'Galeria de Fotos';
$description = '';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
include 'includes/breadcrumb.php';
?>

<div class="container py-4">
    <div class="row">
        <?php $tituloGaleria = "Galeria de Fotos"; include 'includes/galeria-fotos.php' ;?>
    </div>    
</div>

<?php include 'includes/footer.php' ;?>
